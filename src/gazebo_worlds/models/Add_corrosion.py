import cv2 
import numpy as np
import random
import os
import sys


    
def add_Corrosion(path, image, nb,length, ratio, size_min):
    #os.chdir("~/catkin_ws/src/drone-simulator/gazebo_worlds/models/boat/materials/textures")
    print(path)
    print(image)
    print(nb)
    print("length == ", length)
    img = cv2.imread(path+"/"+image)
    cor_img = cv2.imread(path+"/Rust.jpg")
    ratio = img.shape[0]/(2*length)
    img = cv2.resize(img, (471, 243), interpolation = cv2.INTER_AREA)
    print(img.shape)
    for i in range(nb):
        # resize image
        size = random.randint(size_min, size_min*2.5)
        dim_x = size
        dim_y = size*ratio
        dim = (int(dim_x), int(dim_y))
        print("dim == ",dim)
        cor_img = cv2.resize(cor_img, dim, interpolation = cv2.INTER_AREA)
        
        #cor_img = cv2.imread(path+"/Rust.jpg")
        step = img.shape[1]/(nb+1)
        x_offset = random.randint(int(i*(step)), int((i+1)*(step)))
        step = img.shape[0]/(nb+1)
        y_offset = random.randint(0,img.shape[0])
        print([y_offset-y_offset+cor_img.shape[0] , x_offset-x_offset+cor_img.shape[1]])
        img[y_offset:y_offset+cor_img.shape[0], x_offset:x_offset+cor_img.shape[1]] = cor_img

    img = cv2.resize(img, (img.shape[1]*2, img.shape[0]*2), interpolation = cv2.INTER_AREA)
    cv2.imwrite(path+'/out.png', img)
    cv2.imwrite(path+'/'+image+'_out.png', img)


    print("add")




# =======================================================
"""			   main			"""
# =======================================================
if __name__ == "__main__":
    
    path = sys.argv[1]
    image = sys.argv[2]
    nb = sys.argv[3]
    length = sys.argv[4]
    ratio = sys.argv[5]
    size_min = sys.argv[6]
    add_Corrosion(path, image, int(nb), float(length), float(ratio), int(size_min))
    
