#!/usr/bin/python

import sys, os

#model_base=open("base_2.xacro","r").read();
link_model=open("link-model.sdf","r").read();

if len(sys.argv)<4:
    print("Usage: %s <ropelength> <segmentlength> <model_base>" % sys.argv[0])
    sys.exit(0)


ropelength=float(sys.argv[1])
segmentlength=float(sys.argv[2])
base_name = sys.argv[3]
model_base = open(sys.argv[3],"r").read();

links=[]

i=1
# y = a x ^ 2 + b x + c
# b = 0
# c = minlink
# a = (maxlink-minlink)
while segmentlength*i < ropelength:
    links.append(link_model % (i,i+1, segmentlength, segmentlength))
    i += 1
print("Generated %d links"%i)

open("base_test.xacro","w").write(model_base)#%("\n".join(links)))

open("base_test.xacro","w").write(model_base+("\n".join(links)+("\n </robot>")))



