/*
 * Copyright (C) 2014-2016 Open Source Robotics Foundation
 * Copyright 2020 Cedric Pradalier, Associate Prof. GeorgiaTech Lorraine
 * Copyright 2020 Vincent LE DOZE, INRIA, Chroma Team, CITI Lab Lyon, France
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "gazebo_plugins/gazebo_plugin__crawler_cable.h"
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/physics/Collision.hh>
#include <gazebo/common/common.hh>
#include <tf/transform_listener.h>
#include <cmath>
#include <ignition/math/Vector3.hh>
#include <gz/math/Color.hh>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
//#include <thread>         // std::thread
#include <boost/thread.hpp>

using namespace gazebo;

GZ_REGISTER_MODEL_PLUGIN(CrawlerCablePlugin);

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
CrawlerCablePlugin::CrawlerCablePlugin() : ModelPlugin() {
}

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
CrawlerCablePlugin::~CrawlerCablePlugin() {
  #if GAZEBO_MAJOR_VERSION >= 8
  update_connection_ptr.reset();
  #else
  event::Events::DisconnectWorldUpdateBegin(update_connection_ptr);
  #endif
}

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
void CrawlerCablePlugin::Reset() {
  link_->ResetPhysicsStates();
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
void CrawlerCablePlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)  {
    
    std::string name;
    
    //Parameters
    getSdfParam<std::string>(_sdf, "robot_target", robot_target_, "");
    getSdfParam<std::string>(_sdf, "robot_namespace", robot_namespace_, "");
    getSdfParam<std::string>(_sdf, "parent_frame", parent_frame_, "");
    getSdfParam<std::string>(_sdf, "name", name, "");
    getSdfParam<std::string>(_sdf, "link_name", link_name_, "");
    getSdfParam<std::string>(_sdf, "joint_name_y", joint_name_y_, "");
    getSdfParam<std::string>(_sdf, "joint_name_z", joint_name_z_, "");

    getSdfParam<float>(_sdf, "x", cur_x, 10);
    getSdfParam<float>(_sdf, "y", cur_y, 10);
    getSdfParam<float>(_sdf, "z", cur_z, 10);
    getSdfParam<float>(_sdf, "length", length_, 0.5);
    length_init_=length_;
    
    /*float x = cur_x;
    cur_x = -cur_y;
    cur_y = x; */

    pose_.x = -cur_y;
    pose_.y = cur_x;
    pose_.z = cur_z;
      

    std::cout<<"                robot_namespace ======== : "<<robot_namespace_<<"------------------"<<std::endl;
    std::cout<<"                robot_target ======== : "<<robot_target_<<"------------------"<<std::endl;
    std::cout<<"                length ======== : "<<length_init_<<"------------------"<<std::endl;
    std::cout<<"                Name ======== : "<<name<<"------------------"<<std::endl;

    // Get model
    model_ = _model;
    link_names_ = { robot_namespace_ };
    // Get base link
    link_ = model_->GetLink(link_name_);
    cylinder_col_ = link_->GetChildCollision(robot_namespace_+"_"+name+"_collision");

    joint_y_ = model_->GetJoint(joint_name_y_);
    joint_z_ = model_->GetJoint(joint_name_z_);



    if (link_ == NULL)
      gzthrow("Couldn't find specified link \"" << link_name_ << "\".");
    printf("Crawler cable plugin loaded: %s\n", link_name_.c_str());

    
    
    //Create node to comunicate withe other program
    node_handle_ = new ros::NodeHandle(robot_namespace_);
      //Get the lentgh commade value

    std::string id;
    std::string delimiter = "_";
    std::string namespace_ = robot_namespace_;
    namespace_.erase(0, namespace_.find(delimiter) + delimiter.length()); 
    id = namespace_.substr(0, namespace_.find(delimiter));
    /*for (unsigned i=0; i<robot_namespace_.length()-2; ++i){
       id = robot_namespace_.at(i);
    }*/
    //id = robot_namespace_.at(14);
    std::cout<<"  id == "<<id<<std::endl;

    robot_pose_sub_ = node_handle_->subscribe("/"+robot_target_+"_" + id+"/cables/targets", 1, &CrawlerCablePlugin::CallbackTargetPose, this);
    //cable_pose_sub_ = node_handle_->subscribe("/gazebo/model_states", 1, &CrawlerCablePlugin::CallbackCableName, this);

      //Send the length current value
    model_pose_pub_ = node_handle_->advertise<gazebo_msgs::LinkState>("/gazebo/set_link_state", 1);

    //Create node to change the visual of the 
    world_ = model_->GetWorld();

    node_ = transport::NodePtr(new transport::Node());
    node_->Init(world_->Name());
    pub_visual_ = node_->Advertise<msgs::Visual>("~/visual");
    pub_colision_ = node_->Advertise<msgs::Visual>("~/collision");

    // simulation iteration.
    #if GAZEBO_MAJOR_VERSION >= 8
    update_connection_ptr = event::Events::ConnectWorldUpdateBegin(std::bind(&CrawlerCablePlugin::OnUpdate, this));
    #else
    update_connection_ptr = event::Events::ConnectWorldUpdateBegin(boost::bind(&CrawlerCablePlugin::OnUpdate, this));
    #endif
    // Reset
    Reset();
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
float realTargetAngle(float a, float b){
        float angle = 0.0;
        if(a>=0.0){
            if(b>0.0){ //cas D
                angle = - atan(sqrt(pow(a,2))/sqrt(pow(b,2)));
            }
            if(b<=0.0){ //cas C
                angle = 1.5707*2 + atan(sqrt(pow(a,2))/sqrt(pow(b,2)));
            }
        }
            
        if(a<0.0){
            if(b>0.0){ //cas A
                angle = atan(sqrt(pow(a,2))/sqrt(pow(b,2)));
            }

            if(b<=0.0){ //cas B
                angle = - 1.5707*2 - atan(sqrt(pow(a,2))/sqrt(pow(b,2)));
            }
        }
        return angle;
}
void CrawlerCablePlugin::send(std::string name, std::string id){
        char buf[140];
        std::cout<<"name == "<<typeid(name).name()<<std::endl;
        std::cout<<"id == "<<typeid(id).name()<<std::endl;
        //sprintf(buf, "gnome-terminal -e 'bash -c \" roslaunch chroma_gazebo_interfaces 1_test_damien_crawler_cable.launch drone_name:=%s_%s;bash\"'", robot_.c_str(), std::to_string(i+3).c_str());
        sprintf(buf, "roslaunch chroma_gazebo_interfaces 1_test_damien_crawler_cable.launch drone_name:=%s_%s", name.c_str(), id.c_str());
        std::cout<<buf<<std::endl;
        //system("xterm -e ./xt 1");
        system(buf);
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
void CrawlerCablePlugin::OnUpdate() {

  /*std::string id;
  for (unsigned i=0; i<robot_namespace_.length(); ++i){
    id = robot_namespace_.at(i);
  }*/
  float length_max = 10;
  std::string id;
    std::string delimiter = "_";
    std::string namespace_ = robot_namespace_;
    namespace_.erase(0, namespace_.find(delimiter) + delimiter.length()); 
    namespace_.erase(0, namespace_.find(delimiter) + delimiter.length()); 
    id = namespace_.substr(0, namespace_.find(delimiter));
  //std::cout<<"SIZE == "<<sizeof(cible_.points)/sizeof(cible_.points[0])<<std::endl;
  /* ============= Nb Cible ============ */
  int nb_c = 0;
  for(auto i: cible_.points)
  {
   	nb_c++;
    //std::cout<<"nb_c == "<<nb_c<<std::endl;
  }
  /* ============= /Nb Cible ============ */
  if(have_target){
    //std::cout<<"Have target "<<std::endl;
    //std::cout<<"                                                                      rc  == "<<c<<std::endl;

    if(stoi(id)+1 > nb_c){
      //std::cout<<"                                                                      retour a 0"<<std::endl;
      ignition::math::Vector3d visual_scale(0/length_init_, 1, 1);
      gz::math::Color color =  gz::math::Color( 1.0, 1.0-(0/length_max), 1.0-(0/length_max), 1.0 );
      pub_visual_->Publish( SetScale(color, visual_scale) );
      /*msg_model_.link_name = robot_namespace_+"_cable_base_link";
      if(pose_.x != 0 || pose_.y != 0 || pose_.z != 0){
          msg_model_.pose.position.x = 0;
          msg_model_.pose.position.y = 0;
          msg_model_.pose.position.z = 0 ;
          model_pose_pub_.publish(msg_model_);
      }*/
    }
    for(int i = 0; i<nb_c; i++){
      if(stoi(id) == i){
        /* ============= length change ============ */
        float rot_y;
        float rot_z;
        
        float a = pose_.x-cible_.points[i].x;
        float b = pose_.y-cible_.points[i].y;

        float r_x_y = sqrt(pow(pose_.x-cible_.points[i].x,2)+pow(pose_.y-cible_.points[i].y,2));
        float c = pose_.z-cible_.points[i].z;

        rot_z = realTargetAngle(a, b);
        rot_y = realTargetAngle(c, r_x_y);
        if (rot_z-prev_rot_z_>0.1 or rot_z-prev_rot_z_<0.1 ){
            joint_z_->SetPosition(0, rot_z);
            prev_rot_z_=rot_z;
        }
        if (rot_y-prev_rot_y_>0.1 or rot_y-prev_rot_y_<0.1 ){
            joint_y_->SetPosition(0, rot_y);
            prev_rot_y_=rot_y;
        }
        
        
        length_ = sqrt(pow(pose_.x-cible_.points[i].x,2) + pow(pose_.y-cible_.points[i].y,2) + pow(pose_.z-cible_.points[i].z,2));

        ignition::math::Vector3d visual_scale(length_/length_init_, 1, 1);
        
        gz::math::Color color =  gz::math::Color( 1.0, 1.0-(length_/length_max), 1.0-(length_/length_max), 1.0 );
        pub_visual_->Publish( SetScale(color, visual_scale) );

        /* ============= /length change ============ */
        /* ============= pose change ============ */
        if(i>=1){
          //std::cout<<" Pose"<<robot_namespace_<<" : "<<pose_.x<<";"<<pose_.y<<";"<<pose_.z<<std::endl;
          //std::cout<<"                                                                  go to"<<robot_namespace_<<" : "<<cible_.points[i-1].x<<";"<<cible_.points[i-1].y<<";"<<cible_.points[i-1].z<<std::endl;
          if(pose_.x != cible_.points[i-1].x || pose_.y != cible_.points[i-1].y || pose_.z != cible_.points[i-1].z){
            std::cout<<std::endl;
            //std::cout<<"                                                                  Change"<<std::endl;
            std::cout<<std::endl;
            msg_model_.link_name = robot_namespace_+"_cable_base_link";
            msg_model_.pose.position.x = cible_.points[i-1].y;
            msg_model_.pose.position.y = -cible_.points[i-1].x;
            msg_model_.pose.position.z = cible_.points[i-1].z;

            model_pose_pub_.publish(msg_model_);
            
            msg_model_.link_name = robot_namespace_+"_cable_rot_z_link";
            msg_model_.pose.position.x = cible_.points[i-1].y;
            msg_model_.pose.position.y = -cible_.points[i-1].x;
            msg_model_.pose.position.z = cible_.points[i-1].z;

            model_pose_pub_.publish(msg_model_);

pose_.x =cible_.points[i-1].x;
            pose_.y =cible_.points[i-1].y;
            pose_.z =cible_.points[i-1].z;
          }
}
            /* ============= ============ */ 
            /* ============= ============ */ 

            if (parent_frame_.empty() ){
              parent_frame_="true_world";
            }
            //std::cout<<"Frame "<<parent_frame_<<std::endl;
            /*static tf2_ros::TransformBroadcaster br;
            geometry_msgs::TransformStamped transformStamped;
            transformStamped.header.stamp = ros::Time::now();
           transformStamped.header.frame_id = parent_frame_;
           transformStamped.child_frame_id = robot_namespace_+"_cable_base_link";
           transformStamped.transform.translation.x = cible_.points[i-1].x;
           transformStamped.transform.translation.y = cible_.points[i-1].y;
           transformStamped.transform.translation.z = cible_.points[i-1].z;
           tf2::Quaternion q;
           q.setRPY(0, rot_y, 1.5707+rot_z);
           transformStamped.transform.rotation.x = q.x();
           transformStamped.transform.rotation.y = q.y();
           transformStamped.transform.rotation.z = q.z();
           transformStamped.transform.rotation.w = q.w();
         
           br.sendTransform(transformStamped);

           transformStamped.header.stamp = ros::Time::now();
           transformStamped.header.frame_id = parent_frame_;
           transformStamped.child_frame_id = robot_namespace_+"_cable_rot_z_link";
           transformStamped.transform.translation.x = cible_.points[i-1].x;
           transformStamped.transform.translation.y = cible_.points[i-1].y;
           transformStamped.transform.translation.z = cible_.points[i-1].z;

           transformStamped.transform.rotation.x = q.x();
           transformStamped.transform.rotation.y = q.y();
           transformStamped.transform.rotation.z = q.z();
           transformStamped.transform.rotation.w = q.w();
         
           br.sendTransform(transformStamped);

           transformStamped.header.stamp = ros::Time::now();
           transformStamped.header.frame_id = parent_frame_;
           transformStamped.child_frame_id = robot_namespace_+"_cable_link";
           transformStamped.transform.translation.x = cible_.points[i-1].x;
           transformStamped.transform.translation.y = cible_.points[i-1].y;
           transformStamped.transform.translation.z = cible_.points[i-1].z;

           transformStamped.transform.rotation.x = q.x();
           transformStamped.transform.rotation.y = q.y();
           transformStamped.transform.rotation.z = q.z();
           transformStamped.transform.rotation.w = q.w();
         
           br.sendTransform(transformStamped);*/
            /* ============= ============ */ 
            /* ============= ============ */ 



            /* ============= ============ */ 

            
          
        
        /* ============= /pose change ============ */  
        /* ============= posible new cable segment ============ */  
        //std::cout<<"        link name == "<<std::endl;
        if ((i+2)<=nb_c){
          
          int nb_list = 0;
          int l = link_names_.size(); 
          //std::cout<<"length == "<<l<<std::endl;
          /*for (std::string name : link_names_) {
            std::string name_bis = robot_+"_"+std::to_string(i+1);
            if(name != name_bis){
              nb_list++;
            }
          }*/ 
          if(nb_c+2 >= l && new_){
            //std::cout<<"            Add new " <<robot_.c_str() <<std::endl;
            //std::cout<<"length == "<<l<<std::endl;
            //std::cout<<"nb_c == "<<nb_c<<std::endl;
            //std::cout<<"new == "<<new_<<std::endl;
            new_=0;
            
            
            std::string name = robot_.c_str();
            std::string id = std::to_string(i+3).c_str();

            //boost::thread send_thread = boost::thread(&CrawlerCablePlugin::send, this, name, id);
            //send_thread.join();

            //sprintf(buf, "gnome-terminal -e 'bash -c \" roslaunch chroma_gazebo_interfaces 1_test_damien_crawler_cable.launch drone_name:=%s_%s;bash\"'", robot_.c_str(), std::to_string(i+3).c_str());
            //sprintf(buf, "roslaunch chroma_gazebo_interfaces 1_test_damien_crawler_cable.launch drone_name:=%s_%s", robot_.c_str(), std::to_string(i+3).c_str());
            //std::cout<<buf<<std::endl;
            //system("xterm -e ./xt 1");
            //system(buf);
            //std::cout<<"new == "<<new_<<std::endl;
          }
          

        }
        /*for (std::string name : link_names_) {
          std::cout<<"        link name == "<<name<<std::endl;
        }*/
        /* ============= /posible new cable segment ============ */  
      }
    }
  }
  
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                                    Change the scale of the link
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
gazebo::msgs::Visual CrawlerCablePlugin::SetScale( gz::math::Color newColor, ignition::math::Vector3d scale_)
    {
        std::string visual_name_ = "link_visual";
        std::string linkName = link_->GetName();
        physics::LinkPtr link_ = this->model_->GetLink(linkName);

        msgs::Visual visualMsg = link_->GetVisualMessage(visual_name_);
        msgs::Vector3d* scale_factor = new msgs::Vector3d{msgs::Convert(scale_)};

        visualMsg.set_name(link_->GetScopedName());
        visualMsg.set_parent_name(this->model_->GetScopedName());
        visualMsg.set_allocated_scale(scale_factor);

        if ((!visualMsg.has_material()) || visualMsg.mutable_material() == NULL) {
            msgs::Material *materialMsg = new msgs::Material;
            visualMsg.set_allocated_material(materialMsg);
        }
         // Set color
        /*gazebo::msgs::Color *colorMsg = new gazebo::msgs::Color(gazebo::msgs::Convert(newColor));
        gazebo::msgs::Color *diffuseMsg = new gazebo::msgs::Color(*colorMsg);

        gazebo::msgs::Material *materialMsg = visualMsg.mutable_material();
        if (materialMsg->has_ambient())
        {
          materialMsg->clear_ambient();
        }
        materialMsg->set_allocated_ambient(colorMsg);
        if (materialMsg->has_diffuse())
        {
          materialMsg->clear_diffuse();
        }
        materialMsg->set_allocated_diffuse(diffuseMsg);*/
        return visualMsg;
    }
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                                    CallBack target pose + cable pose 
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
void CrawlerCablePlugin::CallbackTargetPose(const chroma_gazebo_plugins_msgs::Points _cible_msg) {
  cible_ = _cible_msg;
  have_target = 1;
}

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
/*void CrawlerCablePlugin::CallbackCableName(const gazebo_msgs::ModelStates _state) {
  
  std::string delimiter = "_";
  std::string rob_namespace = robot_namespace_;

  std::string rob_names = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
  rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
  std::string rob_names_2 = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
  rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
  std::string rob_num = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
  robot_ = rob_names+"_"+rob_names_2+"_"+rob_num;
  
  int nb_cable = link_names_.size();
  int nb = 0;
  for(std::string i: _state.name){
    rob_namespace = i;
    rob_names = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
    rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
    rob_names_2 = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
    rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
    rob_num = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
    
    if (robot_ == rob_names+"_"+rob_names_2+"_"+rob_num){
      nb +=1 ;
    }
  }
  
  if (nb!=nb_cable-1){
    std::cout<<nb<<" == "<<nb_cable<<std::endl;
    link_names_ = { robot_namespace_ };
    for(std::string i: _state.name){
      
        rob_namespace = i;
        rob_names = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
        rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
        rob_names_2 = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
        rob_namespace.erase(0, rob_namespace.find(delimiter) + delimiter.length());
        rob_num = rob_namespace.substr(0, rob_namespace.find(delimiter)); 
        if (robot_ == rob_names+"_"+rob_names_2+"_"+rob_num){
          //std::cout<<"              link name added == "<<i<<std::endl;
          link_names_.push_back(i);
          new_=1;
        }
      
      
    }
  }

}*/
