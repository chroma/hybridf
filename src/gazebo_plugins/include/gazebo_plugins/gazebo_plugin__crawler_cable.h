/*
 * Copyright (C) 2014-2016 Open Source Robotics Foundation
 * Copyright 2020 Cedric Pradalier, Associate Prof. GeorgiaTech Lorraine
 * Copyright 2020 Vincent LE DOZE, INRIA, Chroma Team, CITI Lab Lyon, France
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef GAZEBO_PLUGINS_MAGNETIC_WHEEL_H_
#define GAZEBO_PLUGINS_MAGNETIC_WHEEL_H_

#include "gazebo_plugins/common.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "chroma_gazebo_plugins_msgs/Points.h"
#include <list>
#include <gazebo/common/common.hh>

#include <chrono>
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>
#include <thread>

#include <chroma_gazebo_plugins_msgs/Float.h>
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/ModelStates.h>
namespace gazebo
{

/// \brief A plugin that simulate a crawler movements
class GAZEBO_VISIBLE CrawlerCablePlugin : public ModelPlugin {

  /// Methods ////////////////////////////////////////////////////////
  public:
    CrawlerCablePlugin();  /// \brief Constructor.
    ~CrawlerCablePlugin(); /// \brief Destructor.

  protected:
    /// \brief Load model
    virtual void Load(physics::ModelPtr _in_model, sdf::ElementPtr _in_sdf);
    /// \brief Reset Function
    virtual void Reset();
    /// \brief Callback for World Update events.
    virtual void OnUpdate();
    void CallbackTargetPose(const chroma_gazebo_plugins_msgs::Points _cible_msg);
    void CallbackCableName(const gazebo_msgs::ModelStates _state);
    void send(std::string name, std::string id);
    
    
    //void CallbackPose(const chroma_gazebo_plugins_msgs::Float _pose);
    void URDF(physics::LinkPtr link);
    msgs::Visual SetScale( gz::math::Color newColor, ignition::math::Vector3d scale_);

  /// Attributes /////////////////////////////////////////////////////
  protected:
    /// Gazebo related ///////////////////////////////////////////////
    physics::ModelPtr model_;
    physics::WorldPtr world_;
    physics::LinkPtr link_;
    physics::CollisionPtr cylinder_col_;
    physics::CylinderShapePtr cylinder(physics::CylinderShape(physics::CollisionPtr));
    
    physics::JointPtr joint_x_;
    physics::JointPtr joint_y_;
    physics::JointPtr joint_z_;
    
    
    
    physics::LinkPtr child_link_;
    physics::LinkPtr parent_link_;
    transport::NodePtr node_;
    
    /// Paramters ////////////////////////////////////////////////////
    float length_;
    float length_init_;
    float robot_pose_x;
    float robot_pose_y;
    float robot_pose_z;
    
    float cur_x;
    float cur_y;
    float cur_z;
    
    float prev_rot_z_;
    float prev_rot_y_;
    
    
    std::list<std::string> link_names_;
    std::string link_name_;
    
    std::string robot_;
    std::string joint_name_x_;
    std::string joint_name_y_;
    std::string joint_name_z_;
   
    std::string visual_name_;
    std::string robot_target_;
    std::string robot_namespace_;
    std::string parent_frame_ = "world";
    std::string odom_topic_;
    std::string length_topic_;
    
    bool new_ = 0;
    bool have_target = 0;

  private:
    /// \brief Connection to World Update events.
    event::ConnectionPtr update_connection_ptr;
    
    ros::NodeHandle*   node_handle_;
    
    ros::Subscriber    robot_pose_sub_;
    ros::Subscriber    cable_pose_sub_;

    ros::Publisher     model_pose_pub_;
    
    transport::PublisherPtr pub_visual_;
    transport::PublisherPtr pub_colision_;
    
    chroma_gazebo_plugins_msgs::Points cible_;
    
    geometry_msgs::Point pose_;

    gazebo_msgs::LinkState msg_model_;

};

}
#endif
