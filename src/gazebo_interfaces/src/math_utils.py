#!/usr/bin/python
# coding: utf-8

# Auteur : vledoze
# Date   : 22/11/2017

# ##IMPORTATIONS =======================================================
# Importations globales
import math
import numpy
import numpy.random
import unittest

# Importations locales
# RAS

# Messages ROS
# RAS

# ##CONSTANTES =========================================================
R2D = 180.0/math.pi
D2R = math.pi/180.0
G = 9.81

__NTEST = 100000


# ##FONCTIONS ==========================================================
def quaternion2euler(q):
    """ Conversion des quaternions vers les angles d'euler
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        input 1  :
            q = [q0, q1, q2, q3] - Quaternions
        input 2  :
            q = [q2, q3] - Quaternions simplifiés
        --------------------------------------
        output :
            psi, tet, phi - Angles d'Euler
    """
    # Cas input 1
    if (len(q) == 4):
        # Variables intermediaires
        q0 = q[0]
        q1 = q[1]
        q2 = q[2]
        q3 = q[3]
        # Conversion
        phi = math.atan2(2.0*(q3*q0 + q1*q2), 1.0 - 2.0*(q0*q0 + q1*q1))
        tet = math.asin(2.0*(q3*q1 - q0*q2))
        psi = math.atan2(2.0*(q3*q2 + q0*q1), 1.0 - 2.0*(q1*q1 + q2*q2))
        # Sortie
        return psi, tet, phi
    # Cas input 2
    if (len(q) == 2):
        # Cap
        psi = quaternion2psi(q)
        # Sortie
        return psi, 0.0, 0.0
    # Sortie par defaut
    return None, None, None


def quaternion2tetphi(q):
    """ Conversion des quaternions vers les angles d'euler
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        input :
            q = [q0, q1, q2, q3] - Quaternions
        -------------------------------------
        output :
            tet, phi - Angles d'Euler
    """
    # Verification
    if (len(q) == 4):
        # Variables intermediaires
        q0 = q[0]
        q1 = q[1]
        q2 = q[2]
        q3 = q[3]
        # Conversion
        phi = math.atan2(2.0*(q3*q0 + q1*q2), 1.0 - 2.0*(q0*q0 + q1*q1))
        tet = math.asin(2.0*(q3*q1 - q0*q2))
        # Sortie
        return tet, phi
    # Sortie par defaut
    return None, None


def quaternion2psi(q):
    """ Conversion des quaternions vers les angles d'euler
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        input 1 :
            q = [q2, q3] - Quaternions simplifié
        input 2 :
            q = [q0, q1, q2, q3] - Quaternions
        ------------------------------------------
        output :
            psi - Angles d'Euler (cap)
    """
    # Cas input 1
    if (len(q) == 2):
        # Conversion
        psi = math.atan2(2.0*q[1]*q[0], 1.0 - 2.0*q[0]*q[0])
        # Sortie
        return psi
    # Cas input 2
    if (len(q) == 4):
        # Conversion
        psi = math.atan2(2.0*(q[0]*q[1] + q[3]*q[2]), 1.0 - 2.0*(q[1]*q[1] + q[2]*q[2]))
        # Sortie
        return psi
    # Code d'erreur par defaut
    return None


def euler2quaternion(psi, tet, phi):
    """ Conversion des angles d'eulers vers les quaternions
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        in  :
            psi, tet, phi - Angles d'Euler
        out :
            q = [q0, q1, q2, q3] - Quaternions
    """
    # Variables intermédiaires
    cospsi = math.cos(psi * 0.5)
    sinpsi = math.sin(psi * 0.5)
    costet = math.cos(tet * 0.5)
    sintet = math.sin(tet * 0.5)
    cosphi = math.cos(phi * 0.5)
    sinphi = math.sin(phi * 0.5)
    # Conversions
    q0 = sinphi*costet*cospsi - cosphi*sintet*sinpsi
    q1 = cosphi*sintet*cospsi + sinphi*costet*sinpsi
    q2 = cosphi*costet*sinpsi - sinphi*sintet*cospsi
    q3 = cosphi*costet*cospsi + sinphi*sintet*sinpsi
    # Sortie
    return quaternionnormalize([q0, q1, q2, q3])  # if normalize else [q0, q1, q2, q3]


def tetphi2quaternion(tet, phi):
    """ Conversion des angles d'eulers vers les quaternions
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        in  :
            tet, phi - Angles d'Euler (Assiette, Gite)
        out :
            q = [q0, q1, q2, q3] - Quaternions
    """
    # Variables intermédiaires
    costet = math.cos(tet * 0.5)
    sintet = math.sin(tet * 0.5)
    cosphi = math.cos(phi * 0.5)
    sinphi = math.sin(phi * 0.5)
    # Conversions
    q0 = sinphi*costet
    q1 = cosphi*sintet
    q2 = -sinphi*sintet
    q3 = cosphi*costet
    # Sortie
    return quaternionnormalize([q0, q1, q2, q3])  # if normalize else [q0, q1, q2, q3]


def psi2quaternion(psi):
    """ Conversion des angles d'eulers vers les quaternions
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        in  :
            psi - Angle d'Euler (cap)
        out :
            q = [q2, q3] - Quaternions
    """
    # Variables intermédiaires
    cospsi = math.cos(psi * 0.5)
    sinpsi = math.sin(psi * 0.5)
    # Conversions
    q2 = sinpsi
    q3 = cospsi
    # Sortie
    return [q2, q3]


def euler2matrix(psi, tet, phi):
    """ Calcul de la matrice de rotation à partir des angles d'euler

        input :
            psi, tet, phi - Angles d'Euler
        output :
            rot - matrice de rotation (numpy.matrix)
    """
    # Matrices interediaires
    rot_psi = numpy.matrix([
        [math.cos(psi), -math.sin(psi), 0.0],
        [math.sin(psi), math.cos(psi), 0.0],
        [0.0, 0.0, 1.0]])
    rot_tet = numpy.matrix([
        [math.cos(tet), 0.0, math.sin(tet)],
        [0.0, 1.0, 0.0],
        [-math.sin(tet), 0.0, math.cos(tet)]])
    rot_phi = numpy.matrix([
        [1.0, 0.0, 0.0],
        [0.0, math.cos(phi), -math.sin(phi)],
        [0.0, math.sin(phi), math.cos(phi)]])
    # Sortie
    return rot_psi*rot_tet*rot_phi


def matrix2euler(mat):
    """ Calcul des angles d'euler associés à une matrice de rotation
    """
    psi = math.atan2(mat[1, 0], mat[0, 0])
    tet = math.asin(-mat[2, 0])
    phi = math.atan2(mat[2, 1], mat[2, 2])
    return psi, tet, phi


def psi2matrix(psi):
    """ Calcul de la matrice de rotation à partir de l'angle d'euler psi

        input :
            psi - Angles d'Euler autour de l'axe vertical
        output :
            rot - matrice de rotation (numpy.matrix)
    """
    # Matrices interediaires
    rot_psi = numpy.matrix([
        [math.cos(psi), -math.sin(psi), 0.0],
        [math.sin(psi), math.cos(psi), 0.0],
        [0.0, 0.0, 1.0]])
    # Sortie
    return rot_psi


def quaternion2matrix(q):
    """ Calcul de la matrice de rotation à partir des quaternion
        https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        avec q0 -> q3, q1 ->q0, q2->q1, q3->q2
        soit q0.i + q1*j + q2*k + q3

        input :
            q = [q0, q1, q2, q3] - Quaternions
        output :
            rot - matrice de rotation (numpy.matrix)
    """
    # Variables intermediaires
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    # Matrice de rotation
    rot = numpy.matrix([
        [q3*q3 + q0*q0 - q1*q1 - q2*q2, 2.0*(q0*q1 - q3*q2), 2.0*(q3*q1 + q0*q2)],
        [2.0*(q0*q1 + q3*q2), q3*q3 - q0*q0 + q1*q1 - q2*q2, 2.0*(q1*q2 - q3*q0)],
        [2.0*(q0*q2 - q3*q1), 2.0*(q3*q0 + q1*q2), q3*q3 - q0*q0 - q1*q1 + q2*q2]])
    # Sortie
    return rot


def rotatewitheuler(v, psi, tet, phi):
    """ Rotation avec les angles d'euler

        input :
            v - vecteur d'entree (numpy.array)
            psi, tet, phi - angles d'Euler
        output :
            u - vecteur de sortie (numpy.array)
    """
    rot = euler2matrix(psi, tet, phi)
    u = rot*numpy.matrix(v).T
    return [u[0, 0], u[1, 0], u[2, 0]]


def rotatewithpsi(v, psi):
    """ Rotation avec l'angles d'euler psi

        input :
            v - vecteur d'entree (numpy.array)
            psi - angle d'Euler autour de l'axe vertical
        output :
            u - vecteur de sortie (numpy.array)
    """
    rot = psi2matrix(psi)
    u = rot*numpy.matrix(v).T
    return [u[0, 0], u[1, 0], u[2, 0]]


def rotatewithquaternion(v, q, method3D=True):
    """ Rotation d'un vecteur avec les quaternion
        q0.i + q1*j + q2*k + q3

        input 1 :
            v - vecteur d'entree (numpy.array)
            q = [q0, q1, q2, q3] - Quaternions
        input 2 :
            v - vecteur d'entree (numpy.array)
            q = [q2, q3] - Quaternions simplifiés
        output :
            u - vecteur de sortie (numpy.array)
    """
    # Cas Input 1 ou Input 2
    if (len(v) == 3) & (len(q) == 4):
        if method3D:
            # Variables intermediaires
            w = [q[0], q[1], q[2]]
            t = 2.0*numpy.cross(w, v)
            # Calculs
            u = v + q[3]*t + numpy.cross(w, t)
        else:
            u = quaternionproduct(quaternionproduct(q, v), quaternionconj(q))
            u.pop(-1)
        # Sortie
        return u
    # Cas Input 2
    if (len(v) == 3) & (len(q) == 2):
        if method3D:
            # Variables intermediaires
            w = [0.0, 0.0, q[0]]
            t = 2.0*numpy.cross(w, v)
            # Calculs
            u = v + q[1]*t + numpy.cross(w, t)
        else:
            u = quaternionproduct(quaternionproduct(q, v), quaternionconj(q))
            u.pop(-1)
        # Sortie
        return u
    # Code d'erreur par defaut
    return None


def quaternionconj(q):
    """ Calcul du conjugué du quaternion
        q0.i + q1*j + q2*k + q3

        input 1 :
            q = [q0, q1, q2, q3] - Quaternions
        input 2 :
            q = [q2, q3] - Quaternions simplifiés
        -----------------------------------------
        output 1 :
            q* = [-q0, -q1, -q2, q3] - Quaternions
        output 2 :
            q* = [-q2, q3] - Quaternions simplifiés
    """
    # Cas input 1
    if len(q) == 2:
        return [-q[0], q[1]]
    # Cas input 2
    if len(q) == 4:
        return [-q[0], -q[1], -q[2], q[3]]
    # Code d'erreur par defaut
    return None


def quaternioninv(q):
    """ Calcul de l'inverse d'un quaternion
        q0.i + q1*j + q2*k + q3

        input 1 :
            q = [q0, q1, q2, q3] - Quaternions
        input 2:
            q = [q2, q3] - Quaternions simplifiés
        --------------------------------------
        output 1 :
            q-1 = [-q0/nq, -q1/nq, -q2/nq, q3/nq] - Quaternions
        output 2 :
            q-1 = [-q2/nq, q3/nq] - Quaternions simplifié
    """
    # Cas input 1
    if len(q) == 4:
        n = q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]
        return [-q[0]/n, -q[1]/n, -q[2]/n, q[3]/n]
    # Cas input 2
    if len(q) == 2:
        n = q[0]*q[0] + q[1]*q[1]
        return [-q[0]/n, q[1]/n]
    # Code d'erreur par defaut
    return None


def quaternionproduct(q1, q2):
    """ Defini le produit de hamilton pour deux quaternions
        input 1 :
            q1 = [q01, q11, q21, q31] - Quaternion
            q2 = [q02, q12, q22, q32] - Quaternion
        input 2 :
            q1 = [q01, q11, q21, q31] - Quaternion
            q2 = [q02, q12, q22] - Vecteur
        input 3 :
            q1 = [q01, q11, q21] - Vecteur
            q2 = [q02, q12, q22, q32] - Quaternion
        input 4 :
            q1 = [q21, q31] - Quaternion simplifié
            q2 = [q22, q32] - Quaternion simplifié
        input 5 :
            q1 = [q21, q31] - Quaternion simplifié
            q2 = [q02, q12, q22] - Vecteur
        input 6 :
            q1 = [q01, q11, q21] - Vecteur
            q2 = [q22, q32] - Quaternion simplifié
        -----------------------------------------
        output 1:
            q = (q01.i + q11*j + q21*k + q31)(q02.i + q12*j + q22*k + q32)
              = [q1, q2, q3, q4] - Quaternion
        output 2:
            q = (q01.i + q11*j + q21*k + q31)(q02.i + q12*j + q22*k)
              = [q1, q2, q3, q4] - Quaternion
        output 3:
            q = (q01.i + q11*j + q21*k)(q02.i + q12*j + q22*k + q32)
              = [q1, q2, q3, q4] - Quaternion
        output 4:
            q = (q21*k + q31)(q22*k + q32)
              = [q3, q4] - Quaternion simplifié
        output 5:
            q = (q21*k + q31)(q02.i + q12*j + q22*k)
              = [q1, q2, q3, q4] - Quaternion
        output 6:
            q = (q01.i + q11*j + q21*k)(q22*k + q32)
              = [q1, q2, q3, q4] - Quaternion
    """
    # cas input 1
    if (len(q1) == len(q2) == 4):
        # Variables intermediaires
        q01 = q1[0]
        q11 = q1[1]
        q21 = q1[2]
        q31 = q1[3]
        q02 = q2[0]
        q12 = q2[1]
        q22 = q2[2]
        q32 = q2[3]
        # Calculs
        q0 = q31*q02 + q01*q32 + q11*q22 - q21*q12
        q1 = q31*q12 - q01*q22 + q11*q32 + q21*q02
        q2 = q31*q22 + q01*q12 - q11*q02 + q21*q32
        q3 = q31*q32 - q01*q02 - q11*q12 - q21*q22
        # Sortie
        q = [q0, q1, q2, q3]
        return quaternionnormalize(q)
    # Cas input 2
    if (len(q1) == 4) & (len(q2) == 3):
        # Variables intermediaires
        q01 = q1[0]
        q11 = q1[1]
        q21 = q1[2]
        q31 = q1[3]
        q02 = q2[0]
        q12 = q2[1]
        q22 = q2[2]
        # Calculs
        q0 = q31*q02 + q11*q22 - q21*q12
        q1 = q31*q12 - q01*q22 + q21*q02
        q2 = q31*q22 + q01*q12 - q11*q02
        q3 = -q01*q02 - q11*q12 - q21*q22
        # Sortie
        q = [q0, q1, q2, q3]
        return q
    # Cas input 3
    if (len(q1) == 3) & (len(q2) == 4):
        # Variables intermediaires
        q01 = q1[0]
        q11 = q1[1]
        q21 = q1[2]
        q02 = q2[0]
        q12 = q2[1]
        q22 = q2[2]
        q32 = q2[3]
        # Calculs
        q0 = q01*q32 + q11*q22 - q21*q12
        q1 = -q01*q22 + q11*q32 + q21*q02
        q2 = q01*q12 - q11*q02 + q21*q32
        q3 = -q01*q02 - q11*q12 - q21*q22
        # Sortie
        q = [q0, q1, q2, q3]
        return q
    # Cas input 4
    if (len(q1) == len(q2) == 2):
        # Variables intermediaires
        q21 = q1[0]
        q31 = q1[1]
        q22 = q2[0]
        q32 = q2[1]
        # Calculs
        q2 = q31*q22 + q21*q32
        q3 = q31*q32 - q21*q22
        # Sortie
        q = [q2, q3]
        return quaternionnormalize(q)
    # Cas input 5
    if (len(q1) == 2) & (len(q2) == 3):
        # Variables intermediaires
        q21 = q1[0]
        q31 = q1[1]
        q02 = q2[0]
        q12 = q2[1]
        q22 = q2[2]
        # Calculs
        q0 = q31*q02 - q21*q12
        q1 = q31*q12 + q21*q02
        q2 = q31*q22
        q3 = -q21*q22
        # Sortie
        q = [q0, q1, q2, q3]
        return q
    # Cas input 6
    if (len(q1) == 3) & (len(q2) == 2):
        # Variables intermediaires
        q01 = q1[0]
        q11 = q1[1]
        q21 = q1[2]
        q22 = q2[0]
        q32 = q2[1]
        # Calculs
        q0 = q01*q32 + q11*q22
        q1 = q11*q32 - q01*q22
        q2 = q21*q32
        q3 = -q21*q22
        # Sortie
        q = [q0, q1, q2, q3]
        return q
    # Code d'erreur par defaut
    return None


def quaternionnormalize(q):
    """ Retourne le quaternion normé

        input 1 :
            q1 = [q0, q1, q2, q3] - Quaternions
        input 2 :
            q1 = [q2, q3] - Quaternions simplifiés
        ----------------------------------------
        output 1 :
            q = [q0/n, q1/n, q2/n, q3/n] - Quaternions
        output 2 :
            q = [q2/n, q3/n] - Quaternions simplifiés
    """
    # Cas input 1
    if (len(q) == 4):
        # Variable intermediaire
        q0 = q[0]
        q1 = q[1]
        q2 = q[2]
        q3 = q[3]
        # Calculs
        n = math.sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)
        # Sortie
        return [q0/n, q1/n, q2/n, q3/n]
    # Cas input 2
    if (len(q) == 2):
        # Variable intermediaire
        q2 = q[0]
        q3 = q[1]
        # Calculs
        n = math.sqrt(q2*q2 + q3*q3)
        # Sortie
        return [q2/n, q3/n]


def quaterniondiff(q1, q2):
    """ Difference entre deux quaternions

        input 1:
            q1 = [q01, q11, q21, q31] - Quaternions
            q2 = [q02, q12, q22, q32] - Quaternions
        input 2:
            q1 = [q21, q31] - Quaternions simplifié
            q2 = [q22, q32] - Quaternions simplifié
        ---------------------------------------
        output :
            q = [q0, q1, q2, q3] - Quaternions
    """
    if (len(q1) == len(q2) == 4) | (len(q1) == len(q2) == 2):
        # Sortie
        return quaternionproduct(q1, quaternionconj(q2))
    # Sortie par defaut
    return None


def minmax(v, min_v=-1.0, max_v=1.0):
    """ Borne une valeur entre -1 et 1 """
    return max(min_v, min(max_v, float(v)))


def mean(vlist):
    """ Calcul la valeur moyenne d'une liste """
    m = 0.0
    n = float(len(vlist))
    if n > 0:
        for v in vlist:
            m += v
        return m/n
    else:
        return 0.0


def l1_dist(vlist1, vlist2):
    """ Calcul la distance l1 entre deux listes """
    d = 0.0
    for v1, v2 in zip(vlist1, vlist2):
        d += (v1 - v2)
    return d


def l2_dist(vlist1, vlist2):
    """ Calcul la distance l2 entre deux listes """
    d = 0.0
    for v1, v2 in zip(vlist1, vlist2):
        d += (v1 - v2)*(v1 - v2)
    return d


def euclidian_dist(vlist1, vlist2):
    """ Calcul la distance euclidienne entre deux listes """
    d = 0.0
    for v1, v2 in zip(vlist1, vlist2):
        d += (v1 - v2)*(v1 - v2)
    return math.sqrt(d)


def l1_norm(vlist):
    """ Calcul la norme l1 d'une liste """
    n = 0.0
    for v in vlist:
        n += v
    return n


def euclidian_norm(vlist):
    """ Calcul la norme euclidienne d'une liste """
    n = 0.0
    for v in vlist:
        n += v*v
    return math.sqrt(n)


def round_to_nearest(x, precision):
    """ Arrondi un nombre à la precision demandée """
    correction = 0.5 if x >= 0 else -0.5
    return int(x/precision+correction) * precision


def update_tree_points(list_points, tree_points, drone_name):
    """ Mise a jour d'un arbre de points avec une liste de nouveaux points
        Retourne la liste des nouveaux points
    """
    # Liste de points uniques / rajoutés
    list_points_uniques = []
    # Parcours des nouveaux points
    for point in list_points:
        if point.x in tree_points.keys():
            if point.y in tree_points[point.x].keys():
                if not (point.z in tree_points[point.x][point.y].keys()):
                    # Nouveau point : on stocke le nom du decouvreur
                    tree_points[point.x][point.y][point.z] = drone_name
                    list_points_uniques.append(point)
            else:
                # Nouveau point : on stocke le nom du decouvreur
                tree_points[point.x][point.y] = {point.z: drone_name}
                list_points_uniques.append(point)
        else:
            # Nouveau point : on stocke le nom du decouvreur
            tree_points[point.x] = {point.y: {point.z: drone_name}}
            list_points_uniques.append(point)
    # Retourne la liste des points uniques / rajoutés
    return list_points_uniques


def searchn_tree_points(list_points, tree_points, count=False):
    """ On recherche si un des points de la liste est déja decouvert
        Sortie si count=False
            True : au moins 1 point connu
            False : aucun points
        Sortie si count=True
            Nombre de points deja decouverts dans la liste
    """
    # Compteur
    if count:
        n = 0
    # Parcours des nouveaux points
    for point in list_points:
        if point.x in tree_points.keys():
            if point.y in tree_points[point.x].keys():
                if point.z in tree_points[point.x][point.y].keys():
                    if count:
                        n += 1
                    else:
                        return True
    # On retourne le nombre de points trouvés / Faux
    if count:
        return n
    else:
        return False


def search_tree_points(point, tree_points):
    """ On recherche si un point a deja déja decouvert
        Sortie si count=False
            True : Le point est connu
            False : Le point n'est pas connu
    """
    if point.x in tree_points.keys():
        if point.y in tree_points[point.x].keys():
            if point.z in tree_points[point.x][point.y].keys():
                return True
    return False


def SigmoidLin(x,p,v_max,r0):
    vel = (r0 - x) * p
    if (p <= 0):
        return 0
    if (vel >= v_max):
        return v_max
    return vel


def SigmoidLike(x,R,d):
    if (x < R):
        return 0
    elif (x < R+d):
        return numpy.sin((math.pi * pow(d,-1.0)) * (x-R) - (math.pi / 2)) + 1.0
    else:
        return 1.0


def VelDecayLinSqrt(x,p,a,v_max,r0):
    vel = (x - r0) * p
    if (a<=0 or p<=0 or vel<=0):
        return 0
    if (vel<a/p):
        if (vel>=v_max):
            return v_max
        return vel
    vel = math.sqrt(2*a*(x - r0) - a**2/p**2)

def perpendicular( a ) :
    b = numpy.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b



# ##CLASSES ============================================================
class TestQuaternions(unittest.TestCase):

    def test_conversion(self):
        # Sens Euler -> Quaternion
        for i in range(__NTEST):
            psi = math.pi*numpy.random.uniform()
            tet = math.pi/2.0*numpy.random.uniform()
            phi = math.pi/2.0*numpy.random.uniform()
            q = euler2quaternion(psi, tet, phi)
            psi_, tet_, phi_ = quaternion2euler(q)
            self.assertAlmostEqual(psi_, psi)
            self.assertAlmostEqual(tet_, tet)
            self.assertAlmostEqual(phi_, phi)
        # Sens Quaternion -> Euler
        for i in range(__NTEST):
            q = quaternionnormalize(numpy.random.random_sample((4,)))
            psi, tet, phi = quaternion2euler(q)
            q_ = euler2quaternion(psi, tet, phi)
            for j in range(len(q)):
                self.assertAlmostEqual(q_[j], q[j])

    def test_rotation(self):
        # Sens Euler -> Quaternion
        for i in range(__NTEST):
            psi = math.pi*numpy.random.uniform()
            tet = math.pi/2.0*numpy.random.uniform()
            phi = math.pi/2.0*numpy.random.uniform()
            v = numpy.random.random_sample((3,))
            v1 = rotatewitheuler(v, psi, tet, phi)
            q = euler2quaternion(psi, tet, phi)
            v2 = rotatewithquaternion(v, q)
            v3 = rotatewithquaternion(v, q, method3D=False)
            for j in range(len(v1)):
                self.assertAlmostEqual(v1[j], v2[j])
                self.assertAlmostEqual(v1[j], v3[j])
        # Sens Quaternion->Euler
        for i in range(__NTEST):
            q = quaternionnormalize(numpy.random.random_sample((4,)))
            v = numpy.random.random_sample((3,))
            v1 = rotatewithquaternion(v, q)
            v1bis = rotatewithquaternion(v, q, method3D=False)
            psi, tet, phi = quaternion2euler(q)
            v2 = rotatewitheuler(v, psi, tet, phi)
            for j in range(len(v1)):
                self.assertAlmostEqual(v1[j], v2[j])
                self.assertAlmostEqual(v1bis[j], v2[j])

    def test_quaternionconj(self):
        for i in range(__NTEST):
            psi = math.pi*numpy.random.uniform()
            tet = math.pi/2.0*numpy.random.uniform()
            phi = math.pi/2.0*numpy.random.uniform()
            q = euler2quaternion(psi, tet, phi)
            qc = quaternionconj(q)
            psi, tet, phi = quaternion2euler(quaternionproduct(q, qc))
            self.assertAlmostEqual(psi, 0.0)
            self.assertAlmostEqual(tet, 0.0)
            self.assertAlmostEqual(phi, 0.0)

    def test_quaternioninv(self):
        for i in range(__NTEST):
            psi = math.pi*numpy.random.uniform()
            tet = math.pi/2.0*numpy.random.uniform()
            phi = math.pi/2.0*numpy.random.uniform()
            q = euler2quaternion(psi, tet, phi)
            qi = quaternioninv(q)
            psi, tet, phi = quaternion2euler(quaternionproduct(q, qi))
            self.assertAlmostEqual(psi, 0.0)
            self.assertAlmostEqual(tet, 0.0)
            self.assertAlmostEqual(phi, 0.0)

    def test_quaternioninv2(self):
        for i in range(__NTEST):
            q = [
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform()]
            qi = quaternioninv(q)
            psi, tet, phi = quaternion2euler(quaternionproduct(q, qi))
            self.assertAlmostEqual(psi, 0.0)
            self.assertAlmostEqual(tet, 0.0)
            self.assertAlmostEqual(phi, 0.0)

    def test_quaternioninv3(self):
        for i in range(__NTEST):
            q = [
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform(),
                1000*numpy.random.uniform()]
            qi = quaternioninv(q)
            qc = quaternionconj(q)
            qic = quaternionconj(qi)
            qci = quaternioninv(qc)
            for qic_k, qci_k in zip(qci, qic):
                self.assertAlmostEqual(qic_k, qci_k)


# ##MAIN ===============================================================
if __name__ == "__main__":
    # Tests Quaternions
    unittest.main()
