#!/usr/bin/env python3
# coding: utf-8

# Auteur : vdufour
# Date   : 20/05/2021


# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy as np
import os, time
import tf

import pandas as pd 


import matplotlib.pyplot as plt
import tf2_ros 

from math_utils import euler2quaternion
from math_utils import quaternion2euler
from math_utils import euclidian_norm
from math_utils import quaternion2psi
from math_utils import quaternioninv
from math_utils import quaternionproduct
from math_utils import rotatewithquaternion

# Ros Message
from std_msgs.msg import String, Float32, Float64MultiArray
from std_msgs.msg import Bool as Msg_Bool

from sensor_msgs.msg import Imu as Msg_Imu
from sensor_msgs.msg import LaserScan as Msg_laser
from sensor_msgs.msg import JointState as Msg_joint_state
from sensor_msgs.msg import Image as Msg_im

from geometry_msgs.msg import Twist as Msg_Twist
from geometry_msgs.msg import Point as Msg_Point

from gazebo_msgs.msg import LinkState as Msg_LinkState
from gazebo_msgs.msg import LinkStates as Msg_LinkStates

from turtlesim.msg import Pose as Msg_Pose

from tf2_msgs.msg import TFMessage as Msg_TFMessage
# Ros Message Sur mesure
from chroma_gazebo_plugins_msgs.msg import Uwb as Msg_Uwb
from chroma_gazebo_plugins_msgs.msg import Float as Msg_Float
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_rotspeed
from chroma_gazebo_plugins_msgs.msg import Points as Msg_Points

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
import pickle



from nav_msgs.msg import Odometry as Msg_Odometry

import random as ran

X_TARGET = ran.random()
Y_TARGET = ran.random()
R = 0.1   #m
L2 = 0.26 #m
DEFAULT_FREQ = 10.0
# =======================================================
"""			CLASSES			"""
# =======================================================
class obstacle(object):
    def __init__(self):
        self.pose = None
        self.R_or_L = None
# =======================================================
class crawler(object):
    def __init__(self):
        self.__init__parameters()
        """| init of the object parameter |"""
        # Pose initiale
        self.msg_model_ = Msg_LinkState()
        self.p0_enu = None
        self.q_enu2flu0 = None
        
        # Vecteur d'état
        self.init_ok = False
        self.finish = False
        self.state_penu = None
        self.state_pxy = None
        self.or_init = None
        self.state_psi = None
        self.state_omg = None
        self.roll = None
        self.pitch = None

        self.ob = []       
        self.cross_ob = []
        self.tf_pose = np.array([0.0,
                                 -0.1,
                                 1.0])
        self.tf_or = euler2quaternion(0.0, -1.5707, -1.5707)    
        
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        
        
        self.robot_pose = self.tf_pose
        self.robot_or = self.tf_or
        self.obstacle = [1, 1]

        """| init of the subcriber |"""
        self.__init__subscribers()
        """| init of the publisher |"""
        self.__init__publishers()
        
        
    # ====================================
    """ 	   Parammeter		"""
    # ====================================	
    def __init__parameters(self):
        self.mimic_prefix = rospy.get_param('~mimic')
        self.topic_prefix = rospy.get_param('~topic_prefix')
        self.init_pos = np.array([-rospy.get_param('~initial_pos_y'), rospy.get_param('~initial_pos_x'), rospy.get_param('~initial_pos_z')])

        self.init_or = np.array([rospy.get_param('~initial_roll'), rospy.get_param('~initial_pitch'), rospy.get_param('~initial_yaw')])
        self.freq = rospy.get_param('~freq', DEFAULT_FREQ)     

    # ====================================
    """ 	   Subcriber		"""
    # ====================================		
    def __init__subscribers(self):
        rospy.Subscriber("/gazebo/link_states", Msg_LinkStates, self.callback_pose)
        # rospy.Subscriber("/tf", Msg_TFMessage, self.callback_tf)
        #rospy.Subscriber("/turtle1/pose", Msg_Pose, self.callback_tf)
        #rospy.Subscriber("/crawler/obstacle", Msg_Points, self.callback_ob)

    # ====================================
    """ 	   Publisher		"""
    # ====================================
    def __init__publishers(self):
        self.target = rospy.Publisher("cables/targets", Msg_Points, queue_size=10)
        self.pub_finish = rospy.Publisher("finish", Msg_Bool, queue_size=10)
        self.new_pose = rospy.Publisher("new_pose", Msg_Odometry, queue_size=10)
        
        self.model_pose_pub_ = rospy.Publisher("/gazebo/set_link_state", Msg_LinkState, queue_size=10)
        
    # ====================================
    """ 	   CallBack		"""
    # ====================================
    """def callback_tf(self, data):
        a = 0
        self.tf_pose = np.array([data.x,
                                 data.y,
                                  0])
                                 
        self.tf_or = euler2quaternion(-data.theta, 0.0, 0.0)
        
        for d in data.transforms:
            #if(d.child_frame_id == "crawler_0/base_link"):
            if(d.child_frame_id == "TS0001"):
                print(d.transform)
                self.tf_pose = np.array([d.transform.translation.x,
                                         d.transform.translation.y,
                                         d.transform.translation.z])
                                 
                self.tf_or = np.array([d.transform.rotation.x,
                                       d.transform.rotation.y,
                                       d.transform.rotation.z,
                                       d.transform.rotation.w])
                #euler2quaternion(1.5707, 0.0, 1.5707)     
        
        
        
        self.tf_pose = np.array([data.pose.pose.position.x,
                                 data.pose.pose.position.y,
                                 data.pose.pose.position.z])
            # Orientation initiale repere FLU(t) / ENU
        self.tf_or = quaternioninv([data.pose.pose.orientation.x,
                                    data.pose.pose.orientation.y,
                                    data.pose.pose.orientation.z,
                                    data.pose.pose.orientation.w])
    """
 
    # ====================================
    
    def def_obstacle(self, ob):
        for i in ob:
            print(i)
            for o in i:
                obb = obstacle()
                obb.pose = o
                obb.R_or_L = None
                self.ob.append(obb)

        print(self.ob)

    # ====================================
    def callback_pose(self, data):

        i = 0
        for name in data.name:
            if name == "crawler_10::crawler_10_body_link":
                self.state_penu = np.array([-data.pose[i].position.y,
                                             data.pose[i].position.x,
                                             data.pose[i].position.z])
                                             
                
                if not self.init_ok: 
                    self.or_init =  euler2quaternion(0.0, 0.0, 0.0)
                                   
                self.q_enu2flu0 = [data.pose[i].orientation.x,
                                   data.pose[i].orientation.y,
                                   data.pose[i].orientation.z,
                                   data.pose[i].orientation.w]
                self.state_pxy = rotatewithquaternion(self.state_penu,  self.or_init)[0:2]
                self.init_ok = True
            i+=1
      
                
 
 
    # ====================================
    """ 	   Target		"""
    # ====================================  
    def update_pose(self):
        try:
            
            trans_1 = self.tfBuffer.lookup_transform('totalstation', 'TS0001', rospy.Time())
            trans_2 = self.tfBuffer.lookup_transform('totalstation', self.mimic_prefix, rospy.Time())
            self.tf_pose = [trans_2.transform.translation.x-trans_1.transform.translation.x, 
                            trans_2.transform.translation.y-trans_1.transform.translation.y, 
                            trans_2.transform.translation.z-trans_1.transform.translation.z]
            
            
            a_1, b_1, c_1 = quaternion2euler(trans_1.transform.rotation.x, trans_1.transform.rotation.y, trans_1.transform.rotation.z, trans_1.transform.rotation.w)
            a_2, b_2, c_2 = quaternion2euler(trans_2.transform.rotation.x, trans_2.transform.rotation.y, trans_2.transform.rotation.z, trans_2.transform.rotation.w)
            
            q = euler2quaternion(-(a_2-a_1), b_2-b_1, c_2-c_1)
            
            
            self.tf_or = [q[0], q[1], q[2], q[3]]
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            a = 1
        
        if self.init_ok:
            self.msg_model_.link_name = "crawler_10_body_link";
            self.msg_model_.pose.position.x = self.tf_pose[0]
            self.msg_model_.pose.position.y = self.tf_pose[1]
            self.msg_model_.pose.position.z = self.tf_pose[2]
            
            
 
            self.msg_model_.pose.orientation.x = self.tf_or[0]
            self.msg_model_.pose.orientation.y = self.tf_or[1]
            self.msg_model_.pose.orientation.z = self.tf_or[2]
            self.msg_model_.pose.orientation.w = self.tf_or[3]
            if abs(self.tf_pose[1]-(-self.state_penu[0]))>0.01 or abs(self.tf_pose[2]-self.state_penu[2])>0.01 \
            or abs(self.tf_or[0]-(self.q_enu2flu0[0]))>0.1 or abs(self.tf_or[1]-(self.q_enu2flu0[1]))>0.1 or  abs(self.tf_or[2]-(self.q_enu2flu0[2]))>0.1 or abs(self.tf_or[3]-(self.q_enu2flu0[3]))>0.1 :
            
                self.model_pose_pub_.publish(self.msg_model_)


    # ====================================
    """     define Cable target        """
    # ====================================      
    def R_or_L(self,AB, AC):

        AB_AC = np.cross(AB, AC)
        if(AB_AC>0):
            return "Rigth"
        else:
            return "Left"
        
    # ====================================        
    def cross(self, A, B, C, i):
        AB = [B[0]-A[0], B[1]-A[1]]
        AC = [C[0]-A[0], C[1]-A[1]]
        #print("AB C ", [A,B,C])
        if(self.ob[i].R_or_L == None):
            self.ob[i].R_or_L = self.R_or_L(AB, AC)
            
        _AB_ = np.sqrt(pow(AB[0],2)+pow(AB[1],2))
        _AC_ = np.sqrt(pow(AC[0],2)+pow(AC[1],2))
        
        sens_ab =  AB[0]/abs(AB[0])
        sens_ac =  AC[0]/abs(AC[0])
        
        
        if(_AB_>_AC_ and sens_ab == sens_ac):
            if(self.ob[i].R_or_L != self.R_or_L(AB, AC)):
                self.ob[i].R_or_L = self.R_or_L(AB, AC)
                self.wait = True
                return True
        self.ob[i].R_or_L = self.R_or_L(AB, AC)
        return False 
        
        
        
    
    
    # ====================================    
    def cross_obstacle(self):
        #print("OBSTACLE ")
        for o in range(len(self.ob)):
            self.obstacle = rotatewithquaternion( self.ob[o].pose,  self.or_init)[0:2]
            #print("Obstacle == ", [self.ob[o].pose, self.obstacle])
            ori = [self.init_pos[0], self.init_pos[1],self.init_pos[2]-0.2]
            or_ = rotatewithquaternion( ori,  self.or_init)[0:2]
            #print("Origine == ", [ori, or_])
            if(len(self.cross_ob)>0):
                or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose,  self.or_init)[0:2]
            
            if(self.cross(or_, self.state_pxy, self.obstacle, o)):
                print("CROSS")
                print("or== ", or_)
                print("rob== ", self.state_pxy)
                print("ob== ", self.obstacle)
                self.cross_ob.append(self.ob.pop(o))
                
                break
    
    
    # ====================================        
    def uncross(self, A, B, C, i):
        AB = [B[0]-A[0], B[1]-A[1]]
        AC = [C[0]-A[0], C[1]-A[1]]

        if(self.cross_ob[i].R_or_L == None):
            self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
            
        _AB_ = np.sqrt(pow(AB[0],2)+pow(AB[1],2))
        _AC_ = np.sqrt(pow(AC[0],2)+pow(AC[1],2))
        
        if(_AB_>_AC_):
            if(self.cross_ob[i].R_or_L != self.R_or_L(AB, AC)):
                self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
                self.wait = True
                return True
        self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
        return False 
    # ====================================         

    def uncross_obstacle(self):
        
        if(len(self.cross_ob)>0): 

            ori = [self.init_pos[0], self.init_pos[1],self.init_pos[2]-0.2]
            or_ = rotatewithquaternion( ori,  self.or_init)[0:2]
            self.obstacle = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose,  self.or_init)[0:2]
            if(len(self.cross_ob)>1):
                or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-2].pose,  self.or_init)[0:2]
            if(self.uncross(or_, self.state_pxy, self.obstacle, len(self.cross_ob)-1)):
                print("UNCROSS")
                print("or== ", or_)
                print("rob== ", self.state_pxy)
                print("ob== ", self.obstacle)
                self.ob.append(self.cross_ob.pop(len(self.cross_ob)-1))
    
    
    # ====================================      
    def Cable_target(self):
        l = []

        self.cross_obstacle()
        self.uncross_obstacle()
        
        """l.append([self.state_penu[0],
                  self.state_penu[1],
                  0.15*(self.state_penu[2])]  )"""
        
        for i in range(len(self.cross_ob)):
            l.append(self.cross_ob[i].pose)
            
        
        l.append([self.state_penu[0]+0.1,
                  self.state_penu[1],
                  self.state_penu[2]]  )
        #print("cable target == ",l)
        
        msg_ = Msg_Points()
        for i in range(len(l)):
           
            msg = Msg_Point()
            
            msg.x = l[i][0]
            msg.y = l[i][1]
            msg.z = l[i][2]
            msg_.points.append(msg)
        self.target.publish(msg_)

    # ====================================
    """ 	   Command		"""
    # ====================================
    def command(self):
        #----- Erreur en position ----#
        self.update_pose()
        #print("initial pose == ",self.init_pos)
            

    def step(self):
        """
        """
        # Seulement si OK state
        
        if self.init_ok:
            
            # Calcul des commmandes
            self.command()
            # Choix de la cible
            self.Cable_target()
            
            
            
       
              
              
    # ====================================
    """ 	      Run		"""
    # ====================================
    def run(self):
        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()


# =======================================================
"""			   main			"""
# =======================================================
if __name__ == "__main__":
    rospy.init_node('listener', anonymous=True)
    pilote = crawler()

    obb = pd.read_json('../drone-simulator_ws/src/packages/gazebo_interfaces/src/scenario/obstacle.json')
    print("OBBBBB == ", obb)
    print("OBBBBB == ", len(obb['ob']))
    ob=[]
    if len(obb['ob']) >= 0:
        for i in range(max(max(obb['ob']), 1)):
            obbb = obb[obb['ob']==i]
            ob_ = []
            for j in range(len(obbb.index)):
                ob_.append([obbb[obbb['id']==j]['x'].iloc[0], obbb[obbb['id']==j]['y'].iloc[0], obbb[obbb['id']==j]['z'].iloc[0]])
            ob.append(ob_)    
    print(ob)
    pilote.def_obstacle(ob)  
    pilote.run()
    
