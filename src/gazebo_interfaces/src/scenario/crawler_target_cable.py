#!/usr/bin/env python3
# coding: utf-8

# Auteur : vdufour
# Date   : 20/05/2021


# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy as np
import os, time
import tf

#import pandas as pd 


#import matplotlib.pyplot as plt


from math_utils import euler2quaternion
from math_utils import quaternion2euler
from math_utils import euclidian_norm
from math_utils import quaternion2psi
from math_utils import quaternioninv
from math_utils import quaternionproduct
from math_utils import rotatewithquaternion

# Ros Message
from chroma_gazebo_plugins_msgs.msg import Points as Msg_Points
from geometry_msgs.msg import Point as Msg_Point
from nav_msgs.msg import Odometry as Msg_Odometry

#from sklearn.preprocessing import PolynomialFeatures
#from sklearn.linear_model import LinearRegression
#import pickle





import random as ran

X_TARGET = ran.random()
Y_TARGET = ran.random()
R = 0.1   #m
L2 = 0.26 #m
DEFAULT_FREQ = 10.0
# =======================================================
"""			CLASSES			"""
# =======================================================
class obstacle(object):
    def __init__(self):
        self.pose = None
        self.R_or_L = None
# =======================================================
class crawler(object):
    def __init__(self):
        self.__init__parameters()
        """| init of the object parameter |"""
        # Pose initiale
        self.p0_enu = None
        self.q_enu2flu0 = None
        # Vecteur d'état
        self.init_ok = False
        self.state_penu = None
        self.state_pxy = None

        self.state_psi = None
        self.state_omg = None
        self.roll = None
        self.pitch = None

        self.ob = []        
        self.cross_ob = []
        self.cross_hull = []
        self.cross_hull_id = []
        
        self.obstacle = [1, 1]

        """| init of the subcriber |"""
        self.__init__subscribers()
        """| init of the publisher |"""
        self.__init__publishers()
        
        
    # ====================================
    """ 	   Parammeter		"""
    # ====================================	
    def __init__parameters(self):
        self.topic_prefix = rospy.get_param('~topic_prefix')

        self.init_pos = np.array([-rospy.get_param('~initial_pos_y'), rospy.get_param('~initial_pos_x'), rospy.get_param('~initial_pos_z')])
                                  
        self.freq = rospy.get_param('~freq', DEFAULT_FREQ)

        self.real = int(rospy.get_param('~real', DEFAULT_FREQ))
        
        
    # ====================================
    """ 	   Publisher		"""
    # ====================================
    def __init__publishers(self):
        self.target = rospy.Publisher("cables/targets", Msg_Points, queue_size=10)

    # ====================================
    """ 	   Subcriber		"""
    # ====================================		
    def __init__subscribers(self):
        rospy.Subscriber("topic_odometry", Msg_Odometry, self.callback_odom)

        
    # ====================================
    
    def def_obstacle(self, ob):
        for i in ob:
            print(i)
            for o in i:
                obb = obstacle()
                obb.pose = o
                obb.R_or_L = None
                self.ob.append(obb)

        print(self.ob)

    # ====================================
    def callback_odom(self, data):
        if not(self.init_ok):
            # Position initiale dans repère ENU
            self.p0_enu = np.array([
                data.pose.pose.position.x,
                data.pose.pose.position.y,
                data.pose.pose.position.z])
            # Orientation initiale repere FLU(t) / ENU
            self.q_enu2flu0 = quaternioninv([
                data.pose.pose.orientation.x,
                data.pose.pose.orientation.y,
                data.pose.pose.orientation.z,
                data.pose.pose.orientation.w])
            #self.q_enu2flu0 = euler2quaternion(1.5707, 0.0, 1.5707)
            #self.q_enu2flu0 = euler2quaternion(0.0, 1.5707,1.5707)
            
            self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        
            #self.state_pxy = rotatewithquaternion(self.state_penu - self.p0_enu, self.q_enu2flu0)[0:2]
            #self.ori = euler2quaternion(1.5707, 0.0, 1.5707)
            #self.state_pxyz = rotatewithquaternion(self.state_penu, self.q_enu2flu0)
        
            self.state_pxy = rotatewithquaternion(self.state_penu, self.q_enu2flu0)[0:2]
            
            #self.state_pxy = rotatewithquaternion(self.p0_enu, self.q_enu2flu0)[0:2]
            

        
        # Position dans repere ENU
        self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        
        #self.state_pxy = rotatewithquaternion(self.state_penu - self.p0_enu, self.q_enu2flu0)[0:2]
        #self.ori = euler2quaternion(1.5707, 0.0, 1.5707)
        #self.state_pxyz = rotatewithquaternion(self.state_penu, self.q_enu2flu0)
        
        self.state_pxy = rotatewithquaternion(self.state_penu, self.q_enu2flu0)[0:2]

        
        
        # Orientation repere FLU(t) / ENU
        self.state_q_flu2enu = [
            data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w]
        self.state_psi = quaternion2psi(quaternionproduct(self.q_enu2flu0, self.state_q_flu2enu))
        yaw, self.pitch, self.roll = quaternion2euler(self.q_enu2flu0)

        
        # OK state
        self.init_ok = True
 
 

    # ====================================
    """     define Cable target        """
    # ====================================      
    def R_or_L(self,AB, AC):

        AB_AC = np.cross(AB, AC)
        if(AB_AC>0):
            return "Rigth"
        else:
            return "Left"
        
    # ====================================        
    def cross(self, A, B, C, i):
        AB = [B[0]-A[0], B[1]-A[1]]
        AC = [C[0]-A[0], C[1]-A[1]]
        #print("sim AB C ", [A,B,C])
        if(self.ob[i].R_or_L == None):
            self.ob[i].R_or_L = self.R_or_L(AB, AC)
            
        _AB_ = np.sqrt(pow(AB[0],2)+pow(AB[1],2))
        _AC_ = np.sqrt(pow(AC[0],2)+pow(AC[1],2))
        
        sens_ab =  AB[0]/abs(AB[0])
        sens_ac =  AC[0]/abs(AC[0])
        
        
        if(_AB_>_AC_ and sens_ab == sens_ac):
            if(self.ob[i].R_or_L != self.R_or_L(AB, AC)):
                #print("R or L",(self.ob[i].R_or_L, self.R_or_L(AB, AC)))
                self.ob[i].R_or_L = self.R_or_L(AB, AC)
                self.wait = True
                return True
        self.ob[i].R_or_L = self.R_or_L(AB, AC)
        return False 
        
        
        
    
    
    # ====================================    
    def cross_obstacle(self):
        #print("OBSTACLE ")
        for o in range(len(self.ob)):
            #print("o ==",self.ob[o].pose)
            #self.obstacle = rotatewithquaternion( self.ob[o].pose - self.p0_enu,  self.q_enu2flu0)[0:2]
            self.obstacle = rotatewithquaternion( self.ob[o].pose,  self.q_enu2flu0)[0:2]
            #print("init ", [self.init_pos, self.p0_enu])
            ori = [self.init_pos[0], self.init_pos[1],self.init_pos[2]-0.2]
            or_ = rotatewithquaternion( ori,  self.q_enu2flu0)[0:2]
            #or_ = rotatewithquaternion( [10.0,0.5,0.05] - self.p0_enu,  self.q_enu2flu0)[0:2]
            
            if(len(self.cross_ob)>0):
                #or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose - self.p0_enu,  self.q_enu2flu0)[0:2]
                or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose,  self.q_enu2flu0)[0:2]
            
            if(self.cross(or_, self.state_pxy, self.obstacle, o)):
                print("CROSS")
                print("or== ", or_)
                print("rob== ", self.state_pxy)
                print("ob== ", self.obstacle)
                self.cross_ob.append(self.ob.pop(o))
                
                break
    
    
    # ====================================        
    def uncross(self, A, B, C, i):
        AB = [B[0]-A[0], B[1]-A[1]]
        AC = [C[0]-A[0], C[1]-A[1]]

        if(self.cross_ob[i].R_or_L == None):
            self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
            
        _AB_ = np.sqrt(pow(AB[0],2)+pow(AB[1],2))
        _AC_ = np.sqrt(pow(AC[0],2)+pow(AC[1],2))
        
        if(_AB_>_AC_):
            if(self.cross_ob[i].R_or_L != self.R_or_L(AB, AC)):
                #print("R or L",(self.cross_ob[i].R_or_L, self.R_or_L(AB, AC)))
                self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
                self.wait = True
                return True
        self.cross_ob[i].R_or_L = self.R_or_L(AB, AC)
        return False 
    # ====================================         

    def uncross_obstacle(self):
        
        if(len(self.cross_ob)>0): 

            ori = [self.init_pos[0], self.init_pos[1],self.init_pos[2]-0.2]
            or_ = rotatewithquaternion( ori,  self.q_enu2flu0)[0:2]
            #or_ = rotatewithquaternion( [10.0,0.5,0.05] - self.p0_enu,  self.q_enu2flu0)[0:2]
            #self.obstacle = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose - self.p0_enu,  self.q_enu2flu0)[0:2]
            self.obstacle = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-1].pose,  self.q_enu2flu0)[0:2]
            if(len(self.cross_ob)>1):
                #print("origine_new", self.cross_ob[len(self.cross_ob)-2].pose)
                #or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-2].pose - self.p0_enu,  self.q_enu2flu0)[0:2]
                or_ = rotatewithquaternion( self.cross_ob[len(self.cross_ob)-2].pose,  self.q_enu2flu0)[0:2]
            if(self.uncross(or_, self.state_pxy, self.obstacle, len(self.cross_ob)-1)):
                print("UNCROSS")
                print("or== ", or_)
                print("rob== ", self.state_pxy)
                print("ob== ", self.obstacle)
                self.ob.append(self.cross_ob.pop(len(self.cross_ob)-1))
    
    
    def droite(self, p_1, p_2):
        #print("Ppppp == ", [p_1, p_2])
        a = (p_2[1]-p_1[1])/(p_2[0]-p_1[0])
        b = p_1[1]-a*p_1[0]
        
        return a, b
            
    def Hull(self):
        origine = []
        origine.append(self.init_pos)
        if self.cross_hull is None:
            if self.cross_ob is not None:
                for ob in  self.cross_ob:
                    origine.append(ob.pose)
        else:
            if self.cross_ob is not None:
                print("hull == ", self.cross_hull)
                print("len hull == ", len(self.cross_hull[1]))
                print("hull [] == ", self.cross_hull[1])
                
                if self.cross_hull_id[len(self.cross_hull_id)] > len(self.cross_ob):
                    for hull in  self.cross_hull:
                        origine.append(hull[0])
            else:
                for hull in  self.cross_hull:
                    origine.append(hull)
        
        
        p_1 = origine[len(origine)-1]
        p_2 = self.state_penu

        #etape 1 cable line equation
        cable_y_x = self.droite(p_1[:2], p_2[:2])
        cable_x_z = self.droite(p_1[1:], p_2[1:])
        
        #etape 2 
        #	A. Find all hull curve under the crawler pose 
        polynomial_features= PolynomialFeatures(degree=4)
        
        for z in range(int(p_2[2])*10):
            filename = "../drone-simulator_ws/src/packages/gazebo_interfaces/src/scenario/model/boat/"+str(z)+".pickle"
            model = pickle.load(open(filename, "rb"))
            #	B. what is the y pose for each z of the cable
            #print("z == ", [z, z/10])
            x_cable = ( (z/10) - cable_x_z[1]) / cable_x_z[0]
            y_cable = (x_cable - cable_y_x[1]) / cable_y_x[0]
            #print("Y cable == ", y_cable)
            #print("X cable == ", x_cable)
            y_poly = polynomial_features.fit_transform(np.array([y_cable])[:, np.newaxis])
            x_hull = model.predict(y_poly)[0][0]
            #print("X hull == ", x_hull)
            #		a. if the cable is under the hull 
            if  x_hull-0.50 > x_cable:
                self.cross_hull.append([y_cable , x_hull-0.4, (z/10)])
                self.cross_hull_id.append([len(self.cross_ob)])
                
                print("!!!!		UNDER		!!!!")
                print(" !!!!		"+self.topic_prefix+"		UNDER		!!!!", [x_cable, x_hull])
                print("Y cable == ", y_cable)
                print("X cable == ", x_cable)
                print("z == ", [z, z/10])
                break
            
            
            
            
            
        filename = "../drone-simulator_ws/src/packages/gazebo_interfaces/src/scenario/model/boat/10.pickle"
        model_ = pickle.load(open(filename, "rb"))
        tan_1 = self.tangente(p_1[0], model_)
        #print("a == ",[tan_1[0], cable_y_x[0], cable_x_z[0]])
    
    

    # ====================================
    """ 	  Cable target		"""
    # ====================================
    def Cable_target(self):
        #self.Hull()
        l = []

        self.cross_obstacle()
        self.uncross_obstacle()
        
        for i in range(len(self.cross_ob)):
            l.append(self.cross_ob[i].pose)
            
        for i in range(len(self.cross_hull)):
            l.append(self.cross_hull[i])
            
            
        robot_pose = [self.state_penu[0]-0.1,
                      self.state_penu[1],
                      self.state_penu[2]]
            
        l.append(robot_pose)
        
        msg_ = Msg_Points()
        for i in range(len(l)):
            msg = Msg_Point()
            
            msg.x = l[i][0]
            msg.y = l[i][1]
            msg.z = l[i][2]

            msg_.points.append(msg)
            msg_.header.stamp = rospy.get_rostime()
            
        #print("Message == ", msg_)
        self.target.publish(msg_)
            
    # ====================================
    """ 	   Step		"""
    # ====================================
    def step(self):
        # Seulement si OK state
        if self.init_ok:
            # Calcul des commmandes
            self.Cable_target()

              
    # ====================================
    """ 	      Run		"""
    # ====================================
    def run(self):
        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()


# =======================================================
"""			   main			"""
# =======================================================
if __name__ == "__main__":
    rospy.init_node('listener', anonymous=True)
    pilote = crawler()

    #obb = pd.read_json('../drone-simulator_ws/src/packages/gazebo_interfaces/src/scenario/obstacle.json')
    #print("OBBBBB == ", obb)
    #ob=[]
    """print("len(obb['ob'] == ", len(obb['ob']))
    if len(obb['ob']) >0:
        print("obb['ob']  ",obb['ob'])
        for i in range(max(obb['ob'])):
            print("obb['ob']  ",obb[obb['ob']==i)
            obbb = obb[obb['ob']==i]
            print("obbb == ",obbb)
            ob_ = []
            for j in range(len(obbb.index)):
                print("len(obbb.index == ",len(obbb.index))
                ob_.append([obbb[obbb['id']==j]['x'].iloc[0], obbb[obbb['id']==j]['y'].iloc[0], obbb[obbb['id']==j]['z'].iloc[0]])
            ob.append(ob_)    
    print("!!!!! Obstacle == ",ob)"""
    
    """ob =[[[0.0, 2.2, 4.24],
          [0.0, 2.85, 4.25],
          [0.0, 2.85,  0.76],
          [0.0, 2.21,  0.75]]]"""
    
    ob =[[[0.0, 2.2, 1.0],
          [0.0, 2.85, 1.0],
          [0.0, 2.85,  0.0],
          [0.0, 2.21,  0.0]]]
    
    """
    ob =[[[0.0, 1.8, 1.0],
          [0.0, 2.25, 1.0],
          [0.0, 2.25,  0.0],
          [0.0, 1.81,  0.0]], 
          
          [[0.0, 2.8, 4.24],
          [0.0, 3.25, 4.25],
          [0.0, 3.25,  0.76],
          [0.0, 2.81,  0.75]]
          
          
          ]
    
    
    """
    pilote.def_obstacle(ob)  
    pilote.run()
