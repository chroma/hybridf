#!/usr/bin/env python3
# coding: utf-8

# Auteur : vdufour
# Date   : 20/05/2021


# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy as np
import os, time
import tf

#import pandas as pd 


#import matplotlib.pyplot as plt


from math_utils import euler2quaternion
from math_utils import quaternion2euler
from math_utils import euclidian_norm
from math_utils import quaternion2psi
from math_utils import quaternioninv
from math_utils import quaternionproduct
from math_utils import rotatewithquaternion

# Ros Message
from std_msgs.msg import String, Float32, Float64MultiArray
from std_msgs.msg import Bool as Msg_Bool

from sensor_msgs.msg import Imu as Msg_Imu
from sensor_msgs.msg import LaserScan as Msg_laser
from sensor_msgs.msg import JointState as Msg_joint_state
from sensor_msgs.msg import Image as Msg_im

from geometry_msgs.msg import Twist as Msg_Twist
from geometry_msgs.msg import Point as Msg_Point

# Ros Message Sur mesure
from chroma_gazebo_plugins_msgs.msg import Uwb as Msg_Uwb
from chroma_gazebo_plugins_msgs.msg import Float as Msg_Float
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_rotspeed
from chroma_gazebo_plugins_msgs.msg import Points as Msg_Points

#from sklearn.preprocessing import PolynomialFeatures
#from sklearn.linear_model import LinearRegression
#import pickle



from nav_msgs.msg import Odometry as Msg_Odometry

import random as ran

X_TARGET = ran.random()
Y_TARGET = ran.random()
R = 0.1   #m
L2 = 0.26 #m
DEFAULT_FREQ = 10.0
# =======================================================
"""			CLASSES			"""
# =======================================================
class obstacle(object):
    def __init__(self):
        self.pose = None
        self.R_or_L = None
# =======================================================
class crawler(object):
    def __init__(self):
        self.__init__parameters()
        """| init of the object parameter |"""
        # Pose initiale
        self.p0_enu = None
        self.q_enu2flu0 = None
        #self.init_pos = np.zeros(3)
        self.init_or = np.zeros(3)
        # Vecteur d'état
        self.init_ok = False
        self.finish = False
        self.state_penu = None
        self.state_pxy = None
        
        self.update_prio_ok = None
        self.update_prio = False
        
        
        self.statik = False
        self.prev_state = []
        for i in range(10):
            self.prev_state.append(self.state_pxy)
        self.state_psi = None
        self.state_omg = None
        self.roll = None
        self.pitch = None

        # Target
        self.target_pxy = None
        self.target_list = []
        self.target_list_temp = []
        self.wait = False
        self.stop = None
        self.target_ok = False

        self.all_pose = {}
        self.all_target = {}

        """| init of the subcriber |"""
        self.__init__subscribers()
        """| init of the publisher |"""
        self.__init__publishers()
        
        
    # ====================================
    """ 	   Parammeter		"""
    # ====================================	
    def __init__parameters(self):
        self.topic_prefix = rospy.get_param('~topic_prefix')
        self.hull = rospy.get_param('~hull')
        self.init_pos = np.array([-rospy.get_param('~initial_pos_y'), rospy.get_param('~initial_pos_x'), rospy.get_param('~initial_pos_z')])
                                  
        self.init_or = np.array([rospy.get_param('~initial_roll'), rospy.get_param('~initial_pitch'), rospy.get_param('~initial_yaw')])
        self.freq = rospy.get_param('~freq', DEFAULT_FREQ)
        
        
    # ====================================
    """ 	   Publisher		"""
    # ====================================
    def __init__publishers(self):
        self.pub_cmd = rospy.Publisher("~cmd", Msg_Twist, queue_size=10)
        self.pub_finish = rospy.Publisher("finish", Msg_Bool, queue_size=10)
        self.stoped = rospy.Publisher("stoped", Msg_Bool, queue_size=10)
        self.new_pose = rospy.Publisher("new_pose", Msg_Odometry, queue_size=10)

        self.pose_send = rospy.Publisher("/all_pose", Msg_Odometry, queue_size=10)
        self.target_send = rospy.Publisher("/all_target", Msg_Odometry, queue_size=10)

        self.pub_update_prio = rospy.Publisher("update_prio", Msg_Bool, queue_size=10)

    # ====================================
    """ 	   Subcriber		"""
    # ====================================		
    def __init__subscribers(self):
        rospy.Subscriber("topic_odometry", Msg_Odometry, self.callback_odom)

        rospy.Subscriber("/all_pose", Msg_Odometry, self.callback_pose_all)
        rospy.Subscriber("/all_target", Msg_Odometry, self.callback_targ_all)

        rospy.Subscriber("topic_target", Msg_Point, self.callback_target)
        rospy.Subscriber("stop", Msg_Bool, self.callback_stop)
        #rospy.Subscriber("/crawler/obstacle", Msg_Points, self.callback_ob)

        
    # ====================================
    """ 	   CallBack		"""
    # ====================================

    def callback_targ_all(self, data):
        """
        """
        self.all_target[data.child_frame_id]=[data.pose.pose.position.x,
                                              data.pose.pose.position.y,
                                              data.pose.pose.position.z]
        
    # ====================================    
    def callback_pose_all(self, data):
        """
        """
        self.all_pose[data.child_frame_id]=[data.pose.pose.position.x,
                                            data.pose.pose.position.y,
                                            data.pose.pose.position.z]
        
    # ====================================    

    def callback_target(self, data):
        """
        """
        #print("New_target")
        self.target_list.append(data)
        self.target_ok =True
        self.update_prio_ok = True
        
    # ====================================    
    def callback_stop(self, data):
        """
        """
       
        self.stop = data.data
        

    # ====================================
    def callback_odom(self, data):
        if not(self.init_ok):
            # Position initiale dans repère ENU
            self.p0_enu = np.array([
                data.pose.pose.position.x,
                data.pose.pose.position.y,
                data.pose.pose.position.z])
            # Orientation initiale repere FLU(t) / ENU
            self.q_enu2flu0 = quaternioninv([
                data.pose.pose.orientation.x,
                data.pose.pose.orientation.y,
                data.pose.pose.orientation.z,
                data.pose.pose.orientation.w])
            #self.q_enu2flu0 = euler2quaternion(1.5707, 0.0, 1.5707)
            self.q_enu2flu0 = euler2quaternion(0.0, 1.5707, 1.5707)
            
            self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        
            #self.state_pxy = rotatewithquaternion(self.state_penu - self.p0_enu, self.q_enu2flu0)[0:2]
            #self.ori = euler2quaternion(1.5707, 0.0, 1.5707)
            #self.state_pxyz = rotatewithquaternion(self.state_penu, self.q_enu2flu0)
        
            self.state_pxy = rotatewithquaternion(self.state_penu, self.q_enu2flu0)[0:2]
            #self.state_pxy = rotatewithquaternion(self.p0_enu, self.q_enu2flu0)[0:2]
            

        
        # Position dans repere ENU
        self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        
        #self.state_pxy = rotatewithquaternion(self.state_penu - self.p0_enu, self.q_enu2flu0)[0:2]
        #self.ori = euler2quaternion(1.5707, 0.0, 1.5707)
        #self.state_pxyz = rotatewithquaternion(self.state_penu, self.q_enu2flu0)
        
        self.state_pxy = rotatewithquaternion(self.state_penu, self.q_enu2flu0)[0:2]

        
        
        # Orientation repere FLU(t) / ENU
        self.state_q_flu2enu = [
            data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w]
        self.state_psi = quaternion2psi(quaternionproduct(self.q_enu2flu0, self.state_q_flu2enu))
        yaw, self.pitch, self.roll = quaternion2euler(self.q_enu2flu0)

        
        # OK state
        msg = Msg_Odometry()

        msg.child_frame_id=self.topic_prefix
        msg.pose = data.pose
        self.pose_send.publish(msg)
        self.init_ok = True
 
 
    # ====================================
    """ 	   Target		"""
    # ====================================  
    def update_target(self):
        # Nouvelle cible disponible
        if len(self.target_list) > 0:
            self.finish = False
            # Si on avait deja une cible
            if (self.target_pxy is not None):
                if self.statik:
                    #self.target_list_temp.append(self.target_pxy)
                    self.target_pxy = [self.target_pxy[0]+np.cos(self.state_psi), self.target_pxy[1]+np.sin(self.state_psi)]
                    for i in range(len(self.target_list)-1):
                        self.target_list_temp.append(self.target_list.pop(i))
                        #print(len(self.target_list))
                        #print((self.target_list))
                    for i in range(len(self.target_list_temp)-1):
                        self.target_list.append(self.target_list_temp.pop(i))
                    
                # Distance avec la cible courante
                dist_pxy = euclidian_norm(self.state_pxy - self.target_pxy)
                # Test si on a atteint la cible courante
                if (dist_pxy < 0.2):
                    # Passage a la cible suivante
                    
                    target = self.target_list.pop(0)
                    
                    self.target_pxy = np.array([target.x, target.y])
                    
            # Si on avait aucune cible
            else:
                # Premiere cible
                target = self.target_list.pop(0)
                
                self.target_pxy =  np.array([target.x, target.y])

        else:
            for i in range(10):
                self.prev_state[i]=10*i        
    
    # ====================================
    """ 	   Dodge		"""
    # ====================================

    # ====================================
    """ 	   Command		"""
    # ====================================
    def command(self):
        #----- Erreur en position ----#
        msg = Msg_Twist()
        
        if (self.target_pxy is not None):						# erreur de position
            # Commande vitesse
            #self.test(self.state_pxy[0], self.state_pxy[1], self.state_pxy[2], self.roll, self.pitch)
            #self.test(self.target_pxy[0], self.target_pxy[1], self.target_pxy[2], self.roll, self.pitch)
            
            
            
            
            
            # Commande vitesse
            cmd_vit_xy = self.target_pxy - self.state_pxy					# Erreur de position
            cmd_vit = euclidian_norm(cmd_vit_xy)						# Distance entre le la cible et le robot
            cmd_vit = math.tanh(cmd_vit)							# Calcule de la vitesse comprise entre -1 et 1 grace a tanh
            
            cmd_omg = self.state_psi
            # Commande angulaire
            if cmd_vit > 0.1:	
                vit = 0.4
                cmd_psi = math.atan2(cmd_vit_xy[1], cmd_vit_xy[0])				# creer l'oriantation du crawler 
                cmd_omg = 2.0*math.atan(math.tan((cmd_psi - self.state_psi)/2.0))		# corriger l'oriantation du crawler
            else:
                if euclidian_norm(cmd_vit_xy) < 0.1:	

                    self.finish = True
                    cmd_omg = 0 
                    if (self.state_omg):								
                        cmd_omg = -self.state_omg							# erreur de position
                    cmd_vit = 0.0
                    vit = 0.0
                    self.target_pxy = self.state_pxy	
                    if self.update_prio_ok:
                        msg_prio = Msg_Bool()
                        msg_prio.data = True
                        self.pub_update_prio.publish(msg_prio)
                        self.update_prio_ok = False
                        rate = rospy.Rate(self.freq)
                        rate.sleep()
               
            if (abs(cmd_omg) > 0.1):
                cmd_vit = 0.0  
                vit=0.0 			
            
            
            
            #print([self.topic_prefix ,self.statik])
            msg.linear.x = vit
            msg.linear.y = 0.0
            msg.linear.z = 0.0
            msg.angular.x = 0.0
            msg.angular.y = 0.0
            msg.angular.z = 1*cmd_omg
            # Publication Message ROS avec les commandes
            self.pub_cmd.publish(msg)
            #print("                                                 ",[self.topic_prefix, self.target_pxy, self.state_pxy])
            

        
        else:										# erreur de position
            # Creation Message ROS pour rester sur place
            
            msg.linear.x = 0.0
            msg.linear.y = 0.0
            msg.linear.z = 0.0
            msg.angular.x = 0.0
            msg.angular.y = 0.0
            msg.angular.z = 0.0
            self.pub_cmd.publish(msg)
            
        
        
        #print([self.topic_prefix, msg, self.state_pxy, self.state_penu, self.target_list, len(self.ob), self.ob[0]])
            
            

    def step(self):
        """
        """
        # Seulement si OK state
        
        if self.init_ok:
            # Choix de la cible
            self.update_target()
            # Calcul des commmandes
            self.command()
            msg = Msg_Bool()
            msg.data = False
            if self.finish:
                msg.data = True
            self.pub_finish.publish(msg)


            msg.data = False
            if self.stop:
                self.target_list = []
                self.target_pxy = self.state_pxy
            
                msg.data = True
            self.stoped.publish(msg)
            
            
        else:										# erreur de position
            # Creation Message ROS pour rester sur place
            msg = Msg_Twist()
            msg.linear.x = 0.0
            msg.linear.y = 0.0
            msg.linear.z = 0.0
            msg.angular.x = 0.0
            msg.angular.y = 0.0
            msg.angular.z = 0.0
            self.pub_cmd.publish(msg)
              
              
    # ====================================
    """ 	      Run		"""
    # ====================================
    def run(self):
        rate = rospy.Rate(self.freq)
        
        while not rospy.is_shutdown():

            rate.sleep()
            self.step()
            


# =======================================================
"""			   main			"""
# =======================================================
if __name__ == "__main__":
    
    rospy.init_node('listener', anonymous=True)
    pilote = crawler()
    pilote.run()
