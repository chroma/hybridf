#!/usr/bin/env python3
# coding: utf-8

# Auteur : vdufour
# Date   : 20/05/2021


# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy as np
import os, time
import tf

import matplotlib.pyplot as plt


from math_utils import euler2quaternion
from math_utils import quaternion2euler
from math_utils import euclidian_norm
from math_utils import quaternion2psi
from math_utils import quaternioninv
from math_utils import quaternionproduct
from math_utils import rotatewithquaternion



# Ros Message

from sensor_msgs.msg import LaserScan
from laser_geometry import LaserProjection

from sensor_msgs.msg import PointCloud2 as Msg_Point_Cloud
from std_msgs.msg import Float32 as Msg_Float
from nav_msgs.msg import Odometry as Msg_Odometry


DEFAULT_FREQ = 10.0
# =======================================================
"""			            CLASSES		            	"""
# =======================================================
class crawler(object):
    def __init__(self):
        self.freq = rospy.get_param('~freq', DEFAULT_FREQ)
        # Pose initiale
        self.p0_enu = None
        self.q_enu2flu0 = None
        #self.init_pos = np.zeros(3)
        self.init_or = np.zeros(3)
        # Vecteur d'état
        self.init_ok = False
        self.state_penu = None
        self.state_pxy = None
        
        self.state_psi = None
        self.state_omg = None
        self.roll = None
        self.pitch = None
        self.msg_thickness = Msg_Float()
        
        
        self.laser_projector = LaserProjection()
        self.scan_topic = "/scan"


        """| init of the subcriber |"""
        self.__init__subscribers()
        """| init of the publisher |"""
        self.__init__publishers()
        
        
        
    # ====================================
    """ 	   Publisher		"""
    # ====================================
    def __init__publishers(self):
        self.pub_thick = rospy.Publisher("thickness", Msg_Float, queue_size=10)

    # ====================================
    """ 	   Subcriber		"""
    # ====================================		
    def __init__subscribers(self):
        rospy.Subscriber("topic_odometry", Msg_Odometry, self.callback_odom)
        rospy.Subscriber(self.scan_topic, LaserScan, self.on_scan)
    
    # ====================================
    """ 	   CallBack		"""
    # ====================================
    
    def callback_odom(self, data):
        if not(self.init_ok):
            # Position initiale dans repère ENU
            self.p0_enu = np.array([
                data.pose.pose.position.x,
                data.pose.pose.position.y,
                data.pose.pose.position.z])
            # Orientation initiale repere FLU(t) / ENU
            self.q_enu2flu0 = quaternioninv([
                data.pose.pose.orientation.x,
                data.pose.pose.orientation.y,
                data.pose.pose.orientation.z,
                data.pose.pose.orientation.w])
            self.q_enu2flu0 = euler2quaternion(1.5707, 0.0, 1.5707)
            
            self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
                

        
        # Position dans repere ENU
        self.state_penu = np.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])

        self.state_pxy = rotatewithquaternion(self.state_penu, self.q_enu2flu0)[0:2]

        
        
        # Orientation repere FLU(t) / ENU
        self.state_q_flu2enu = [
            data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w]
        self.state_psi = quaternion2psi(quaternionproduct(self.q_enu2flu0, self.state_q_flu2enu))
        yaw, self.pitch, self.roll = quaternion2euler(self.q_enu2flu0)

        
        # OK state
        self.init_ok = True

    # ====================================
    
 
    def on_scan(self, scan):
        rospy.loginfo("Got scan, projecting")
        cloud = self.laser_projector.projectLaser(scan)
        for p in pc2.read_points(cloud, field_names = ("x", "y", "z"), skip_nans=True):
            if abs(self.state_pxy[0] - p[0]) < 0.1 and abs(self.state_pxy[1] - p[1]) < 0.1:
                self.msg_thickness.data = p[2]
  

    # ====================================
    """ 	      Run		"""
    # ====================================
    def run(self):
        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            rate.sleep()

# =======================================================
"""			   main			"""
# =======================================================
if __name__ == "__main__":
    rospy.init_node('listener', anonymous=True)
    pilote = crawler()

    pilote.run()
