cmake_minimum_required(VERSION 2.8.3)
project(task_manager_crawler)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS 
  roscpp 
  tf2_ros  
  tf2_geometry_msgs 
  std_msgs 
  task_manager_lib 
  task_manager_sync 
  task_manager_msgs 
  geometry_msgs 
  sensor_msgs 
  # mesh_planner 
  # floor_plane_ransac
  nav_msgs
  # bugwright_mission_protocol
  )

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

#######################################
## Declare ROS messages and services ##
#######################################

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  Waypoint.msg
  PathSegment.msg
  Path.msg)

## Generate services in the 'srv' folder
add_service_files(
  FILES
  StartMission.srv
  StartMissionWP.srv
  GetPrio.srv
  UpdatePrio.srv
  PlanRequest.srv
)

generate_dynamic_reconfigure_options(
    cfg/TaskGoTo.cfg
    cfg/TaskGoToMesh.cfg
    cfg/TaskGoToMeshWithConstraint.cfg
    cfg/TaskReachAngle.cfg
    cfg/TaskConstant.cfg
    cfg/TaskHorizontalTransect.cfg
    cfg/TaskVerticalTransect.cfg
    cfg/TaskWaitForCollision.cfg
    cfg/TaskWaitForButton.cfg
    cfg/TaskPointTowards.cfg
    cfg/TaskWaitPrio.cfg
    # cfg/TaskFollowBeam.cfg
    # cfg/TaskDriveBetweenWalls.cfg
)


## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs   task_manager_msgs
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES task_manager_crawler
  CATKIN_DEPENDS roscpp std_msgs task_manager_lib task_manager_msgs
  sensor_msgs geometry_msgs #bugwright_mission_protocol
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
    include ${catkin_INCLUDE_DIRS}
)

## Declare a cpp library
# add_library(task_manager_crawler
#   src/${PROJECT_NAME}/task_manager_crawler.cpp
# )

## Declare a cpp executable
# add_executable(task_manager_crawler_node src/task_manager_crawler_node.cpp)

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
# add_dependencies(task_manager_crawler_node task_manager_crawler_generate_messages_cpp)

## Specify libraries to link a library or executable target against
# target_link_libraries(task_manager_crawler_node
#   ${catkin_LIBRARIES}
# )

ADD_EXECUTABLE( crawler_task_server_sync src/task_server.cpp src/CrawlerEnv.cpp) 
TARGET_LINK_LIBRARIES(crawler_task_server_sync ${catkin_LIBRARIES} dl )

ADD_EXECUTABLE( crawler_task_server_sim src/task_server_sim.cpp src/CrawlerSimEnv.cpp) 
TARGET_LINK_LIBRARIES(crawler_task_server_sim ${catkin_LIBRARIES} dl )

add_executable(path_publisher_mission src/path_mission_publisher.cpp)
target_link_libraries(path_publisher_mission ${catkin_LIBRARIES})
add_dependencies(path_publisher_mission beginner_tutorials_generate_messages_cpp)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_SHARE_DESTINATION}/tasks)

ADD_LIBRARY( crawler_TaskGoTo SHARED tasks/TaskGoTo.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskGoTo ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskGoTo ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskGoToMesh SHARED tasks/TaskGoToMesh.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskGoToMesh ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskGoToMesh ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskGoToMeshWithConstraint SHARED tasks/TaskGoToMeshWithConstraint.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskGoToMeshWithConstraint ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskGoToMeshWithConstraint ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskReachAngle SHARED tasks/TaskReachAngle.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskReachAngle ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskReachAngle ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskConstant SHARED tasks/TaskConstant.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskConstant ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskConstant ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskHorizontalTransect SHARED tasks/TaskHorizontalTransect.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskHorizontalTransect ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskHorizontalTransect ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskVerticalTransect SHARED tasks/TaskVerticalTransect.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskVerticalTransect ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskVerticalTransect ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskWaitForCollision SHARED tasks/TaskWaitForCollision.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskWaitForCollision ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskWaitForCollision ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskWaitForButton SHARED tasks/TaskWaitForButton.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskWaitForButton ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskWaitForButton ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskPointTowards SHARED tasks/TaskPointTowards.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskPointTowards ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskPointTowards ${${PROJECT_NAME}_EXPORTED_TARGETS})

ADD_LIBRARY( crawler_TaskWaitPrio SHARED tasks/TaskWaitPrio.cpp)
TARGET_LINK_LIBRARIES(crawler_TaskWaitPrio ${catkin_LIBRARIES} dl)
ADD_DEPENDENCIES(crawler_TaskWaitPrio ${${PROJECT_NAME}_EXPORTED_TARGETS})

# ADD_LIBRARY( crawler_TaskFollowBeam SHARED tasks/TaskFollowBeam.cpp)
# TARGET_LINK_LIBRARIES(crawler_TaskFollowBeam ${catkin_LIBRARIES} dl)
# ADD_DEPENDENCIES(crawler_TaskFollowBeam ${${PROJECT_NAME}_EXPORTED_TARGETS})

# ADD_LIBRARY( crawler_TaskDriveBetweenWalls SHARED tasks/TaskDriveBetweenWalls.cpp)
# TARGET_LINK_LIBRARIES(crawler_TaskDriveBetweenWalls ${catkin_LIBRARIES} dl)
# ADD_DEPENDENCIES(crawler_TaskDriveBetweenWalls ${${PROJECT_NAME}_EXPORTED_TARGETS})

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS task_manager_crawler task_manager_crawler_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_task_manager_crawler.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
