#!/usr/bin/python
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import rospy
from math import *
from task_manager_lib.TaskClient import *
import tf

#Global Variables and Flags
vert_transect_dist = 15
horz_transect_dist = 1
transect_threshold = 0.1
angle_threshold = 0.05
height_reached = False
linear_speed = 0.3


rospy.init_node('task_client1')
listener = tf.TransformListener()
server_node = rospy.get_param("~server","/crawler_tasks1")
default_period = rospy.get_param("~period",0.05)
tc = TaskClient(server_node,default_period)

(initial_t,rot) = listener.lookupTransform('/base_link', '/odom', rospy.Time(0))

# [verticle, facing left, facing right]
angle = {"face_up": 1.57, "face_left": 3.14, "face_right": 0}
k_alpha = {"face_up": 2.8, "face_left": 1.57 , "face_right": -1.57}

tc.ReachAngle(target=angle["face_up"], use_tf=True)

for i in range(3):
    w4obj = tc.WaitForCollision(foreground=False, depth=1.0)
    tc.addCondition(ConditionIsCompleted("Object Detector",tc,w4obj))
    try:
        height_reached = False
        tc.ReachAngle(target=angle["face_up"], use_tf=True)
        tc.VerticalTransect(relative=True,altitude=vert_transect_dist,linear=linear_speed, use_tf=True, threshold=transect_threshold)
        height_reached = True
        tc.ReachAngle(target=angle["face_left"],use_tf=True, threshold=angle_threshold)
        tc.HorizontalTransect(relative=True,positive=False,linear=linear_speed,distance=horz_transect_dist,use_tf=True)
        tc.clearConditions()

    except TaskConditionException as e:
        rospy.loginfo("Distance Hit")
        #if I did not get hight enough try to do a horz transect
        if not height_reached:
            tc.ReachAngle(target=angle["face_left"],use_tf=True, threshold=angle_threshold)
            w4obj = tc.WaitForCollision(foreground=False, depth=0.5)
            tc.addCondition(ConditionIsCompleted("Object Detector",tc,w4obj))
            try:
                tc.HorizontalTransect(relative=True,positive=False,linear=linear_speed,distance=horz_transect_dist,use_tf=True)
                tc.clearConditions()
            except TaskConditionException as e:
                pass

    #No matter what go down and do a new sweep
    (t,_) = listener.lookupTransform("/base_link","/odom",rospy.Time(0))
    dist_to_go_down = sqrt((t[0]-initial_t[0])**2 + (t[1]-(initial_t[1])**2))
    tc.ReachAngle(target=angle["face_up"], use_tf=True)
    tc.VerticalTransect(relative=True,altitude=-dist_to_go_down,linear=linear_speed, use_tf=True,threshold=transect_threshold)
    tc.ReachAngle(target=angle["face_left"],use_tf=True, threshold=angle_threshold)
    tc.HorizontalTransect(relative=True,positive=False,linear=linear_speed,distance=horz_transect_dist,use_tf=True)


