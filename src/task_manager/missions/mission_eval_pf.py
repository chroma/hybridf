#!/usr/bin/python
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import rospy
from math import *
from task_manager_lib.TaskClient import *
import tf

#Global Variables and Flags
vert_transect_dist = 8
horz_transect_dist = 1.5
transect_threshold = 0.1
angle_threshold = 0.1
height_reached = False
linear_speed = 0.3
k_alpha=3.5
max_vrot=2.5


rospy.init_node('task_client1')
listener = tf.TransformListener()
server_node = rospy.get_param("~server","/crawler_tasks1")
default_period = rospy.get_param("~period",0.05)
tc = TaskClient(server_node,default_period)
tc.Wait(duration=3.0)

(initial_t,rot) = listener.lookupTransform('world', 'base_footprint_gt', rospy.Time(0))
print(initial_t)

# [verticle, facing left, facing right]
angle = {"face_up": 1.57, "face_left": 3.14, "face_right": 0.0}

tc.ReachAngle(target=angle["face_up"], use_tf=False, k_alpha=k_alpha, max_vrot=max_vrot)

for i in range(3):
    tc.ReachAngle(target=angle["face_up"], use_tf=False, k_alpha=k_alpha, max_vrot=max_vrot)
    tc.VerticalTransect(relative=True,altitude=vert_transect_dist,linear=linear_speed, threshold=transect_threshold, k_alpha=k_alpha, max_vrot=max_vrot)
    tc.ReachAngle(target=angle["face_right"],use_tf=False, k_alpha=k_alpha, max_vrot=max_vrot, threshold=angle_threshold)
    tc.HorizontalTransect(relative=True,positive=True,linear=linear_speed,distance=horz_transect_dist,k_alpha=k_alpha,max_vrot=max_vrot,k_z=3)


    #No matter what go down and do a new sweep
    (t,_) = listener.lookupTransform('world', "base_footprint_gt",rospy.Time(0))
    print(t)
    dist_to_go_down = abs(t[2]-initial_t[2])
    tc.ReachAngle(target=angle["face_up"], k_alpha=k_alpha, max_vrot=max_vrot, use_tf=False)
    tc.VerticalTransect(relative=False,altitude=initial_t[2],linear=linear_speed, threshold=transect_threshold, k_alpha=k_alpha, max_vrot=max_vrot)
    tc.ReachAngle(target=angle["face_right"],use_tf=False, k_alpha=k_alpha, max_vrot=max_vrot, threshold=angle_threshold)
    tc.HorizontalTransect(relative=True,positive=True,linear=linear_speed,distance=horz_transect_dist,k_alpha=k_alpha,max_vrot=max_vrot,k_z=3)


