#!/usr/bin/python
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import rospy
from math import *
from task_manager_lib.TaskClient import *

rospy.init_node('task_client3')
server_node = rospy.get_param("~server","/crawler_tasks3")
default_period = rospy.get_param("~period",0.2)
tc = TaskClient(server_node,default_period)

tc.SetStatusSync(status=0);
tc.ReachAngle(target=0);
tc.VerticalTransect(altitude=-1.5,linear=0.3)
#tc.VerticalTransect(altitude=9,linear=0.3)
tc.ReachAngle(target=pi/2);
tc.SetStatusSync(status=1);
tc.WaitForStatusSync(partner="partner1",status=2);
for i in range(3):
    tc.HorizontalTransect(relative=True,positive=True,linear=0.5,distance=10.0)
    tc.ReachAngle(target=0);
    tc.VerticalTransect(relative=True,altitude=-0.5,linear=0.3)
    tc.ReachAngle(target=-pi/2);
    tc.HorizontalTransect(relative=True,positive=False,linear=0.5,distance=10.0)
    tc.ReachAngle(target=0);
    tc.VerticalTransect(relative=True,altitude=-0.5,linear=0.3)
    tc.ReachAngle(target=pi/2);
tc.SetStatusSync(status=2);
tc.WaitForStatusSync(partner="partner1",status=3);
tc.VerticalTransect(altitude=4.0,linear=0.5)
tc.SetStatusSync(status=0);


rospy.loginfo("Mission3 completed")


