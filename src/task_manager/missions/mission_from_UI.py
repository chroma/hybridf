#!/usr/bin/python3
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import sys
import rospy
from math import *
from threading import Lock

from task_manager_lib.TaskClient import *
import tf

#import custom messages
from bugwright_mission_protocol.msg import Mission

#import cutom services
from bugwright_mission_protocol.srv import PrepareMissions, PrepareMissionsResponse, ExecuteMissions, ExecuteMissionsResponse
from mesh_planner.srv import *

#import Ros Geometry Point message
from geometry_msgs.msg import PoseArray, Pose, PoseStamped, PointStamped, Point
from nav_msgs.msg import Path as NavPath
#import mesh planner Path message
from mesh_planner.msg import Path

#mission types
MISSION_TYPE_INSPECT_POINT = 0;
MISSION_TYPE_INSPECT_PATH = 1;
MISSION_TYPE_INSPECT_AREA = 2;

LINEAR_SPEED = 0.8
MAX_ROTATION_SPEED = 1.0
ROTATION_GAIN = 1.0

MISSION_ID_ON_ERROR = 5000

HORIZONTAL_PATH_LEN = 1.0 #meters

path_pub = None

def ConvertPoseArrayToPath(pose_array):
    path = NavPath()
    path.header.frame_id = pose_array.header.frame_id
    path.header.stamp = rospy.Time.now()
    for i in range(0, len(pose_array.poses)):
        pose = PoseStamped()
        pose.header.frame_id = pose_array.header.frame_id
        pose.header.stamp = rospy.Time.now()
        pose.pose = pose_array.poses[i]
        path.poses.append(pose)
    return path

def ConvertMeshMsgSegmentsToPath(mesh_msg_segments):
    poses = PoseArray()
    segments = mesh_msg_segments.segments

    for i in range(0, len(segments)):
        pose = Pose()
        pose.position.x = segments[i].start.x
        pose.position.y = segments[i].start.y
        pose.position.z = segments[i].start.z

        #TODO: calculate orientation based on rotation matrix
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1
    
        poses.poses.append(pose)
    
    #add the final point
    pose = Pose()
    pose.position.x = segments[len(segments)-1].end.x
    pose.position.y = segments[len(segments)-1].end.y
    pose.position.z = segments[len(segments)-1].end.z
    pose.orientation.x = 0
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1
    poses.poses.append(pose)
    return poses

#create mission class
class Mission:
    #initialize mission
    def __init__(self, mesh_planner_service, mission_reference, mission_type, point_array, world_frame):
        self.mission_reference = mission_reference;
        self.mission_type = mission_type;
        self.point_array = point_array;
        self.world_frame = world_frame;
        self.has_been_executed = False;
        
        #make sure point array is not empty
        if(len(self.point_array) == 0):
            rospy.logerr("Mission point array is empty")
            return

        #mission start point (its the end point when mission is just go to point)
        self.start_point = self.point_array[0]
        self.end_point = self.point_array[len(self.point_array)-1]

        self.mesh_planner_service = mesh_planner_service

        #print out all attribute values after initialization

    def GetMissionPath(self):
        global path_pub
        #if mission is just go to point
        if(self.mission_type == MISSION_TYPE_INSPECT_POINT):
            return None
        elif(self.mission_type == MISSION_TYPE_INSPECT_PATH):
            #iterate over points
            path = PoseArray()
            #create header
            path.header.frame_id = self.world_frame
            path.header.stamp = rospy.Time.now()
            for i in range(0, len(self.point_array)-1):
                #get path between two points
                path_segments = self.GetPathWithService(self.point_array[i], self.point_array[i+1])
                #if path is None, then mission is not possible
                if(path_segments == None):
                    return None
                path_segments = ConvertMeshMsgSegmentsToPath(path_segments)                
                path.poses += path_segments.poses
            
            path2 = ConvertPoseArrayToPath(path)
            path_pub.publish(path2)
            return path
        
        
        elif(self.mission_type == MISSION_TYPE_INSPECT_AREA):
            #currently this is a square area with 4 points
            if(len(self.point_array) != 4):
                rospy.logerr("Mission area is not a square")
                return None
            
            #calculate height and width of area
            height = abs(self.point_array[0].z - self.point_array[1].z)
            width = abs(self.point_array[1].x - self.point_array[2].x)

            #using width calculate number of verticle paths, one per meter
            num_verticle_paths = int(width/HORIZONTAL_PATH_LEN)
            
            #for each verticle create a path from bottom to top, then horizonal path 1 meter
            path = PoseArray()

            #create header
            path.header.frame_id = self.world_frame
            path.header.stamp = rospy.Time.now()
            
            start_point = self.point_array[0]
            print("Start point: " + str(start_point))

            for i in range(0, num_verticle_paths):
                #verticle path up
                next_point = start_point
                next_point.z = start_point.z + height
                path_segments = self.GetPathWithService(start_point,next_point)
                if(path_segments == None):
                    return None
                path_segments = ConvertMeshMsgSegmentsToPath(path_segments)
                start_point = next_point
                path.poses += path_segments.poses

                #verticle path going down, can fake it since it is just rever
                next_point.z = next_point.z - height
                path_segments = self.GetPathWithService(start_point, next_point)
                if(path_segments == None):
                    return None
                path_segments = ConvertMeshMsgSegmentsToPath(path_segments)
                start_point = next_point
                path.poses += path_segments.poses

                #horizontal path
                next_point.x = next_point.x + HORIZONTAL_PATH_LEN
                path_segments = self.GetPathWithService(start_point, next_point)
                if(path_segments == None):
                    return None
                path_segments = ConvertMeshMsgSegmentsToPath(path_segments)
                path.poses += path_segments.poses
                start_point = next_point


            path2 = ConvertPoseArrayToPath(path)
            path_pub.publish(path2)
            return path
        
    def RunAreaMissionRightToLeft(self, task_client):
        start_point = self.point_array[0]
        height = abs(self.point_array[0].z - self.point_array[1].z)
        

    def ExecuteMission(self, task_client):
        if(self.mission_type == MISSION_TYPE_INSPECT_POINT):
            #execute mission
            rospy.loginfo("Executing go to point: " + str(self.end_point))
            x = self.end_point.x
            y = self.end_point.y
            z = self.end_point.z
            # task_client.GoToMesh(goal_x=x, goal_y=y, goal_z=z, linear=LINEAR_SPEED, min_rot_gain=ROTATION_GAIN)
            # task_client.GoToMeshWithConstraint(goal_x=x, goal_y=y, goal_z=z, linear=LINEAR_SPEED, min_rot_gain=ROTATION_GAIN)
            task_client.GoToMesh(linear=0.5,goal_x=x,goal_y=y,goal_z=z,threshold=0.05,max_vrot=5.0,min_rot_gain=5.0, exp_vrot=3.0,max_alpha=3.0,max_dy=2.0,k_y=1.0,k_alpha=-5.0)
            return
        if(self.mission_type == MISSION_TYPE_INSPECT_PATH):
            # execute path
            print("Executing go to path mission")
            print("Path has " + str(len(self.point_array)) + " points")
            for i in range(0, len(self.point_array)):
                print("Going to point " + str(i) + " of " + str(len(self.point_array)-1))
                point = self.point_array[i]
                print(self.point_array)
                # task_client.GoToMesh(goal_x=point.x, goal_y=point.y, goal_z=point.z, linear=LINEAR_SPEED, min_rot_gain=ROTATION_GAIN)
            task_client.GoToMesh(linear=0.5,goal_x=point.x,goal_y=point.y,goal_z=point.z,threshold=0.05,max_vrot=5.0,min_rot_gain=5.0, exp_vrot=3.0,max_alpha=3.0,max_dy=2.0,k_y=1.0,k_alpha=-5.0)
        
    def GetStartPoint(self):
        return self.start_point

    def GetEndPoint(self):
        return self.end_point
    
    def GetReference(self):
        return self.mission_reference
    
    def GetType(self):
        return self.mission_type
    

    #get path from mesh planner
    def GetPathWithService(self, start, end):
        stamp_start = PointStamped()
        stamp_start.header.frame_id = self.world_frame
        stamp_start.header.stamp = rospy.Time.now()
        stamp_start.point.x = start.x
        stamp_start.point.y = start.y
        stamp_start.point.z = start.z

        stamp_end = PointStamped()
        stamp_end.header.frame_id = self.world_frame
        stamp_end.header.stamp = rospy.Time.now()
        stamp_end.point.x = end.x
        stamp_end.point.y = end.y
        stamp_end.point.z = end.z

        try:
            resp1 = self.mesh_planner_service(stamp_start, stamp_end)
            return resp1.path
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
            return None

#Mission Group class
class MissionGroup:
    #initialize mission group
    def __init__(self, mesh_planner_service, mission_group_id, mission_array, current_robot_position, world_frame):
        self.mission_group_id = mission_group_id
        self.mission_array = []
        self.current_robot_position = current_robot_position
        self.world_frame = world_frame
        self.group_has_been_executed = False;
        self.mesh_planner_service = mesh_planner_service

        #add missions to mission array
        for i in range(0, len(mission_array)):
            mission = Mission(self.mesh_planner_service, i, mission_array[i].mission_type, mission_array[i].points, world_frame )
            self.mission_array.append(mission)

    
    def GetPathWithService(self, start, end):
        stamp_start = PointStamped()
        stamp_start.header.frame_id = self.world_frame
        stamp_start.header.stamp = rospy.Time.now()
        stamp_start.point.x = start.x
        stamp_start.point.y = start.y
        stamp_start.point.z = start.z

        stamp_end = PointStamped()
        stamp_end.header.frame_id = self.world_frame
        stamp_end.header.stamp = rospy.Time.now()
        stamp_end.point.x = end.x
        stamp_end.point.y = end.y
        stamp_end.point.z = end.z
        try:
            resp1 = self.mesh_planner_service(stamp_start, stamp_end)
            return resp1.path
        except:
            print("Service call failed")
            return None


    #override index operator
    def __getitem__(self, index):
        return self.mission_array[index]

    def PrepareMissionsResponse(self):
        #get PoseArray for each mission
        mission_path_array = []
        mission_reference_array = []
        
        for i in range(0, len(self.mission_array)):
            #check of mission is go to point, don't need transition path for that
            if(self.mission_array[i].GetType() == MISSION_TYPE_INSPECT_POINT):
                #get path from mesh planner
                path = self.GetPathWithService(self.current_robot_position, self.mission_array[i].GetStartPoint())
                #if path is None, then mission is not possible
                if(path == None):
                    print("Mission is not possible")
                    return MISSION_ID_ON_ERROR, None, []
        
                #returns PoseArray
                path = ConvertMeshMsgSegmentsToPath(path)
                path2 = ConvertPoseArrayToPath(path)
                path_pub.publish(path2)
                
                #add path to mission path array
                mission_path_array.append(path)
                mission_reference_array.append(self.mission_array[i].GetReference())
                #update current robot position with destination
                self.current_robot_position = self.mission_array[i].GetEndPoint()
                continue
            else:
                #create transition path
                path = self.GetPathWithService(self.current_robot_position, self.mission_array[i].GetStartPoint())
                #if path is None, then mission is not possible
                if(path == None):
                    print("Mission is not possible")
                    return MISSION_ID_ON_ERROR, None, []
                path = ConvertMeshMsgSegmentsToPath(path)
                path2 = ConvertPoseArrayToPath(path)
                path_pub.publish(path2)
                mission_path_array.append(path)
                mission_reference_array.append(-1)
                #update current robot position with destination
                self.current_robot_position = self.mission_array[i].GetStartPoint()
            
            #get mission path
            path = self.mission_array[i].GetMissionPath()
            #if path is None, then mission is not possible
            if(path == None):
                print("Mission is not possible")
                return MISSION_ID_ON_ERROR, None, []
            mission_path_array.append(path)
            mission_reference_array.append(self.mission_array[i].GetReference())
            #update current robot position with destination
            self.current_robot_position = self.mission_array[i].GetEndPoint()

        return self.mission_group_id, mission_path_array, mission_reference_array
    def ExecuteMissions(self, task_client):
        #iterate over missions and execute them
        for i in range(0, len(self.mission_array)):
            #got to mission start point, if mission is not go to point
            if(self.mission_array[i].GetType() != MISSION_TYPE_INSPECT_POINT):
                start_point = self.mission_array[i].GetStartPoint()
                task_client.GoToMesh(goal_x=start_point.x, goal_y=start_point.y, goal_z=start_point.z, linear=LINEAR_SPEED, min_rot_gain=ROTATION_GAIN)
            #execute mission
            self.mission_array[i].ExecuteMission(task_client)
        
        #set group has been executed to true
        self.group_has_been_executed = True

    def CheckIfGroupHasBeenExecuted(self):
        return self.group_has_been_executed

#Mission Manager class
class MissionManager:
    #initialize mission manager
    def __init__(self):
        #get parameters from ros parameter server
        self.world_frame = rospy.get_param('world_frame', '/world')
        self.robot_frame = rospy.get_param('robot_frame', '/base_link')
        self.trans = None
        self.rot = None
        self.missions_to_execute = []
        self.mission_group_counter = 0
        self.latest_robot_position = None
        self.mission_groups = {}
        self.mission_execution_queue = []
        self.queue_mutex = Lock()
        rospy.wait_for_service('/mesh_planner/plan_stamped')
        self.mesh_planner_service = rospy.ServiceProxy('/mesh_planner/plan_stamped', PlanRequestStamped)

        return


    def PrepareMissions(self, req):
        #create id for group of missions
        mission_id = self.mission_group_counter
        self.mission_group_counter += 1

        counter = 0
        while(True):
            if(self.latest_robot_position == None):
                rospy.logerr("Latest robot position is None")
                #wait a bit and try again
                rospy.sleep(1)
                counter += 1
                if(counter > 5):
                    #exit ros node
                    rospy.signal_shutdown("Latest robot position is None, and timeout waiting for it")
                continue
            else:
                break

        #create mission group
        mission_group = MissionGroup(self.mesh_planner_service, mission_id, req.missions ,self.latest_robot_position, self.world_frame)

        #console log mission group
        rospy.loginfo("Mission group " + str(mission_id) + " created")

        #add mission group to mission groups
        self.mission_groups[mission_id] = mission_group
        
        id, paths, references = mission_group.PrepareMissionsResponse()
        
        #send mission to task manager
        return PrepareMissionsResponse(id, paths, references)

    
    def MissionExecutionSrv(self, req):
        
        #check if mission group exists
        if(req.id not in self.mission_groups):
            rospy.logerr("Mission group " + str(req.id) +" does not exist")
            return ExecuteMissionsResponse(False)

        #check if mission group has been executed
        if(self.mission_groups[req.id].CheckIfGroupHasBeenExecuted()):
            rospy.logerr("Mission group " + str(req.id) + " has already been executed")
            return ExecuteMissionsResponse(False)


        mission_group = self.mission_groups[req.id]
        
        #add mission group to mission groups to execute
        self.queue_mutex.acquire()
        self.mission_execution_queue.append(mission_group)
        self.queue_mutex.release()

        return ExecuteMissionsResponse(True)

    def RobotPosCb(self, data):
        self.latest_robot_position = Point()
        #store latest robot position with 3 decimals
        self.latest_robot_position.x = round(data.pose.position.x, 3)
        self.latest_robot_position.y = round(data.pose.position.y, 3)
        self.latest_robot_position.z = round(data.pose.position.z, 3)
        #self.latest_robot_position = data.pose.position

    #run planner as ros node
    def RunPlanner(self):
        global path_pub
        rospy.init_node('mission_manager')
        server_node = rospy.get_param("~server","/crawler_tasks1")
        default_period = rospy.get_param("~period",0.2)
        path_pub = rospy.Publisher('/mission_manager/planned_path', NavPath, queue_size=10)    
        self.tc = TaskClient(server_node,default_period)

        #subscribe to robot position
        rospy.Subscriber("/mesh_pf1/pose", PoseStamped , self.RobotPosCb)

        #provite mission preparing service
        s1 = rospy.Service('~prepare_missions', PrepareMissions, self.PrepareMissions)
        s2 = rospy.Service('~execute_missions', ExecuteMissions, self.MissionExecutionSrv)
        
        rate = rospy.Rate(10) # 10hz
        rospy.loginfo("Mission manager waiting for missions")


        while not rospy.is_shutdown():
            #check if there are any mission groups to execute
            self.queue_mutex.acquire()
            num_missions = len(self.mission_execution_queue);
            self.queue_mutex.release()

            for i in range(0, num_missions):
                #execute mission groups
                self.queue_mutex.acquire()
                mission_group = self.mission_execution_queue.pop(0)
                self.queue_mutex.release()
                mission_group.ExecuteMissions(self.tc)
                #mission group has been executed
                rospy.loginfo("Mission group " + str(mission_group.mission_group_id) + " has been executed")
            rate.sleep()

            
#main function
if __name__ == '__main__':
    #this topis shows the pose array as a path in rviz, for debugging
    mission_manager = MissionManager()
    mission_manager.RunPlanner()
