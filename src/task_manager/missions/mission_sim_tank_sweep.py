#!/usr/bin/python
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import rospy
from math import *
from task_manager_lib.TaskClient import *

rospy.init_node('task_client1')
server_node = rospy.get_param("~server","/crawler_tasks1")
default_period = rospy.get_param("~period",0.2)
tc = TaskClient(server_node,default_period)


tc.GoToMesh(linear=0.5, goal_x=10.0, goal_y=-5.0, goal_z=6.0, min_rot_gain=0.5, exp_vrot=0.15, k_y=2.5, k_alpha=-2.5)

tc.ReachAngle(target=0.0, threshold=0.2);
tc.VerticalTransect(altitude=10,linear=0.3, use_tf=True)
tc.ReachAngle(target=pi/2, use_tf=True, threshold=0.2);
for i in range(3):
    tc.HorizontalTransect(relative=True,positive=True,linear=0.5,distance=5.0, use_tf=True)
    tc.ReachAngle(target=0, threshold=0.2);
    tc.VerticalTransect(relative=True,altitude=-0.5,linear=0.5, use_tf=True)
    tc.ReachAngle(target=-pi/2, use_tf=True, threshold=0.2);
    tc.HorizontalTransect(relative=True,positive=False,linear=0.5,distance=5.0, use_tf=True)
    tc.ReachAngle(target=0, use_tf=True, threshold=0.2);
    tc.VerticalTransect(relative=True,altitude=-0.5,linear=0.5, use_tf=True)
    tc.ReachAngle(target=pi/2, use_tf=True, threshold=0.2);
tc.VerticalTransect(altitude=5.0,linear=0.5, use_tf=True)




rospy.loginfo("Mission completed")


