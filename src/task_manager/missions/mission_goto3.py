#!/usr/bin/python
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
import rospy
from math import *
from task_manager_lib.TaskClient import *

rospy.init_node('task_client3')
server_node = rospy.get_param("~server","/crawler_tasks3")
default_period = rospy.get_param("~period",0.2)
tc = TaskClient(server_node,default_period)

tc.SetStatusSync(status=0);
tc.GoToMesh(goal_x=9.0, goal_y=-10, goal_z=4, linear=0.5)
tc.SetStatusSync(status=1);
tc.WaitForStatusSync(partner="partner1",status=2);
tc.GoToMesh(goal_x=9.0, goal_y=-15.0, goal_z=4, linear=0.2)
tc.GoToMesh(goal_x=9.0, goal_y=-15.0, goal_z=0, linear=0.2)
tc.GoToMesh(goal_x=9.0, goal_y=-10.0, goal_z=0, linear=0.2)
tc.GoToMesh(goal_x=9.0, goal_y=-5, goal_z=2, linear=0.2)
tc.SetStatusSync(status=2);
tc.WaitForStatusSync(partner="partner1",status=3);
tc.GoToMesh(goal_x=9.0, goal_y=-1, goal_z=4.5, linear=0.5)
tc.SetStatusSync(status=0);


rospy.loginfo("Mission3 completed")


