#include <math.h>
#include "TaskWaitForButton.h"
#include "task_manager_crawler/TaskWaitForButtonConfig.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskWaitForButton::initialise()  {
    button_sub = env->getNodeHandle().subscribe("/joy",1,&TaskWaitForButton::buttonCallback,this);
    return TaskStatus::TASK_INITIALISED;
}

void TaskWaitForButton::buttonCallback(const sensor_msgs::Joy::ConstPtr& msg) {
    std::vector<int32_t> buttons =(*msg).buttons;
    if(buttons[cfg.success_button]){
        cont_current_task = true;
    }
    if(buttons[cfg.fail_button]){
        end_current_task = true;
    }

}

TaskIndicator TaskWaitForButton::iterate()
{
    if (cont_current_task) {
        return TaskStatus::TASK_COMPLETED;
    }
    else if (end_current_task){
        return TaskStatus::TASK_FAILED;
    }
    return TaskStatus::TASK_RUNNING;
}

DYNAMIC_TASK(TaskFactoryWaitForButton);
