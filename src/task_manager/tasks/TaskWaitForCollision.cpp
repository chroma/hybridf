#include <math.h>
#include "TaskWaitForCollision.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskWaitForCollision::initialise()  {

    return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskWaitForCollision::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskWaitForCollision: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }
    if(env->objDetected()){
        if (env->getObjDist() < cfg.depth) {
            ROS_INFO("Detected Object at %.2f",env->getObjDist());
            return TaskStatus::TASK_COMPLETED;
        }
    }
	return TaskStatus::TASK_RUNNING;
}

DYNAMIC_TASK(TaskFactoryWaitForCollision);
