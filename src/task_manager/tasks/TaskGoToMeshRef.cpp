#include <math.h>
#include "tf/LinearMath/Vector3.h"
#include "tf/transform_datatypes.h"
#include "TaskGoToMesh.h"
#include "mesh_planner/PlanRequest.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskGoToMesh::initialise()  {

    if (!ros::service::waitForService("/mesh_planner/plan", ros::Duration(0.5))) {
        ROS_ERROR("Service /mesh_planner/plan is not available");
        return TaskStatus::TASK_INITIALISATION_FAILED;
    }
    ros::ServiceClient client = env->getNodeHandle().serviceClient<mesh_planner::PlanRequest>("/mesh_planner/plan");
    mesh_planner::PlanRequest srv;
    const geometry_msgs::Point & P = env->getUwb();
    srv.request.start = P;
    srv.request.end.x = cfg.goal_x;
    srv.request.end.y = cfg.goal_y;
    srv.request.end.z = cfg.goal_z;
    ROS_INFO("Calling service /mesh_planner/plan");
    if (client.call(srv))
    {
        current_segment = 0;
        path = srv.response.path;
        ROS_INFO("Planned path from (%.2f,%.2f,%.2f) to (%.2f,%.2f,%.2f): %d segments", 
                P.x,P.y,P.z, cfg.goal_x, cfg.goal_y, cfg.goal_z,(int)path.segments.size());
    }
    else
    {
        ROS_ERROR("Failed to call service /mesh_planner/plan");
        return TaskStatus::TASK_INITIALISATION_FAILED;
    }


    return TaskStatus::TASK_INITIALISED;
}
            
static inline double square(double x) {
    return x*x;
}

static inline double hypot(double x,double y,double z) {
    return sqrt(x*x+y*y+z*z);
}

TaskIndicator TaskGoToMesh::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskGoToMesh: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }
    const geometry_msgs::Point & P = env->getUwb();
    // TODO: express the goal point in the robot frame (this requires computing the normal from the acceleration)
    double distance = hypot(P.x-cfg.goal_x, P.y-cfg.goal_y,P.z-cfg.goal_z);
    if (distance < cfg.threshold) {
        ROS_INFO("Reached target position on mesh");
		return TaskStatus::TASK_COMPLETED;
    }

    double theta = env->getHeading();

    double x,y; // coordinate in the current segment frame
    double target_theta;

    while (current_segment < path.segments.size()) {
        const geometry_msgs::Point & S = path.segments[current_segment].start;
        const geometry_msgs::Point & E = path.segments[current_segment].end;
        // [ux,uy,uz] is a reference frame aligned with the hull normal (z)
        // with x along the path segment
        tf::Vector3 ux,uy,uz;
        tf::vector3MsgToTF(path.segments[current_segment].ux,ux);
        tf::vector3MsgToTF(path.segments[current_segment].uy,uy);
        tf::vector3MsgToTF(path.segments[current_segment].uz,uz);

        // [vx,vy,vz] is a reference frame aligned with the hull normal (z), 
        // with x horizontal, used only to compute the target theta
        tf::Vector3 vz(uz);
        tf::Vector3 vx = vz.cross(tf::Vector3(0,0,-1));
        if (vx.length() > 1e-3) {
            vx /= vx.length();
        } else {
            vx = tf::Vector3(1,0,0);
        }
        tf::Vector3 vy = vz.cross(vx).normalize();
        tf::Vector3 se(E.x-S.x,E.y-S.y,E.z-S.z);
        tf::Vector3 sp(P.x-S.x,P.y-S.y,P.z-S.z);
        x = se.dot(vx); y = se.dot(vy);

        target_theta = M_PI/2 - atan2(y,x); // theta is defined using gravity

        x = sp.dot(ux);
        if (x > se.length()) {
            current_segment++;
            ROS_INFO("Switching to segment %d",current_segment);
            continue;
        }
        y = sp.dot(uy);
        break;
    }

    if (current_segment >= path.segments.size()) {
        ROS_WARN("Reach final segment with an error larger than threshold");
		return TaskStatus::TASK_COMPLETED;
    }


    double alpha = remainder(target_theta - theta,2*M_PI);
    double dy = CrawlerEnv::saturate(y,cfg.max_dy);

    double alpha_gain = exp(-0.5*square(alpha/cfg.max_alpha));
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha - cfg.k_y*alpha_gain*y, cfg.max_vrot);
    
    double rot_gain = exp(-0.5*square(rot/(3*cfg.max_vrot)));
    printf("I %d D %.1f P %.2f %.2f %.2f T %.3f / %.3f X %.2f %.2f A %.3f dY %.1f rot %.3f\n", current_segment, distance,P.x,P.y,P.z,theta,target_theta,x,y,alpha,dy,rot);
    // printf("Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", P.z,theta,alpha,dz,rot);
    env->publishVelocity(cfg.linear * rot_gain , rot);

	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskGoToMesh::terminate()
{
    env->publishVelocity(0,0);
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryGoToMesh);
