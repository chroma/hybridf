#ifndef TASK_WAITFORCOLLISION_H
#define TASK_WAITFORCOLLISION_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskWaitForCollisionConfig.h"


using namespace task_manager_lib;

namespace task_manager_crawler {
    class TaskWaitForCollision : public TaskInstance<TaskWaitForCollisionConfig, CrawlerEnv>
    {
        protected:

        public:
            TaskWaitForCollision(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskWaitForCollision() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

    };
    class TaskFactoryWaitForCollision : public TaskDefinition<TaskWaitForCollisionConfig, CrawlerEnv, TaskWaitForCollision>
    {
        public:
            TaskFactoryWaitForCollision(TaskEnvironmentPtr env) :
                Parent("WaitForCollision","Reach a desired destination",true,env) {}
            virtual ~TaskFactoryWaitForCollision() {};
    };
};

#endif // TASK_WAITFORCOLLISION_H
