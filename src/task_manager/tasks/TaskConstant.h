#ifndef TASK_CONSTANT_H
#define TASK_CONSTANT_H

#include <ros/ros.h>
#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskConstantConfig.h"

using namespace task_manager_lib;


namespace task_manager_crawler {
    class TaskConstant : public TaskInstance<TaskConstantConfig,CrawlerEnv>
    {

        protected:
            ros::Time initial_time;
        public:
            TaskConstant(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskConstant() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryConstant : public TaskDefinition<TaskConstantConfig, CrawlerEnv, TaskConstant>
    {

        public:
            TaskFactoryConstant(TaskEnvironmentPtr env) : 
                Parent("Constant","Apply a constant command for a given duration",true,env) {}
            virtual ~TaskFactoryConstant() {};
    };
};

#endif // TASK_CONSTANT_H
