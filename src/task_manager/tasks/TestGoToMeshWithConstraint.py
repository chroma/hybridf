import numpy as np

# Define the robot's poses as vertices in a graph
poses = {
    'start': (0, 0),
    'pose1': (1, 1),
    'pose2': (2, 2),
    'end': (3, 3)
}

# Define edges and their costs between the poses
edges = [
    ('start', 'pose1', 1),
    ('start', 'pose2', 2),
    ('pose1', 'pose2', 1),
    ('pose1', 'end', 2),
    ('pose2', 'end', 1)
]

# Implement Dijkstra's algorithm
def dijkstra(graph, start):
    shortest_path = {pose: float('inf') for pose in graph}
    shortest_path[start] = 0
    not_visited = set(graph)

    while not_visited:
        current_pose = min(not_visited, key=lambda pose: shortest_path[pose])
        not_visited.remove(current_pose)

        for neighbor, weight in graph[current_pose].items():
            potential_path = shortest_path[current_pose] + weight
            if potential_path < shortest_path[neighbor]:
                shortest_path[neighbor] = potential_path

    return shortest_path

# Create an adjacency dictionary from edges
graph = {pose: {} for pose in poses}
for edge in edges:
    start, end, cost = edge
    graph[start][end] = cost
    graph[end][start] = cost

# Find the shortest path
start_pose = 'start'
end_pose = 'end'
shortest_path = dijkstra(graph, start_pose)

# Print the shortest path
print(f'Shortest path from {start_pose} to {end_pose} with costs:')
for pose, cost in shortest_path.items():
    print(f'{pose}: {cost}')

# You can use this information to control the robot's motion to stay upright and not point down as it moves from one pose to another.

