#include <math.h>
#include "TaskPointTowards.h"
#include "task_manager_crawler/TaskPointTowardsConfig.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;

TaskIndicator TaskPointTowards::initialise()
{
    global_goal = tf2::Vector3(cfg.goal_x,cfg.goal_y,cfg.goal_z);
    tf2::Transform Trob = env->lookupTransform();
    switch (cfg.relative) {
        case TaskPointTowards_RelativePose:
            // The target is defined in the robot frame. 
            global_goal = Trob * global_goal;
            break;
        case TaskPointTowards_RelativePosition:
            // The target is defined in the global orientation frame, but
            // offset by the robot position
            global_goal += Trob.getOrigin();
            break;
        case TaskPointTowards_RelativeGravity:
            // The target is defined in a local frame aligned with gravity.
            // (0,0,1) is up, x is horizontal and y obtained by cross product 
            {
                tf2::Matrix3x3 R = Trob.getBasis();
                tf2::Vector3 normal = R.getColumn(2);
                tf2::Vector3 uz = tf2::Vector3(0,0,1);
                tf2::Vector3 ux = normal.cross(uz);
                if (ux.length()<1e-3) {
                    ROS_ERROR("Cannot use relative gravity on horizontal surfaces");
                    return TaskStatus::TASK_INITIALISATION_FAILED;
                }
                ux /= ux.length();
                tf2::Vector3 uy = uz.cross(ux);
                uy /= uy.length();
                global_goal = Trob.getOrigin() + ux * cfg.goal_x 
                    + uy * cfg.goal_y + uz * cfg.goal_z;
            }
            break;
        case TaskPointTowards_Absolute:
        default:
            break;
    }
	return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskPointTowards::iterate()
{
    tf2::Transform Trob = env->lookupTransform();
    tf2::Vector3 goal = Trob.inverse() * global_goal; 

    if (goal.length() < cfg.dist_threshold) {
        ROS_ERROR("Point (%f,%f,%f) is too close to point towards it.",
                global_goal.x(),global_goal.y(),global_goal.z());
        return TaskStatus::TASK_FAILED;
    }
    double norm2d = hypot(goal.y(),goal.x()) / goal.length();
    if (norm2d < cfg.dir_threshold) {
        ROS_ERROR("Point (%f,%f,%f) is too close to the robot zenith to point towards it.",
                global_goal.x(),global_goal.y(),global_goal.z());
        return TaskStatus::TASK_FAILED;
    }
    double theta = atan2(goal.y(),goal.x());
    ROS_INFO("GOAL == %f /, %f", goal.y(),goal.x());
    double alpha = remainder(- theta,2*M_PI);
    if (fabs(alpha) < cfg.threshold) {
		return TaskStatus::TASK_COMPLETED;
    }
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha, cfg.max_vrot);
    env->publishVelocity(0.0 , rot);
    ROS_INFO("Task Points Towards- DeltaRot: %2.3f :: Current: %2.3f :: rot: %2.3f", alpha, theta, rot);

	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskPointTowards::terminate()
{
    env->publishVelocity(0,0);
	return Parent::terminate();
}

DYNAMIC_TASK(TaskFactoryPointTowards);
