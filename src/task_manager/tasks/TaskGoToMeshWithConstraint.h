#ifndef TASK_GoToMeshWithConstraint_MESH_H
#define TASK_GoToMeshWithConstraint_MESH_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskGoToMeshWithConstraintConfig.h"
#include "task_manager_crawler/Path.h"

using namespace task_manager_lib;

namespace task_manager_crawler {
    class TaskGoToMeshWithConstraint : public TaskInstance<TaskGoToMeshWithConstraintConfig, CrawlerEnv>
    {
        protected:
            task_manager_crawler::Path path;
            unsigned int current_segment;

        public:
            TaskGoToMeshWithConstraint(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskGoToMeshWithConstraint() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryGoToMeshWithConstraint : public TaskDefinition<TaskGoToMeshWithConstraintConfig, CrawlerEnv, TaskGoToMeshWithConstraint>
    {

        public:
            TaskFactoryGoToMeshWithConstraint(TaskEnvironmentPtr env) : 
                Parent("GoToMeshWithConstraint","Reach a desired destination on the mesh",true,env) {}
            virtual ~TaskFactoryGoToMeshWithConstraint() {};
    };
};

#endif // TASK_GoToMeshWithConstraint_MESH_H
