
#include <math.h>
#include "TaskVerticalTransect.h"
#include "task_manager_crawler/TaskVerticalTransectConfig.h"

using namespace task_manager_msgs;
using namespace task_manager_lib;

using namespace task_manager_crawler;

// #define DEBUG_GOTO
TaskIndicator TaskVerticalTransect::initialise() 
{
    tf2::Transform pose = env->lookupTransform();
    initial_height_ = pose.getOrigin().z();
    double target_height = cfg.relative?(initial_height_+cfg.altitude):cfg.altitude;
    ROS_INFO("Vertical transect: current height %.2f going to %.2f",initial_height_,target_height);
    positive_ = (target_height - initial_height_) > 0;
    return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskVerticalTransect::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskHorizontalTransect: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }

    tf2::Transform pose = env->lookupTransform();

    //Assumes we are in a world frame where z implies height
    //If using odom_ekf this should be changed to y()
    double current_height = pose.getOrigin().z();

    double target_height = cfg.relative?(initial_height_+cfg.altitude):cfg.altitude;
    double dz = target_height - current_height;
    if (fabs(dz) < cfg.threshold) {
		return TaskStatus::TASK_COMPLETED;
    }

    double theta = env->getHeading();
    if (cfg.force_positive) {
        double target_theta = 1.57;
        double alpha = remainder(target_theta - theta,2*M_PI);
        double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha, cfg.max_vrot);
        double rot_gain = exp(-0.5*(rot*rot)/(9*cfg.max_vrot*cfg.max_vrot));
        double linear = CrawlerEnv::saturate(cfg.k_z * dz,cfg.linear);
        printf("Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", current_height,theta,alpha,dz,rot);
        env->publishVelocity(linear * rot_gain , rot);
    } else {
        if (!positive_) {
            dz = -dz;
        }
        double target_theta = positive_?1.57:-1.57;
        double alpha = remainder(target_theta - theta,2*M_PI);
        double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha, cfg.max_vrot);
        double rot_gain = exp(-0.5*(rot*rot)/(9*cfg.max_vrot*cfg.max_vrot));
        double linear = CrawlerEnv::saturate(cfg.k_z * dz,cfg.linear);
        printf("Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", current_height,theta,alpha,dz,rot);
        env->publishVelocity(linear * rot_gain , rot);
    }
	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskVerticalTransect::terminate()
{
    env->publishVelocity(0,0);
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryVerticalTransect);
