#include <math.h>
#include "TaskReachAngle.h"
#include "task_manager_crawler/TaskReachAngleConfig.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;

TaskIndicator TaskReachAngle::initialise()
{
    initial_theta = env->getHeading();
	return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskReachAngle::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskReachAngle: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }

    double theta = env->getHeading();

    double target = cfg.relative?(initial_theta+cfg.target):cfg.target;
    double alpha = remainder(target - theta,2*M_PI);
    if (fabs(alpha) < cfg.threshold) {
		return TaskStatus::TASK_COMPLETED;
    }
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha, cfg.max_vrot);
    env->publishVelocity(0.0 , rot);
    ROS_INFO("Task Reach Angle- DeltaRot: %2.3f :: Current: %2.3f :: target: %2.3f :: rot: %2.3f", alpha, theta, target, rot);

	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskReachAngle::terminate()
{
    env->publishVelocity(0,0);
	return Parent::terminate();
}

DYNAMIC_TASK(TaskFactoryReachAngle);
