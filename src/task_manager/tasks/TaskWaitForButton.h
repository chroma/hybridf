#ifndef TASK_WAITFORBUTTON_H
#define TASK_WAITFORBUTTON_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskWaitForButtonConfig.h"
#include "sensor_msgs/Joy.h"


using namespace task_manager_lib;
#define CONTINUE_TASK_BUTTON    3 // on logitech this is button Y per http://wiki.ros.org/joy
#define NEXT_TASK_BUTTON        4 // on logitech this is button X per same site

namespace task_manager_crawler {
    class TaskWaitForButton : public TaskInstance<TaskWaitForButtonConfig, CrawlerEnv>
    {
        protected:
            ros::Subscriber button_sub;
            bool cont_current_task = false;
            bool end_current_task = false;

        void buttonCallback(const sensor_msgs::Joy::ConstPtr& msg);

        public:
            TaskWaitForButton(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskWaitForButton() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

    };
    class TaskFactoryWaitForButton : public TaskDefinition<TaskWaitForButtonConfig, CrawlerEnv, TaskWaitForButton>
    {
        public:
            TaskFactoryWaitForButton(TaskEnvironmentPtr env) :
                Parent("WaitForButton","Reach a desired destination",true,env) {}
            virtual ~TaskFactoryWaitForButton() {};
    };
};

#endif // TASK_WAITFORBUTTON_H
