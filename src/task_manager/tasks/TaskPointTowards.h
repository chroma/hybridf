#ifndef TASK_POINT_TOWARDS_H
#define TASK_POINT_TOWARDS_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskPointTowardsConfig.h"
using namespace task_manager_lib;

namespace task_manager_crawler {
    class TaskPointTowards : public TaskInstance<TaskPointTowardsConfig,CrawlerEnv>
    {
        protected:
            tf2::Vector3 global_goal;
        public:
            TaskPointTowards(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskPointTowards() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryPointTowards : public TaskDefinition<TaskPointTowardsConfig, CrawlerEnv, TaskPointTowards>
    {
        public:
            TaskFactoryPointTowards(TaskEnvironmentPtr env) : 
                Parent("PointTowards","Turns on the spot to align the robot with a target point",true,env) {}
            virtual ~TaskFactoryPointTowards() {};
    };
};

#endif // TASK_POINT_TOWARDS_H
