#ifndef TASK_FOLLOW_BEAM_H
#define TASK_FOLLOW_BEAM_H

#include <ros/ros.h>
#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskFollowBeamConfig.h"

#include "floor_plane_ransac/BeamDetection.h"

using namespace task_manager_lib;


namespace task_manager_crawler {
    class TaskFollowBeam : public TaskInstance<TaskFollowBeamConfig,CrawlerEnv>
    {

        protected:
            ros::Subscriber beam_sub;
            floor_plane_ransac::BeamDetection detection;

            void beamCb(const floor_plane_ransac::BeamDetectionConstPtr msg) {
                detection = *msg;
            }

        public:
            TaskFollowBeam(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskFollowBeam() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryFollowBeam : public TaskDefinition<TaskFollowBeamConfig, CrawlerEnv, TaskFollowBeam>
    {

        public:
            TaskFactoryFollowBeam(TaskEnvironmentPtr env) : 
                Parent("FollowBeam","Follow a stiffener beam",true,env) {}
            virtual ~TaskFactoryFollowBeam() {};
    };
};

#endif // TASK_FOLLOW_BEAM_H
