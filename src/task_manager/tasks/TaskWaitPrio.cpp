#include <math.h>
#include "tf2/LinearMath/Vector3.h"
#include "tf2/LinearMath/Transform.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "TaskWaitPrio.h"
#include "task_manager_crawler/GetPrio.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskWaitPrio::initialise()  {
	std::cout<<"INIT"<<std::endl;

    return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskWaitPrio::iterate()
{
	bool check_test=true;
	// check with simon if it is needed to be call from the task
	int robot_id = cfg.robot_id;
	srv.request.x = cfg.x;
	srv.request.y = cfg.y;
	std::cout<<"ENTER"<<std::endl;

	if (!ros::service::waitForService("/get_prio", ros::Duration(2.0))) {
		std::cout<<"		Not ENTER"<<std::endl;
		// ROS_INFO("Waiting for service call /wait_prio");
		return TaskStatus::TASK_RUNNING;
	}
	else{
		if (client.call(srv)) {
				// Check if the first element of the received priorities matches the robot_id
				std::cout<<"		ENTER"<<std::endl;
				if (srv.response.prio.empty() || (srv.response.prio[srv.response.prio.size()-1] == robot_id))  {
					ROS_INFO("Crawler %d has priority for its next waypoint",robot_id);
					return TaskStatus::TASK_COMPLETED;
				} else {
					ROS_INFO("Crawler %d doesn't have priority for its next waypoint",robot_id);
				}
			}
			else {
				ROS_WARN("Received priorities list is empty.");
			}
		 
	}
	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskWaitPrio::terminate()
{
	std::cout<<"TERMINATE"<<std::endl;

	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryWaitPrio);


