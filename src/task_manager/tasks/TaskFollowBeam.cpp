
#include <math.h>
#include "TaskFollowBeam.h"
#include "std_srvs/SetBool.h"
#include "task_manager_crawler/TaskFollowBeamConfig.h"

using namespace task_manager_msgs;
using namespace task_manager_lib;

using namespace task_manager_crawler;

// #define DEBUG_GOTO

TaskIndicator TaskFollowBeam::initialise() 
{
    beam_sub = env->getNodeHandle().subscribe("/floor_profile_ransac/beam",1,&TaskFollowBeam::beamCb,this);

    if (!cfg.detection_service.empty()) {
        ros::ServiceClient client = env->getNodeHandle().serviceClient<std_srvs::SetBool>(cfg.detection_service);
        std_srvs::SetBool activate;
        activate.request.data = true;
        if (client.call(activate)) {
            ROS_INFO("Activate floor_profile_ransac detection: %d:%s", (int)activate.response.success,activate.response.message.c_str());
        } else {
            ROS_ERROR("TaskFollowBeam: Failed to call service '%s'",cfg.detection_service.c_str());
            return TaskStatus::TASK_INITIALISATION_FAILED;
        }
    }

    return TaskStatus::TASK_INITIALISED;
}


TaskIndicator TaskFollowBeam::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskFollowBeam: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }

    if ((ros::Time::now() - detection.header.stamp).toSec() > 1.) {
        env->publishVelocity(0,0);
        return TaskStatus::TASK_RUNNING;
    }



    double alpha = atan(detection.beam_a);
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha + cfg.k_y*detection.beam_b, cfg.max_vrot);
    double rot_gain = exp(-0.5*(rot*rot)/(9*cfg.max_vrot*cfg.max_vrot));
    printf("D %.3f %.3f A %.3f rot %.3f\n", detection.beam_a,detection.beam_b,alpha,rot);
    env->publishVelocity(cfg.linear * rot_gain , rot);
	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskFollowBeam::terminate()
{
    env->publishVelocity(0,0);
    if (!cfg.detection_service.empty()) {
        ros::ServiceClient client = env->getNodeHandle().serviceClient<std_srvs::SetBool>(cfg.detection_service);
        std_srvs::SetBool activate;
        activate.request.data = false;
        if (client.call(activate)) {
            ROS_INFO("Disable floor_profile_ransac: %d:%s", (int)activate.response.success,activate.response.message.c_str());
        } else {
            ROS_ERROR("TaskFollowBeam: Failed to call service '%s'",cfg.detection_service.c_str());
        }
    }
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryFollowBeam);
