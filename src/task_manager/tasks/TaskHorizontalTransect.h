#ifndef TASK_HORIZ_TRANSECT_H
#define TASK_HORIZ_TRANSECT_H

#include <ros/ros.h>
#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskHorizontalTransectConfig.h"

using namespace task_manager_lib;


namespace task_manager_crawler {
    class TaskHorizontalTransect : public TaskInstance<TaskHorizontalTransectConfig,CrawlerEnv>
    {

        protected:
            tf2::Transform initial_position_;
            double initial_height_;
        public:
            TaskHorizontalTransect(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskHorizontalTransect() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryHorizontalTransect : public TaskDefinition<TaskHorizontalTransectConfig, CrawlerEnv, TaskHorizontalTransect>
    {

        public:
            TaskFactoryHorizontalTransect(TaskEnvironmentPtr env) : 
                Parent("HorizontalTransect","Performs an horizontal transect for a given distance",true,env) {}
            virtual ~TaskFactoryHorizontalTransect() {};
    };
};

#endif // TASK_HORIZ_TRANSECT_H
