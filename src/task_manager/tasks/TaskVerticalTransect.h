#ifndef TASK_VERT_TRANSECT_H
#define TASK_VERT_TRANSECT_H

#include <ros/ros.h>
#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskVerticalTransectConfig.h"

using namespace task_manager_lib;


namespace task_manager_crawler {
    class TaskVerticalTransect : public TaskInstance<TaskVerticalTransectConfig,CrawlerEnv>
    {

        protected:
            double initial_height_;
            bool positive_;
        public:
            TaskVerticalTransect(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskVerticalTransect() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryVerticalTransect : public TaskDefinition<TaskVerticalTransectConfig, CrawlerEnv, TaskVerticalTransect>
    {

        public:
            TaskFactoryVerticalTransect(TaskEnvironmentPtr env) : 
                Parent("VerticalTransect","Performs a vertical transect to a given height",true,env) {}
            virtual ~TaskFactoryVerticalTransect() {};
    };
};

#endif // TASK_VERT_TRANSECT_H
