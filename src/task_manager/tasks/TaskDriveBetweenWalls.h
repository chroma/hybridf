#ifndef TASK_DRIVE_BETWEEN_WALLS_H
#define TASK_DRIVE_BETWEEN_WALLS_H

#include <ros/ros.h>
#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskDriveBetweenWallsConfig.h"

#include "floor_plane_ransac/WallDetection.h"

using namespace task_manager_lib;


namespace task_manager_crawler {
    class TaskDriveBetweenWalls : public TaskInstance<TaskDriveBetweenWallsConfig,CrawlerEnv>
    {

        protected:
            ros::Subscriber wall_sub;
            floor_plane_ransac::WallDetection detection;
            ros::Time last_confidence;

            void beamCb(const floor_plane_ransac::WallDetectionConstPtr msg) {
                detection = *msg;
            }

        public:
            TaskDriveBetweenWalls(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskDriveBetweenWalls() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryDriveBetweenWalls : public TaskDefinition<TaskDriveBetweenWallsConfig, CrawlerEnv, TaskDriveBetweenWalls>
    {

        public:
            TaskFactoryDriveBetweenWalls(TaskEnvironmentPtr env) : 
                Parent("DriveBetweenWalls","Drive between two walls/stiffener",true,env) {}
            virtual ~TaskFactoryDriveBetweenWalls() {};
    };
};

#endif // TASK_DRIVE_BETWEEN_WALLS_H
