#ifndef TASK_GoToMesh_MESH_H
#define TASK_GoToMesh_MESH_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskGoToMeshConfig.h"
#include "task_manager_crawler/Path.h"

using namespace task_manager_lib;

namespace task_manager_crawler {
    class TaskGoToMesh : public TaskInstance<TaskGoToMeshConfig, CrawlerEnv>
    {
        protected:
            task_manager_crawler::Path path;
            unsigned int current_segment;

        public:
            TaskGoToMesh(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskGoToMesh() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryGoToMesh : public TaskDefinition<TaskGoToMeshConfig, CrawlerEnv, TaskGoToMesh>
    {

        public:
            TaskFactoryGoToMesh(TaskEnvironmentPtr env) : 
                Parent("GoToMesh","Reach a desired destination on the mesh",true,env) {}
            virtual ~TaskFactoryGoToMesh() {};
    };
};

#endif // TASK_GoToMesh_MESH_H
