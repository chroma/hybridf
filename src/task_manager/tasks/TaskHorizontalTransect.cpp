
#include <math.h>
#include "TaskHorizontalTransect.h"
#include "task_manager_crawler/TaskHorizontalTransectConfig.h"

using namespace task_manager_msgs;
using namespace task_manager_lib;

using namespace task_manager_crawler;

// #define DEBUG_GOTO

TaskIndicator TaskHorizontalTransect::initialise() 
{
    tf2::Transform pose = env->lookupTransform();
    initial_position_ = pose;
    initial_height_ = pose.getOrigin().z();
    return TaskStatus::TASK_INITIALISED;
}

TaskIndicator TaskHorizontalTransect::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskHorizontalTransect: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }

    tf2::Transform pose = env->lookupTransform();
    double current_height = pose.getOrigin().z();

    double distance_travelled = hypot(pose.getOrigin().x()-initial_position_.getOrigin().x(),
                                      pose.getOrigin().y()-initial_position_.getOrigin().y());
    if (distance_travelled > cfg.distance) {
		return TaskStatus::TASK_COMPLETED;
    }

    double theta = env->getHeading();


    double target_theta = cfg.positive?(0):(M_PI);
    double target_height = cfg.relative?(initial_height_+cfg.altitude):cfg.altitude;

    double alpha = remainder(target_theta - theta,2*M_PI);
    double dz = CrawlerEnv::saturate(target_height - current_height,cfg.max_dz);
    if (!cfg.positive) dz = -dz;

    double alpha_gain = exp(-0.5*(alpha*alpha)/(cfg.max_alpha*cfg.max_alpha));

    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha 
            + cfg.k_z*alpha_gain*dz, cfg.max_vrot);
    double rot_gain = exp(-0.5*(rot*rot)/(9*cfg.max_vrot*cfg.max_vrot));
    printf("D %.1f Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", distance_travelled,current_height,theta,alpha,dz,rot);

    env->publishVelocity(fabs(cfg.linear) * rot_gain , rot);
	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskHorizontalTransect::terminate()
{
    env->publishVelocity(0,0);
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryHorizontalTransect);
