
#include <math.h>
#include "TaskDriveBetweenWalls.h"
#include "floor_plane_ransac/SetWallParameters.h"
#include "std_srvs/SetBool.h"
#include "task_manager_crawler/TaskDriveBetweenWallsConfig.h"

using namespace task_manager_msgs;
using namespace task_manager_lib;

using namespace task_manager_crawler;

// #define DEBUG_GOTO

TaskIndicator TaskDriveBetweenWalls::initialise() 
{
    last_confidence = ros::Time::now();
    detection.confidence = 0;
    wall_sub = env->getNodeHandle().subscribe("/floor_wall_ransac/wall",1,&TaskDriveBetweenWalls::beamCb,this);

    if (!cfg.detection_service.empty()) {
        ros::ServiceClient client = env->getNodeHandle().serviceClient<std_srvs::SetBool>(cfg.detection_service);
        std_srvs::SetBool activate;
        activate.request.data = true;
        if (client.call(activate)) {
            ROS_INFO("Activate floor_wall_ransac: %d:%s", (int)activate.response.success,activate.response.message.c_str());
        } else {
            ROS_ERROR("TaskDriveBetweenWalls: Failed to call service '%s'",cfg.detection_service.c_str());
            return TaskStatus::TASK_INITIALISATION_FAILED;
        }
    }

    if (!cfg.parameter_service.empty()) {
        ros::ServiceClient client = env->getNodeHandle().serviceClient<floor_plane_ransac::SetWallParameters>(cfg.parameter_service);
        floor_plane_ransac::SetWallParameters srv;
        srv.request.expected_width = cfg.expected_width;
        srv.request.width_tolerance = cfg.width_tolerance;
        if (client.call(srv)) {
            ROS_INFO("Set wall param: %d", (int)srv.response.success);
        } else {
            ROS_ERROR("TaskDriveBetweenWalls: Failed to call service '%s'",cfg.parameter_service.c_str());
            return TaskStatus::TASK_INITIALISATION_FAILED;
        }
    }
    

    return TaskStatus::TASK_INITIALISED;
}

static inline double square(double x) {
    return x*x;
}

TaskIndicator TaskDriveBetweenWalls::iterate()
{
    ros::Time now = ros::Time::now();
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskDriveBetweenWalls: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }

    if ((now - detection.header.stamp).toSec() > 1.) {
        env->publishVelocity(0,0);
        return TaskStatus::TASK_RUNNING;
    }

    if ((detection.confidence < cfg.min_confidence) || (detection.score1 < cfg.min_score) || (detection.score2 < cfg.min_score)) {
        env->publishVelocity(0,0);
        if ((now - last_confidence).toSec() > cfg.wait_confidence) {
            ROS_WARN("TaskDriveBetweenWalls: Aborting because the confidence has been too low for too long");
            return TaskStatus::TASK_FAILED;
        } else {
            return TaskStatus::TASK_RUNNING;
        }
    }



    last_confidence = now;
    double alpha = -(detection.angle_rad-M_PI/2);
    double dy = -(detection.radius2+detection.radius1)/2;
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha + cfg.k_y*dy, cfg.max_vrot);
    double rot_gain = exp(-0.5*square(rot/cfg.exp_vrot));
    printf("D %.3f A %.3f rot %.3f\n", dy,alpha,rot);
    env->publishVelocity(cfg.linear * rot_gain , rot);
	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskDriveBetweenWalls::terminate()
{
    env->publishVelocity(0,0);
    if (!cfg.detection_service.empty()) {
        ros::ServiceClient client = env->getNodeHandle().serviceClient<std_srvs::SetBool>(cfg.detection_service);
        std_srvs::SetBool activate;
        activate.request.data = false;
        if (client.call(activate)) {
            ROS_INFO("Disable floor_wall_ransac: %d:%s", (int)activate.response.success,activate.response.message.c_str());
        } else {
            ROS_ERROR("TaskDriveBetweenWalls: Failed to call service '%s' on terminate.",cfg.detection_service.c_str());
        }
    }
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryDriveBetweenWalls);
