#include <math.h>
#include "tf2/LinearMath/Vector3.h"
#include "tf2/LinearMath/Transform.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "TaskGoToMesh.h"
#include "task_manager_crawler/PlanRequest.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskGoToMesh::initialise()  {

    if (!ros::service::waitForService("/mesh_planner/plan", ros::Duration(0.5))) {
        ROS_ERROR("Service /mesh_planner/plan is not available");
        return TaskStatus::TASK_INITIALISATION_FAILED;
    }
    ros::ServiceClient client = env->getNodeHandle().serviceClient<task_manager_crawler::PlanRequest>("/mesh_planner/plan");
    task_manager_crawler::PlanRequest srv;
    // const geometry_msgs::Point & P = env->getUwb();
    tf2::Transform T=env->lookupTransform();
    srv.request.start.x = T.getOrigin().x();
    srv.request.start.y = T.getOrigin().y();
    srv.request.start.z = T.getOrigin().z();
    ROS_INFO("Initial Pose == [%.2f %.2f %.2f]", srv.request.start.x ,srv.request.start.y, srv.request.start.z);
    const geometry_msgs::Point & P = srv.request.start;
    //const geometry_msgs::Point & P = env->getUwb();
    srv.request.start = P;
    srv.request.end.x = cfg.goal_x;
    srv.request.end.y = cfg.goal_y;
    srv.request.end.z = cfg.goal_z;
    ROS_INFO("Target Pose == [%.2f %.2f %.2f]", srv.request.end.x ,srv.request.end.y, srv.request.end.z);
    
    ROS_INFO("Calling service /mesh_planner/plan");
    if (client.call(srv))
    {
        current_segment = 0;
        path = srv.response.path;
        ROS_INFO("Planned path from (%.2f,%.2f,%.2f) to (%.2f,%.2f,%.2f): %d segments", 
                P.x,P.y,P.z, cfg.goal_x, cfg.goal_y, cfg.goal_z,(int)path.segments.size());
    }
    else
    {
        ROS_ERROR("Failed to call service /mesh_planner/plan");
        return TaskStatus::TASK_INITIALISATION_FAILED;
    }


    return TaskStatus::TASK_INITIALISED;
}
            
static inline double square(double x) {
    return x*x;
}

static inline double hypot(double x,double y,double z) {
    return sqrt(x*x+y*y+z*z);
}

TaskIndicator TaskGoToMesh::iterate()
{
    /*
     * not relevant in simulation, because we depend on TF
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskGoToMesh: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }
    */
    
    tf2::Transform Trob = env->lookupTransform();
    tf2::Vector3 vR;
    vR = Trob.getOrigin();
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    ROS_INFO("ROBOT Pose == [%.2f %.2f %.2f]", vR.x() ,vR.y(), vR.z());
    
    double x=0,y=0; // coordinate in the current segment frame
    double theta=0;

    while (current_segment < path.segments.size()) {
        const geometry_msgs::Point & S = path.segments[current_segment].start;
        const geometry_msgs::Point & E = path.segments[current_segment].end;
        
        
        printf("Segment (%.2f %.2f %.2f) -> (%.2f %.2f %.2f): (%.2f %.2f %.2f)\n",
                S.x,S.y,S.z,E.x,E.y,E.z,vR.x(),vR.y(),vR.z());
        tf2::Vector3 vS, vE;

        tf2::fromMsg(S,vS);
        tf2::fromMsg(E,vE);



        // [ux,uy,uz] is a reference frame aligned with the hull normal (z)
        // with x along the path segment
        tf2::Vector3 ux,uy,uz;
        tf2::fromMsg(path.segments[current_segment].ux,ux);
        tf2::fromMsg(path.segments[current_segment].uy,uy);
        tf2::fromMsg(path.segments[current_segment].uz,uz);
        tf2::Matrix3x3 Rseg(ux.x(),uy.x(),uz.x(),
                ux.y(),uy.y(),uz.y(),
                ux.z(),uy.z(),uz.z());
        tf2::Transform Tseg(Rseg,vS);


        tf2::Vector3 vS_R, vE_R, vR_R;
        vS_R = Tseg.inverse() * vS; 
        vE_R = Tseg.inverse() * vE; 
        vR_R = Tseg.inverse() * vR; 
        printf("Transform (%.2f %.2f %.2f) -> (%.2f %.2f %.2f) : (%.2f %.2f %.2f)\n",
                vS_R.x(),vS_R.y(),vS_R.z(),vE_R.x(),vE_R.y(),vE_R.z(),vR_R.x(),vR_R.y(),vR_R.z());

        tf2::Transform Tdiff = Tseg.inverse() * Trob;
        tf2::Vector3 se(E.x-S.x,E.y-S.y,E.z-S.z);
        tf2::Vector3 P_seg = Tdiff.getOrigin();
        tf2::Matrix3x3 Rdiff(Tdiff.getRotation());
        tf2::Vector3 D_seg = Rdiff * tf2::Vector3(1,0,0);
        ROS_INFO(" Tseg == %.2f %.2f %.2f", Tseg.getOrigin().x(), Tseg.getOrigin().y(), Tseg.getOrigin().z());
	ROS_INFO(" Tdiff == %.2f %.2f %.2f", P_seg.x(), P_seg.y(), P_seg.z());

        
        theta = atan2(D_seg.y(),D_seg.x());
        ROS_INFO("theta (%f)",theta);
        x = P_seg.x();
        y = P_seg.y();
        printf("[I (%d)] [P (%.2f)] (%.2f)] (%.2f)] [T (%.3f)] [X (%.2f)] (%.2f)]\n", current_segment, vR.x(),vR.y(),vR.z(),theta,x,y);
        if (x > se.length()-cfg.threshold) {
            current_segment++;
            ROS_INFO("Switching to segment %d",current_segment);
            continue;
        }
        break;
    }

    if (current_segment >= path.segments.size()) {
        ROS_WARN("Reach final segment with an error larger than threshold");
		return TaskStatus::TASK_COMPLETED;
    }


    double alpha = remainder(theta,2*M_PI);
    ROS_INFO("alpha (%f)",alpha);
    double dy = CrawlerEnv::saturate(y,cfg.max_dy);
    
    double alpha_gain = exp(-0.5*square(alpha/cfg.max_alpha));
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha - cfg.k_y*alpha_gain*y, cfg.max_vrot);
    ROS_INFO("ANGLE == %f, %f", cfg.k_alpha*alpha - cfg.k_y*alpha_gain*y, cfg.max_vrot);
    double rot_gain = 1;
    if (fabs(rot) > cfg.min_rot_gain) {
        rot_gain = exp(-0.5*square((rot - cfg.min_rot_gain)/cfg.exp_vrot));
    }
    printf("              A %.3f dY %.1f rot %.3f\n", alpha,dy,rot);
    //printf("Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", P.z,theta,alpha,dz,rot);
    env->publishVelocity(cfg.linear * rot_gain , rot);

	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskGoToMesh::terminate()
{
    env->publishVelocity(0,0);
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryGoToMesh);
