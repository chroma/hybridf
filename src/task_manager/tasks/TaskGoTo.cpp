#include <math.h>
#include "TaskGoTo.h"
#include "task_manager_crawler/TaskGoToConfig.h"
using namespace task_manager_msgs;
using namespace task_manager_lib;
using namespace task_manager_crawler;


TaskIndicator TaskGoTo::initialise()  {
    initial_pose = env->getUwb();
    ROS_INFO("TaskGoTo: Moving %fm in direction %f",cfg.distance,cfg.bearing);
    return TaskStatus::TASK_INITIALISED;
}
            
static inline double square(double x) {
    return x*x;
}

static inline double hypot(double x,double y,double z) {
    return sqrt(x*x+y*y+z*z);
}

TaskIndicator TaskGoTo::iterate()
{
    if (!env->sensorsAreAlive()) {
        ROS_ERROR("TaskGoTo: Aborting because sensors are dead");
        return TaskStatus::TASK_FAILED;
    }
    const geometry_msgs::Point & P = env->getUwb();
    // TODO: express the goal point in the robot frame (this requires computing the normal from the acceleration)
    double distance = hypot(P.x-initial_pose.x, P.y-initial_pose.y,P.z-initial_pose.z);
    if (distance > cfg.distance) {
		return TaskStatus::TASK_COMPLETED;
    }

    double theta = env->getHeading();
    double alpha = remainder(cfg.bearing - theta,2*M_PI);
    double rot = CrawlerEnv::saturate(cfg.k_alpha*alpha, cfg.max_vrot);
    double rot_gain = exp(-0.5*square(rot/(3*cfg.max_vrot)));
    double linear = CrawlerEnv::saturate(cfg.k_v * std::max(cfg.distance - distance,0.05),cfg.linear);
    // printf("Z %.1f T %.3f A %.3f dZ %.1f rot %.3f\n", P.z,theta,alpha,dz,rot);
    env->publishVelocity(linear * rot_gain , rot);

	return TaskStatus::TASK_RUNNING;
}

TaskIndicator TaskGoTo::terminate()
{
    env->publishVelocity(0,0);
	return TaskStatus::TASK_TERMINATED;
}

DYNAMIC_TASK(TaskFactoryGoTo);
