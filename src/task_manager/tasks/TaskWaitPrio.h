#ifndef TASK_WaitPrio_H
#define TASK_WaitPrio_H

#include "task_manager_lib/TaskDefinition.h"
#include "task_manager_crawler/CrawlerEnv.h"
#include "task_manager_crawler/TaskWaitPrioConfig.h"
// #include "task_manager_crawler/Path.h"
#include "task_manager_crawler/GetPrio.h"

using namespace task_manager_lib;

namespace task_manager_crawler {
    class TaskWaitPrio : public TaskInstance<TaskWaitPrioConfig, CrawlerEnv>
    {
        protected:
        ros::ServiceClient client = env->getNodeHandle().serviceClient<task_manager_crawler::GetPrio>("/get_prio");
        task_manager_crawler::GetPrio srv;
            // task_manager_crawler::Path path;
            // unsigned int current_segment;

        public:
            TaskWaitPrio(TaskDefinitionPtr def, TaskEnvironmentPtr env) : Parent(def,env) {}
            virtual ~TaskWaitPrio() {};

            virtual TaskIndicator initialise() ;

            virtual TaskIndicator iterate();

            virtual TaskIndicator terminate();
    };
    class TaskFactoryWaitPrio : public TaskDefinition<TaskWaitPrioConfig, CrawlerEnv, TaskWaitPrio>
    {

        public:
            TaskFactoryWaitPrio(TaskEnvironmentPtr env) : 
                Parent("WaitPrio","Wait for priority from multirobot planning ",true,env) {}
            virtual ~TaskFactoryWaitPrio() {};
    };
};

#endif // TASK_WaitPrio_H
