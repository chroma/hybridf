#ifndef CRAWLER_ENV_H
#define CRAWLER_ENV_H

#include <memory>
#include <mutex>
#include <map>
#include <ros/ros.h>
#include "task_manager_lib/ServiceStorage.h"
#include "task_manager_sync/TaskEnvironmentSync.h"
#include "std_srvs/Empty.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "sensor_msgs/Imu.h"
#include <tf2/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>
#include "std_msgs/Float32.h"

namespace task_manager_crawler {
    class CrawlerEnv: public task_manager_sync::TaskEnvironmentSync, task_manager_lib::ServiceStorage
    {
        protected:
            unsigned int crawlerId;
            ros::Subscriber uwbSub;
            ros::Subscriber imuSub;
            ros::Subscriber object_dist_sub;
            ros::Publisher velPub;
            std::string baseFrame_;
            std::string refFrame_;
            float object_depth;
            bool  object_sensed = false;
            bool simMode_;
            tf2_ros::Buffer tfBuffer_;
            tf2_ros::TransformListener tfListener_;
            ros::Time lastDistReceived;

            void uwbCallback(const geometry_msgs::PointStamped::ConstPtr& msg) {
                lastUwbReceived = ros::Time::now();
                uwbPose = *msg;
            }
            ros::Time lastUwbReceived;
            geometry_msgs::PointStamped uwbPose;

            void imuCallback(const sensor_msgs::Imu::ConstPtr& msg) {
                lastImuReceived = ros::Time::now();
                imu = *msg;
            }
            ros::Time lastImuReceived;
            sensor_msgs::Imu imu;

            void ObjectDistCallback(const std_msgs::Float32::ConstPtr& msg);


        public:
            CrawlerEnv(ros::NodeHandle & nh, const std::string & name, unsigned int id=1);
            ~CrawlerEnv() {};

            bool sensorsAreAlive() const;
            

            static double saturate(double x, double sat) {
                if (x > sat) return sat;
                if (x < -sat) return -sat;
                return x;
            }

            const geometry_msgs::PointStamped & getUwbStamped() const {
                return uwbPose;
            }

            const geometry_msgs::Point & getUwb() const {
                return uwbPose.point;
            }

            double getObjDist() {
                return object_depth;
            }

            bool objDetected() {
                return object_sensed;
            }

            double getHeading() const ;

            geometry_msgs::Pose2D getPose2D() const {
                geometry_msgs::Pose2D pose; 
                // TODO...
                pose.x = uwbPose.point.y;
                pose.y = uwbPose.point.z;
                pose.theta = getHeading();
                return pose;
            }

            const sensor_msgs::Imu & getImu() const {
                return imu;
            } 

            void publishVelocity(double linear, double angular) {
                geometry_msgs::Twist cmd;
                cmd.linear.x = linear;
                cmd.angular.z = angular;
                velPub.publish(cmd);
            }

            tf2_ros::Buffer & getTFBuffer() {
                return tfBuffer_;
            }
            tf2_ros::TransformListener & getListener() {
                return tfListener_;
            }


            tf2::Transform lookupTransform(const ros::Time & t = ros::Time(0));
    };

    typedef boost::shared_ptr<CrawlerEnv> CrawlerEnvPtr;
    typedef boost::shared_ptr<CrawlerEnv const> CrawlerEnvConstPtr;
};

#endif // CRAWLER_ENV_H
