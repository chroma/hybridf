#include <fstream>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
//#include "gnuplot-iostream.h"
#include <sys/stat.h>
#include <tf/transform_datatypes.h>

class MyNode {
public:
  MyNode(ros::NodeHandle &nh)
      : nh_(nh), path_file_("path_mission.csv", std::ios_base::out) {
    pub_ = nh.advertise<nav_msgs::Path>("/pub_path_mission", 10);
    sub_odom_ = nh_.subscribe("/crawler/gt/pose", 1, &MyNode::odomCallback, this);
    path_file_ << "x,y,z,vz,vy,vz,yaw\n"; // write header to file
  }

  ~MyNode() {
    if (path_file_.is_open()) {
      path_file_.close();
    }
  }

private:

  ros::NodeHandle nh_;
  ros::Subscriber sub_odom_;
  ros::Publisher pub_;
  std::ofstream path_file_;
  nav_msgs::Path path_msg;

  void odomCallback(const nav_msgs::Odometry::ConstPtr &msg) {
    
    path_msg.header = msg->header;
    geometry_msgs::PoseStamped pose;
    pose.header = msg->header;
    pose.pose = msg->pose.pose;
    path_msg.poses.push_back(pose);

    tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    std::cout << "Yaw: " << yaw*180/3.14 << std::endl;


    // Publish the path message
    pub_.publish(path_msg);
    // std::cout<< "x= " << pose.pose.position.x << "," << "y = "<< pose.pose.position.y << ","
    //          << "z = " << pose.pose.position.z << "," << "vx= "<< msg->twist.twist.linear.x << "," 
    //          << "vy = "<< msg->twist.twist.linear.y << "," << "vz= "<< msg->twist.twist.linear.z << "," 
    //          << "yaw = "<< yaw << "\n"; 
    // write pose to file
    if (path_file_.is_open()) {
      path_file_ << pose.pose.position.x << "," << pose.pose.position.y <<      ","
                 << pose.pose.position.z << "," << msg->twist.twist.linear.x << ","  
                 << msg->twist.twist.linear.y << "," << msg->twist.twist.linear.z << "," 
                 << yaw*180/3.14 <<"\n";
    }
  }
};

// bool fileExists(const std::string& filename)
// {
//     struct stat buf;
//     if (stat(filename.c_str(), &buf) != -1)
//     {
//         return true;
//     }
//     return false;
// }

int main(int argc, char **argv) {
  ros::init(argc, argv, "path_publisher_mission");
  ros::NodeHandle nh;
  MyNode node(nh);
  ros::Rate loop_rate(1); // set the subscription frequency to 1 Hz (once every 1 seconds)

  while (ros::ok())
  {
    ros::spinOnce(); // receive messages
    loop_rate.sleep(); // wait until it's time to receive the next message
  }
  return 0;
}
