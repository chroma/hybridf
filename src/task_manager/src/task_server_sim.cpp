#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include "task_manager_sync/TaskServerSync.h"

#include "task_manager_crawler/CrawlerSimEnv.h"


using namespace task_manager_crawler;
using namespace task_manager_lib;

class TaskServer : public task_manager_sync::TaskServerSync {
    public:
        TaskServer(TaskEnvironmentPtr _env) : task_manager_sync::TaskServerSync(_env) {
            start();
        }

};

int main(int argc, char *argv[])
{
    ros::init(argc,argv,"crawler_tasks");//init ros
    
    std::cout<<"	init crawler task node ros "<<std::endl;
    std::cout<<"	init crawler task node ros "<<std::endl;
    std::cout<<"	init crawler task node ros "<<std::endl;        
    
    ros::NodeHandle nh("~");
    std::string partner_name = "partner";
    int id = 1;
    nh.getParam("my_name",partner_name);
    nh.getParam("my_id",id);

    CrawlerEnvPtr env(new CrawlerEnv(nh,partner_name,id));
    
    std::cout<<"	Define the env "<<std::endl;
    std::cout<<"	Define the env "<<std::endl;
    std::cout<<"	Define the env "<<std::endl;
    
    env->addSyncSource("partner1");
    env->addSyncSource("partner2");
    
    std::cout<<"	env->addSyncSource(\"partner1\"); "<<std::endl;
    std::cout<<"	env->addSyncSource(\"partner1\"); "<<std::endl;
    std::cout<<"	env->addSyncSource(\"partner1\"); "<<std::endl;
    
    TaskServer ts(env);
    
    std::cout<<"	TaskServer ts(env); "<<std::endl;
    std::cout<<"	TaskServer ts(env);"<<std::endl;
    std::cout<<"	TaskServer ts(env); "<<std::endl;
    ros::spin();

    return 0;
}
