
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include "task_manager_crawler/CrawlerSimEnv.h"
#include "std_srvs/SetBool.h"
// #include "floor_plane_ransac/SetWallParameters.h"

using namespace task_manager_crawler;
CrawlerEnv::CrawlerEnv(ros::NodeHandle & n, 
        const std::string & name, unsigned int id) : task_manager_sync::TaskEnvironmentSync(n,name,"sync"), 
    task_manager_lib::ServiceStorage(n), crawlerId(id), tfListener_(tfBuffer_)
{
    char buffer[128]; sprintf(buffer,"/crawler_%d",id);
    std::string cname(buffer);

    nh.param("base_frame",baseFrame_,std::string("base_link"));
    nh.param("ref_frame",refFrame_,std::string("world"));
    nh.param("sim_mode",simMode_,false);

    // registerServiceClient<std_srvs::SetBool>("/floor_wall_ransac/start_stop");
    // registerServiceClient<floor_plane_ransac::SetWallParameters>("/floor_wall_ransac/set_param");

    object_dist_sub = nh.subscribe("/lidar_distance/distance",10,&CrawlerEnv::ObjectDistCallback,this);
    uwbSub = nh.subscribe(cname+"/groundtruth/odom",1,&CrawlerEnv::uwbCallback,this);
    imuSub = nh.subscribe(cname+"/imu",1,&CrawlerEnv::imuCallback,this);
    velPub = nh.advertise<geometry_msgs::Twist>(cname+"/cmd_vel",1);
    std::cout<<"Crawler ENv Simulation task"<<std::endl;
}



bool CrawlerEnv::sensorsAreAlive() const {
    ros::Time now = ros::Time::now();
    /*
     * not relevant for gazebo sim
    if ((now - lastUwbReceived).toSec() > 0.5) {
        ROS_ERROR("Last UWB message is more than 0.5s old, check gazebo");
        return false;
    }
    */
    if ((now - lastImuReceived).toSec() > 2.5) {
        ROS_ERROR("Last IMU message is more than 0.5s old, check gazebo");
        return false;
    }
    return true;
}


tf2::Transform CrawlerEnv::lookupTransform(const ros::Time & t) {
    geometry_msgs::TransformStamped transformStamped;
    try{
      transformStamped = tfBuffer_.lookupTransform(refFrame_, baseFrame_, t);
    } catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
    }
    tf2::Transform res;
    tf2::fromMsg(transformStamped.transform,res);
    return res;
}

double CrawlerEnv::getHeading() const {
    if(simMode_){
        return atan2(imu.linear_acceleration.x,imu.linear_acceleration.y);
    }
    return atan2(imu.linear_acceleration.y,imu.linear_acceleration.z);
}

void CrawlerEnv::ObjectDistCallback(const std_msgs::Float32::ConstPtr& msg) {
    std_msgs::Float32 dist = (*msg);
    lastDistReceived = ros::Time::now();
    object_depth = dist.data;
    object_sensed = true;
}

