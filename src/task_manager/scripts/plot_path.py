import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.patches as patches
import mpl_toolkits.mplot3d.art3d as art3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# def data_for_cylinder_along_z(center_x,center_y,radius,height_z):
#     z = np.linspace(0, height_z, 50)
#     theta = np.linspace(0, 2*np.pi, 50)
#     theta_grid, z_grid=np.meshgrid(theta, z)
#     x_grid = radius*np.cos(theta_grid) + center_x
#     y_grid = radius*np.sin(theta_grid) + center_y
#     return x_grid,y_grid,z_grid

# def plot_reference_mission_path():
#     ypoints = np.array([3, 8, 1, 10])
#     plt.plot(ypoints)
#     plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# mission="full_tank_mission"
# mission="rectangle"


mission="rectangle"
x_goal = 9.19
y_goal = 5.4
z_goal = 0.1
W = 8.0
H = 2.0
step_mission=2.0
iterations=int(W//step_mission)
print("iterations",iterations)
rest=W%step_mission
if(rest!=0):
    step_mission=step_mission+(rest/iterations)
print("steps",step_mission)

# plot_reference_mission_path()
if(mission=="full_tank_mission"):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    Xc,Yc,Zc = data_for_cylinder_along_z(26.0,26.0,25.5,20)
    ax.plot_surface(Xc, Yc, Zc, alpha=0.1)
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    df = pd.read_csv('crawler_missions_path_tests/path.csv')
    # df = pd.read_csv('path.csv')
    step=10
    starting_point=0
    visualization_distance=3
    x=df['x'].to_numpy()
    y=df['y'].to_numpy()
    z=df['z'].to_numpy()
    ax.scatter(x[starting_point],y[starting_point],z[starting_point],label="Starting position") 
    ax.plot([x[starting_point],x[starting_point]+step],[y[starting_point]-visualization_distance,y[starting_point]-visualization_distance-2],[1,1],ls='-.',lw=0.9,color="green",label="Step = 10")
    ax.plot([x[starting_point],x[starting_point]],[y[starting_point]-visualization_distance,y[starting_point]-visualization_distance],[z[starting_point]-visualization_distance,z[starting_point]-visualization_distance+18],ls='-.',lw=0.9,color="blue",label="Height = 18")
    ax.plot(x,y,z,ls='-',lw=0.5,color="red",label="Path")
    leg = ax.legend(loc='upper right')
    plt.show()

elif (mission=="rectangle"):
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    '''
    Xc,Yc,Zc = data_for_cylinder_along_z(26.0,26.0,25.5,20)
    ax.plot_surface(Xc, Yc, Zc, alpha=0.1)
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    '''
    # df = pd.read_csv('crawler_missions_path_tests/path.csv')
    df = pd.read_csv('~/bugwright2-ws/bugwright_ws/src/task_manager_crawler/scripts/crawler_missions_path_tests/3steps_rectangle_mission_path_with_noise.csv')
    step=2
    starting_point=0
    visualization_distance=3
    x=df['x'].to_numpy()
    y=df['y'].to_numpy()
    z=df['z'].to_numpy()
    # ax.scatter(x[starting_point],y[starting_point],z[starting_point],label="Starting position")
    height = 2
    width = 8
    x0 = x_goal
    y0 = y_goal
    z0 = z_goal
    x1 = x[-1]
    y1 = y[-1]
    z1 = z[-1]
    # rect_verts = np.array([[(x0, y0, z0), 
    #                         (x0, y0 , z0+ height),
    #                         (x1, y1 , z1+ height),
    #                         (x1, y1, z1)]])
    # rect = art3d.Poly3DCollection(rect_verts, alpha=0.25, facecolor='yellow',)
    # ax.add_collection3d(rect)


    for i in range(iterations):
        if (i==iterations-1) :
            ax.plot([x_goal, x_goal],[y_goal, y_goal],[z_goal,z_goal+H])
        else:
            ax.plot([x_goal, x_goal],[y_goal, y_goal],[z_goal,z_goal+H])
            ax.plot([x_goal, x_goal+step_mission],[y_goal, y_goal],[z_goal,z_goal])
            x_goal=x_goal+step_mission


    # ax.plot(x[0:365],y[0:365],z[0:365],ls='-',lw=0.5,color="red",label="Path")
    # ax.plot(x,y,z,ls='-',lw=0.5,color="red",label="Path")

    leg = ax.legend(loc='upper right')
    plt.show()


    z=df['z'].to_numpy()

elif (mission=="rectangle_ship"):
    df = pd.read_csv('path.csv')

    step=10
    starting_point=0
    visualization_distance=3
    
    x=df['x'].to_numpy()
    y=df['y'].to_numpy()
    z=df['z'].to_numpy()

    ax.scatter(x[starting_point],y[starting_point],z[starting_point],label="Starting position")
    height = 2
    width = 8
    x0 = x[starting_point]
    y0 = y[starting_point]
    z0 = z[starting_point]
    x1 = x[-1]
    y1 = y[-1]
    z1 = z[-1]
    # rect_verts = np.array([[(x0, y0, z0), 
    #                         (x0, y0 , z0+ height),
    #                         (x1, y1 , z1+ height),
    #                         (x1, y1, z1)]])
    # rect = art3d.Poly3DCollection(rect_verts, alpha=0.25, facecolor='yellow',)
    # ax.add_collection3d(rect)

    ax.plot(x,y,z,ls='-',lw=0.5,color="red",label="Path")
    leg = ax.legend(loc='upper right')
    plt.show()

