#!/usr/bin/env python3
import rospy
from task_manager_crawler.srv import StartMissionWP,StartMissionWPResponse,UpdatePrio,GetPrio
from task_manager_crawler.msg import Waypoint

import roslib; roslib.load_manifest('task_manager_crawler')
from nav_msgs.msg import Odometry
from math import *
from task_manager_lib.TaskClient import *
from geometry_msgs.msg import Point as Msg_Point
from std_msgs.msg import Bool as Msg_Bool
import tf2_ros


class MissionServer:
    def __init__(self):
        rospy.init_node('mission_server')
        rospy.Service('start_mission_wp', StartMissionWP, self.handle_start_mission)
        rospy.loginfo("Mission Server service ready.")
        rospy.Subscriber("update_prio", Msg_Bool, self.callback_prio)
        self.prio = None
        self.robot_id = rospy.get_param('~topic_prefix')
        self.wps = None
        self.pub_target = rospy.Publisher("/"+self.robot_id+"/new_target", Msg_Point, queue_size=10)
        
        self.pub_priority = rospy.Publisher("/"+self.robot_id+"/priority", Msg_Bool, queue_size=10)
         
        server_node = rospy.get_param("~server","/"+self.robot_id+"/crawler_tasks1")
        default_period = rospy.get_param("~period",0.2)
        self.tc = TaskClient(server_node,default_period)
        self.i = 0
         

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
    
    def callback_prio(self, data):
        self.prio = data.data
        
    def handle_start_mission(self, req):
        robot_id = req.robot_id
        waypoints = req.waypoints
        rospy.loginfo("Starting mission for robot %s with waypoints:", self.robot_id)
        for i in range(len(waypoints)):
            rospy.loginfo("Waypoint %d : x:%.2f y:%.2f z:%.2f",i,waypoints[i].x,waypoints[i].y,waypoints[i].z)
        success = self.execute_mission(waypoints)
        return(success) 

    def execute_mission(self, waypoints):
        self.wps = waypoints
        self.i = 0
        return True
    

    def step(self):
        
        
        initial_pos=None
        
        if self.wps is not None:
            
            if self.i>0:
                #Go to an intermediate wp
               semi_wp = [(self.wps[self.i].x + self.wps[self.i-1].x)/2, (self.wps[self.i].y + self.wps[self.i-1].y)/2, (self.wps[self.i].z + self.wps[self.i-1].z)/2]
               rospy.loginfo("Waypoint == : %f" %self.wps[self.i].x )
               #self.tc.GoToMesh(linear=0.7,goal_x=semi_wp[0],goal_y=semi_wp[1],goal_z=semi_wp[2],threshold=0.1,max_vrot=5.0,min_rot_gain=0.5, exp_vrot=0.15,max_alpha=0.25,max_dy=0.5,k_y=2.5,k_alpha=-2.5)
               #self.tc.PointTowards(goal_x=semi_wp[0],goal_y=semi_wp[1],goal_z=semi_wp[2], k_alpha=0.4, max_vrot=0.7,relative=0,threshold=0.6)

               self.tc.GoToMesh(linear=0.08,goal_x=semi_wp[0],goal_y=semi_wp[1],goal_z=semi_wp[2],threshold=0.2,max_vrot=0.5, max_dy=0.5,k_y=-1,k_alpha=0.2)


            #Wait until we have priority to move on to the next wp
            print("Reel ",[self.robot_id, self.wps[self.i].x, self.wps[self.i].y, self.wps[self.i].z])
            trans_1 = self.tfBuffer.lookup_transform('totalstation', 'TS0001', rospy.Time())
            self.tf_pose = [self.wps[self.i].x-trans_1.transform.translation.x, 
                            self.wps[self.i].y-trans_1.transform.translation.y, 
                            self.wps[self.i].z-trans_1.transform.translation.z]
            
            self.tc.WaitPrio(x=self.tf_pose[0], y=self.tf_pose[2], robot_id=int(self.robot_id.split('_')[1]))

            # Go to next wp
            #self.tc.GoToMesh(linear=0.7,goal_x=self.wps[self.i].x,goal_y=self.wps[self.i].y,goal_z=self.wps[self.i].z,threshold=0.1,max_vrot=5.0,min_rot_gain=0.5, exp_vrot=0.15,max_alpha=0.25,max_dy=0.5,k_y=2.5,k_alpha=-2.5)
            
            #self.tc.PointTowards(goal_x=self.wps[self.i].x,goal_y=self.wps[self.i].y,goal_z=self.wps[self.i].z,k_alpha=0.4, max_vrot=0.7,relative=0,threshold=0.6)
            msg = Msg_Point()
            msg.x = self.wps[self.i].x 
            msg.y = -self.wps[self.i].y
            msg.z = 0
            self.pub_target.publish(msg)
            self.tc.GoToMesh(linear=0.08,goal_x=self.wps[self.i].x,goal_y=self.wps[self.i].y,goal_z=self.wps[self.i].z,threshold=0.2, max_dy=0.5, max_vrot=0.5, k_y=-1,k_alpha=0.2)

            print("update prio")
            self.pub_priority.publish(self.prio)

            


            # Update the priority of the reached wp
            rospy.wait_for_service('/update_prio')
            valid_update = False
            while not valid_update :
                try:
                    update_prio = rospy.ServiceProxy('/update_prio', UpdatePrio)
                    self.tf_pose = [self.wps[self.i].x-trans_1.transform.translation.x, 
                                    self.wps[self.i].y-trans_1.transform.translation.y, 
                                    self.wps[self.i].z-trans_1.transform.translation.z]
                    valid_update = update_prio(self.tf_pose[0], self.tf_pose[2], int(self.robot_id.split('_')[1])).valid_update
                except rospy.ServiceException as e:
                    rospy.loginfo("Service call failed: %s"%e)
        
            rospy.loginfo(self.robot_id+"reached wp (%d,%d)",self.wps[self.i].x, self.wps[self.i].y)
            self.i+=1
            if self.i >=len(self.wps):
                self.wps = None

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            
            rate.sleep()
            self.step()
        
        
        
if __name__ == "__main__":
    mission_server = MissionServer()
    #rospy.spin()
    mission_server.run()
    

