#!/usr/bin/env python3
import rospy
import time
from task_manager_crawler.srv import StartMissionWP,UpdatePrioRequest,UpdatePrio,GetPrio,GetPrioRequest,TriggerPrio,TriggerPrioRequest
from task_manager_crawler.msg import Waypoint
from task_manager_crawler.srv import GetPrio, GetPrioResponse

def handle_get_prio(req):
    # rospy.loginfo("Received request for wp (x={}, y={})".format(req.x, req.y))
    priorities = [0, 0, 2]
    rospy.loginfo(f"Priorities: {priorities}")
    return GetPrioResponse(prio=priorities)

# def handle_trigger_prio(req):
#     rospy.wait_for_service('Trigger_prio')
#     try:
#         get_prio = rospy.ServiceProxy('get_prio', GetPrio)
#         req = GetPrioRequest()
#         req.waypoint = waypoint
#         resp = get_prio(req)
#         return resp.priority_order_list
#     except rospy.ServiceException as e:
#         rospy.logerr("Service call failed: %s", e)



def start_mission_client_wp(robot_id, waypoints):
    rospy.init_node('start_mission_wp_client')
    rospy.wait_for_service('start_mission_wp')
    try:
        start_mission_wp = rospy.ServiceProxy('start_mission_wp', StartMissionWP)
        response = start_mission_wp(robot_id, waypoints)
        if response.success:
            rospy.loginfo("Mission started successfully for %s", robot_id)
        else:
            rospy.logerr("Failed to start mission for robot %s", robot_id)
    except rospy.ServiceException as e:
        rospy.logerr("Service call failed: %s", e)

if __name__ == "__main__":
    # Example usage
    waypoints=[]
    for msg in range(2):
        msg= Waypoint()
        msg.x=16.0
        msg.x=2.0
        msg.x=10.0
        waypoints.append(msg)
    
    rospy.init_node('get_prio_server')
    rospy.Service('/wait_prio', GetPrio, handle_get_prio)
    rospy.loginfo("Get prio server is ready")
    rospy.spin()
    # waypoints = [[1.0, 2.0, 0.0], [3.0, 4.0, 0.0], [5.0, 6.0, 0.0]]
    # waypoints=[2.0,5.2,9.5]
    start_mission_client_wp(robot_id, waypoints)
