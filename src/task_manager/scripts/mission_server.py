#!/usr/bin/env python3
import rospy
from task_manager_crawler.srv import StartMission,StartMissionResponse
# ROS specific imports
import roslib; roslib.load_manifest('task_manager_crawler')
from nav_msgs.msg import Odometry
from math import *
from task_manager_lib.TaskClient import *

# TODO : Nantes sim
initial_pose = None

def handle_start_mission(req):
    # Extract the x, y, and z values from the service request
    x = req.x
    y = req.y
    z = req.z   
    W = req.W
    H = req.H
    print(req)
    # Adapt steps to the width W
    step=2.0
    iterations=int(W//step)
    print("iterations",iterations)
    rest=W%step
    if(rest!=0):
        step=step+(rest/iterations)
    print("steps",step)

    # Check the value of the variable 'world' to determine the appropriate tasks parameters for the crawler to execute.    
    world = "delivrable"
    if world == 'banzacourt_f':
        success= full_inspection_tank_mission(x, y, z,W,H,step,iterations)
    elif world == 'banzacourt':
        success= rectangle_mission_banzacourt(x,y,z,W,H,step,iterations)
    elif world== 'nantes-moebius':
        success= rectangle_mission_nantes_moebius(x, y, z,W,H,step,iterations)
    elif world== 'delivrable':
        success= delivrable_mission(x, y, z,W,H,step,iterations)
    else:
        rospy.logerr('Error: Invalid world specified')
        success=False

    # Return the success flag as the service response
    return StartMissionResponse(success)

# def triangle_mission():
    linear_velocity=0.5
    Vertex_A_x=0
    Vertex_A_y=0
    Vertex_B_x=2
    Vertex_B_y=0
    Vertex_C_x=1
    Vertex_C_y=2
    print("triangle mission ")
    #go to starting point
    tc.GoToMesh(goal_x=Vertex_A_x, goal_y=Vertex_A_y, goal_z=0, linear=linear_velocity)
    
    return True

# save the initial pose 
def odom_callback(msg):
    global initial_pose
    if initial_pose is None:
        initial_pose = msg.pose.pose
def rectangle_mission_banzacourt(x,y,z,W,H,step,iterations):
    tc.SetStatusSync(status=0);
    tc.GoToMesh(linear=0.7,goal_x=x,goal_y=y,goal_z=z,threshold=0.05,max_vrot=5.0,min_rot_gain=0.5, exp_vrot=0.15,max_alpha=0.25,max_dy=0.5,k_y=2.5,k_alpha=-2.5)
    linear_velocity=0.7
    Height=H
    max_vrot_reach_angle=3.0
    max_vrot_vertical_transect=5.0
    max_vrot_horizontal_transect=3.5
    k_alpha_H=5.0
    threshold_=0.05
    # subscribe to the /odom topic to get the initial pose
    rospy.Subscriber('/crawler/gt/pose', Odometry, odom_callback)
    # wait until the initial pose is received
    while initial_pose is None:
        rospy.sleep(0.1)
    initial_x=initial_pose.position.x
    initial_y=initial_pose.position.y
    initial_z=initial_pose.position.z
    rospy.loginfo(f"current_x = {initial_pose.position.x}, current_y = {initial_pose.position.y}, current_z = {initial_pose.position.z}")    
    # TODO: Go to (x,y,z)
    # TODO: Full tank inspection/optimization
    # TODO: add other world
    n=iterations
    for i in range(n):
        #go to starting point
        # tc.GoToMesh(goal_x=x, goal_y=y, goal_z=z, linear=linear_velocity)
        # tc.GoToMesh(goal_x=19,goal_y=5,goal_z=10,k_alpha=2,max_vrot=4.0,linear=0.5)
        print("state = ",i)
        if(i==n-1):
            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=threshold_)

            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+Height,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)

            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=threshold_)

            # go down
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)
        else :
            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=threshold_)

            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+Height,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)

            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=threshold_)

            # go down
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)

            # pointing right
            tc.ReachAngle(target=0,k_alpha=5.0,max_vrot=3.0,threshold=threshold_)

            # # go right 
            tc.HorizontalTransect(linear=linear_velocity,altitude=initial_z,distance=step,max_vrot=max_vrot_horizontal_transect,k_alpha=k_alpha_H)
    tc.SetStatusSync(status=1);
    print("mission end")

    return True
def rectangle_mission_nantes_moebius(x,y,z,W,H,step,iterations):
    
    tc.GoToMesh(linear=1.0,goal_x=x,goal_y=y,goal_z=z,threshold=0.1,max_vrot=5.0,min_rot_gain=5.0, exp_vrot=5.0,max_alpha=5.0,max_dy=5.0,k_y=5.0,k_alpha=-5.0)
    linear_velocity=0.8
    k_alpha_ship_vertical=5.0
    max_vrot_reach_angle=4.0
    max_vrot_vertical_transect=5.0
    threshold_v=0.15
    # subscribe to the /odom topic to get the initial pose
    rospy.Subscriber('/crawler/gt/pose', Odometry, odom_callback)

    # wait until the initial pose is received
    while initial_pose is None:
        rospy.sleep(0.1)

    initial_x=initial_pose.position.x
    initial_y=initial_pose.position.y
    initial_z=initial_pose.position.z
    rospy.loginfo(f"current_x = {initial_pose.position.x}, current_y = {initial_pose.position.y}, current_z = {initial_pose.position.z}")    

    # TODO: Go to (x,y,z)
    # TODO: Full tank inspection/optimization
    # TODO: add other world


# gen.add("linear",     double_t, 0,    "Linear velocity",  0.)
# gen.add("altitude",     double_t, 0,    "Target height over water (or below for negative values)",  0.)
# gen.add("threshold",     double_t, 0,    "Threshold target height for task completion",  0.05)
# gen.add("max_vrot",      double_t, 0,    "Maximum angular velocity",  1.0)
# gen.add("k_alpha",       double_t, 0,    "Gain for angular control",  -0.5)
# gen.add("k_z",           double_t, 0,    "Gain for vertical speed",  1.0)
# gen.add("relative",      bool_t,   0,    "Is the target altitude relative or absolute",  False)
# gen.add("force_positive",bool_t,   0,    "When true, keep the Altiscan pointing up",  True)

    for i in range(iterations):
        #go to starting point
        # tc.GoToMesh(linear=0.7,goal_x=10,goal_y=2,goal_z=5,threshold=0.05,max_vrot=5.0,min_rot_gain=0.5, exp_vrot=0.15,max_alpha=0.25,max_dy=0.5,k_y=2.5,k_alpha=-2.5)
        # tc.GoToMesh(goal_x=19,goal_y=5,goal_z=10,k_alpha=2,max_vrot=4.0,linear=0.5)
        if(i==iterations-1):
            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

            # pointing up
            # tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.01)

            # go down
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

        else :
            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

            # pointing up
            # tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.01)

            # go down
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

            # pointing up
            tc.ReachAngle(target=0,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.25)

            # go right 
            tc.HorizontalTransect(linear=linear_velocity,altitude=initial_z,distance=step,max_vrot=5.0,k_alpha=5.0)
        
    return True

def full_inspection_tank_mission(x,y,z,W,H,step,iterations):
    linear_velocity=0.7
    max_tank_altitude=19
    min_tank_altitude=1
    step=10
    max_vrot_reach_angle=3.0
    max_vrot_vertical_transect=5.0

    # TODO: Go to (x,y,z)
    # TODO: Full tank inspection
    # TODO: add other world

    for i in range(16):
        #go to starting point
        # tc.GoToMesh(goal_x=x, goal_y=y, goal_z=z, linear=linear_velocity)
        # tc.GoToMesh(goal_x=19,goal_y=5,goal_z=10,k_alpha=2,max_vrot=4.0,linear=0.5)
        print("state = ",i)
        # pointing up
        tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.005)

        # go up
        tc.VerticalTransect(linear=linear_velocity,altitude=max_tank_altitude,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)

        # pointing up
        tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.005)

        # go down
        tc.VerticalTransect(linear=linear_velocity,altitude=min_tank_altitude,k_alpha=2.0,max_vrot=max_vrot_vertical_transect)

        # pointing right
        tc.ReachAngle(target=0,k_alpha=5.0,max_vrot=3.0,threshold=0.005)

        # # go right 
        tc.HorizontalTransect(linear=linear_velocity,altitude=min_tank_altitude,distance=step,max_vrot=5.0,k_alpha=5.0)

    return True



def delivrable_mission(x,y,z,W,H,step,iterations):

    tc.GoToMeshWithConstraint(linear=0.3,goal_x=x,goal_y=y,goal_z=z,threshold=0.05,max_vrot=4.0,min_rot_gain=5.0, exp_vrot=5.0,max_alpha=5.0,max_dy=3.0,k_y=1.0,k_alpha=-5.0)
    linear_velocity=0.8
    k_alpha_ship_vertical=5.0
    max_vrot_reach_angle=4.0
    max_vrot_vertical_transect=5.0
    threshold_v=0.15
    # subscribe to the /odom topic to get the initial pose
    rospy.Subscriber('/crawler/gt/pose', Odometry, odom_callback)

    # wait until the initial pose is received
    while initial_pose is None:
        rospy.sleep(0.1)

    initial_x=initial_pose.position.x
    initial_y=initial_pose.position.y
    initial_z=initial_pose.position.z
    rospy.loginfo(f"current_x = {initial_pose.position.x}, current_y = {initial_pose.position.y}, current_z = {initial_pose.position.z}")    


    for i in range(1):

            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=max_vrot_reach_angle,threshold=0.1)

            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H+0.3,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

            # pointing right
            tc.ReachAngle(target=0,k_alpha=4.0,max_vrot=2.0,threshold=0.25)

            # # go right 
            tc.HorizontalTransect(linear=linear_velocity,altitude=initial_z+H-0.3,distance=step,max_vrot=5.0,k_alpha=5.0)

            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=4.0,max_vrot=3,threshold=0.1)

            # go down
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

           # pointing right
            tc.ReachAngle(target=0,k_alpha=4.0,max_vrot=2.0,threshold=0.1)

            # # go right 
            tc.HorizontalTransect(linear=linear_velocity,altitude=initial_z+H,distance=step+0.6,max_vrot=5.0,k_alpha=5.0)

            # pointing up
            tc.ReachAngle(target=1.57,k_alpha=4.0,max_vrot=3,threshold=0.1)

            # go up
            tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect,threshold=threshold_v)

    return True


# def rectangle_mission_moebius(x,y,z,W,H,step,iterations):
#     tc.GoToMesh(linear=1.0,goal_x=x,goal_y=y,goal_z=z,threshold=0.05,max_vrot=5.0,min_rot_gain=5.0, exp_vrot=5.0,max_alpha=5.0,max_dy=5.0,k_y=5.0,k_alpha=-5.0)
#     linear_velocity=0.5
#     k_alpha_ship_vertical=5.0
#     max_vrot_reach_angle=4.0
#     max_vrot_vertical_transect=5.0

#     # subscribe to the /odom topic to get the initial pose
#     rospy.Subscriber('/crawler/gt/pose', Odometry, odom_callback)

#     # wait until the initial pose is received
#     while initial_pose is None:
#         rospy.sleep(0.1)

#     initial_x=initial_pose.position.x
#     initial_y=initial_pose.position.y
#     initial_z=initial_pose.position.z
#     rospy.loginfo(f"current_x = {initial_pose.position.x}, current_y = {initial_pose.position.y}, current_z = {initial_pose.position.z}")    

#     # TODO: Go to (x,y,z)
#     # TODO: Full tank inspection/optimization
#     # TODO: add other world

#     for i in range(iterations):
#         #go to starting point
#         # tc.GoToMesh(linear=0.7,goal_x=10,goal_y=2,goal_z=5,threshold=0.05,max_vrot=5.0,min_rot_gain=0.5, exp_vrot=0.15,max_alpha=0.25,max_dy=0.5,k_y=2.5,k_alpha=-2.5)
#         # tc.GoToMesh(goal_x=19,goal_y=5,goal_z=10,k_alpha=2,max_vrot=4.0,linear=0.5)
#         if(i==iterations-1):
#             # go up
#             tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect)

#             # go down
#             tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect)

#         else :
#             # go up
#             tc.VerticalTransect(linear=linear_velocity,altitude=initial_z+H,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect)

#             # go down
#             tc.VerticalTransect(linear=linear_velocity,altitude=initial_z,k_alpha=k_alpha_ship_vertical,max_vrot=max_vrot_vertical_transect)

#             # go right 
#             tc.HorizontalTransect(linear=linear_velocity,altitude=initial_z,distance=step,max_vrot=5.0,k_alpha=5.0)
        
#     return True


def start_mission_server():
    rospy.init_node('start_mission_server')
    server_node = rospy.get_param("~server","/crawler_tasks1")
    default_period = rospy.get_param("~period",0.2)
    global tc 
    initial_pos=None
    tc = TaskClient(server_node,default_period)
    rospy.Service('start_mission', StartMission, handle_start_mission)
    rospy.spin()

if __name__ == '__main__':
    start_mission_server()
