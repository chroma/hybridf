#!/usr/bin/env python3
import rospy
from task_manager_crawler.srv import PlanRequest, PlanRequestResponse
from geometry_msgs.msg import Vector3
from task_manager_crawler.msg import Path, PathSegment
from std_msgs.msg import Header


def handle_plan_request(req):
    rospy.loginfo("Received a path planning request")
    
    # Dummy path generation
    header = Header(seq=0, stamp=rospy.Time.now(), frame_id="world")
    segment = PathSegment(
        header=header,
        start=req.start,
        end=req.end,
        ux=Vector3(x=1.0, y=0.0, z=0.0),
        uy=Vector3(x=0.0, y=1.0, z=0.0),
        uz=Vector3(x=0.0, y=0.0, z=1.0)
    )
    path = Path(header=header, segments=[segment])    
    rospy.loginfo("Sending path response")
    return PlanRequestResponse(path=path)

def path_planning_server():
    rospy.init_node('path_planning_server')
    rospy.loginfo("Path planning server is ready")
    rospy.Service('/mesh_planner/plan', PlanRequest, handle_plan_request)
    rospy.spin()

if __name__ == "__main__":
    path_planning_server()  


