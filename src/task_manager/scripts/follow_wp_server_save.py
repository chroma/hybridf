#!/usr/bin/env python3
import rospy
from task_manager_crawler.srv import StartMissionWP,StartMissionWPResponse,UpdatePrio,GetPrio
from task_manager_crawler.msg import Waypoint

import roslib; roslib.load_manifest('task_manager_crawler')
from nav_msgs.msg import Odometry
from math import *
from task_manager_lib.TaskClient import *
from geometry_msgs.msg import Point as Msg_Point
from std_msgs.msg import Bool as Msg_Bool

class MissionServer:
    def __init__(self):
        rospy.init_node('mission_server')
        rospy.Service('start_mission_wp', StartMissionWP, self.handle_start_mission)
        rospy.loginfo("Mission Server service ready.")
        self.prio = None
        self.robot_id = rospy.get_param('~topic_prefix')
        self.wps = None
        
        
        server_node = rospy.get_param("~server","/"+self.robot_id+"/crawler_tasks1")
        default_period = rospy.get_param("~period",0.2)
        self.tc = TaskClient(server_node,default_period)
        self.i = 0
        
    def handle_start_mission(self, req):
        robot_id = req.robot_id
        waypoints = req.waypoints
        rospy.loginfo("Starting mission for robot %s with waypoints:", self.robot_id)
        for i in range(len(waypoints)):
            rospy.loginfo("Waypoint %d : x:%.2f y:%.2f z:%.2f",i,waypoints[i].x,waypoints[i].y,waypoints[i].z)
        success = self.execute_mission(waypoints)
        return(success) 

    def execute_mission(self, waypoints):
        self.wps = waypoints
        self.i = 0
        return True
    

    def step(self):
        
        
        initial_pos=None
        
        if self.wps is not None:
            
            if self.i>0:
                #Go to an intermediate wp
               semi_wp = [(self.wps[self.i].x + self.wps[self.i-1].x)/2, (self.wps[self.i].y + self.wps[self.i-1].y)/2, (self.wps[self.i].z + self.wps[self.i-1].z)/2]
               self.tc.GoToMesh(linear=0.1,goal_x=semi_wp[0],goal_y=semi_wp[1],goal_z=semi_wp[2],threshold=0.1,max_vrot=0.3, max_dy=0.5,k_y=-1,k_alpha=0.3)
               

            #Wait until we have priority to move on to the next wp
            self.tc.WaitPrio(x=self.wps[self.i].x, y=self.wps[self.i].z, robot_id=int(self.robot_id.split('_')[1]))
            
            # Go to next wp
            self.tc.GoToMesh(linear=0.1,goal_x=self.wps[self.i].x,goal_y=self.wps[self.i].y,goal_z=self.wps[self.i].z,threshold=0.1,max_vrot=0.3, k_y=-1,k_alpha=0.3)
            
            


            # Update the priority of the reached wp
            rospy.wait_for_service('/update_prio')
            valid_update = False
            while not valid_update :
                try:
                    update_prio = rospy.ServiceProxy('/update_prio', UpdatePrio)
                    valid_update = update_prio(self.wps[self.i].x, self.wps[self.i].y, int(self.robot_id.split('_')[1])).valid_update
                except rospy.ServiceException as e:
                    rospy.loginfo("Service call failed: %s"%e)
        
            rospy.loginfo(self.robot_id+"reached wp (%d,%d)",self.wps[self.i].x, self.wps[self.i].y)
            self.i+=1

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            
            rate.sleep()
            self.step()
        
        
        
if __name__ == "__main__":
    mission_server = MissionServer()
    #rospy.spin()
    mission_server.run()
    

