import rospy
from task_manager_crawler.srv import StartMission,StartMissionResponse
from nav_msgs.msg import Odometry
from math import *
from task_manager_lib.TaskClient import *

initial_pose = None
tc = None

def handle_start_mission(req):
    # Extract the x, y, and z values from the service request
    x = req.x
    y = req.y
    z = req.z
    shape= "rectangle"
    
    # # Start the mission with the given coordinates
    # if(shape=="Rectangle"):
    #     success = start_mission(x, y, z)

    if shape == 'triangle':
        success=triangle_mission()
    elif shape == 'rectangle':
        success=rectangle_mission(x,y,z)
        # rospy.loginfo_once("mission is running")
        # print("rectangle mission is running")
        success=True
    else:
        rospy.logerr('Error: Invalid shape specified')
        success=False


    # Return the success flag as the service response
    return StartMissionResponse(success)

def triangle_mission():
    linear_velocity=0.5
    Vertex_A_x=0
    Vertex_A_y=0
    Vertex_B_x=2
    Vertex_B_y=0
    Vertex_C_x=1
    Vertex_C_y=2
    print("triangle mission ")
    #go to starting point
    tc.GoToMesh(goal_x=Vertex_A_x, goal_y=Vertex_A_y, goal_z=0, linear=linear_velocity)
    
    return True

def odom_callback(msg):
    global initial_pose
    if initial_pose is None:
        initial_pose = msg.pose.pose
        
def rectangle_mission(x, y, z):
    global initial_pose
    global tc
    linear_velocity=0.3
    Height=5
    step=0.5
    max_vrot_reach_changle=3.0
    max_vrot_vertical_transect=5.0
    
    # subscribe to the /odom topic to get the initial pose
    rospy.Subscriber('/odom', Odometry, odom_callback)
    
    # wait until the initial pose is received
    while initial_pose is None:
        rospy.sleep(0.1)

    # save the initial pose as the current pose
    current_pose = initial_pose

    # TODO: Implement mission up->right->down->right->///

    for i in range(4):
        #go to starting point
        # tc.GoToMesh(goal_x=x, goal_y=y, goal_z=z, linear=linear_velocity)
        # tc.GoToMesh(goal_x=19,goal_y=5,goal_z=10,k_alpha=2,max_vrot=4.0,linear=0.5)
        # pointing up
        tc.ReachAngle(target=1.57,k_alpha=5.0,max_vrot=3.0,threshold=0.005)
        
        # go up
        tc.VerticalTransect(linear=linear_velocity,altitude=5,k_alpha=2.0,max_vrot=5)
        tc.VerticalTransect(linear=linear_velocity,relative=True,altitude=Height) 

        # go down
        tc.VerticalTransect(linear=linear_velocity,relative=True,altitude=Height)

        # go right 
        tc.HorizontalTransect(relative=True,positive=True,linear=linear_velocity,distance=step, use_tf=True)

    return True

def start_mission_server():
    global tc
    rospy
