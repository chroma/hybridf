#!/usr/bin/env python3
import sys
import rospy
from task_manager_crawler.srv import StartMission

def start_mission_client(x, y, z,W,H):
    rospy.wait_for_service('start_mission')
    try:
        start_mission = rospy.ServiceProxy('start_mission', StartMission)
        response = start_mission(x, y, z,W,H)
        return response.success
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print("Usage: python script_name.py x y z W H")
        sys.exit(1)
    try:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        z = float(sys.argv[3])
        W = float(sys.argv[4])
        H = float(sys.argv[5])
    except ValueError:
        print("Invalid arguments. Please provide numeric values for x, y, z, W, and H.")
        sys.exit(1)

    # # Arguments passed
    # print("\nName of Python script:", sys.argv[0])
    # print("\nArguments passed:", end = " ")
    # for i in range(1, n):
    #     print(sys.argv[i], end = " ")


    # rospy.init_node('start_mission_client')
    # world="moeubius"
    # if world== "delivrable":
    #     x = 14.0
    #     y = 2.0
    #     z = 7.0
    #     W = 8.0
    #     H = 2.0
        
    # elif world== "moeubius":
    #     # x = -1.0
    #     # y = -1.0
    #     # z = 14
    #     # W = 8.0
    #     # H = 2.0
    #     a=2

    # elif world == "nantes":
    #     x = 4.0
    #     y = -4.0
    #     z = 7.0
    #     W = 8.0
    #     H = 2.0
    # elif world == "delivrable":
    #     x = 15.0
    #     y = 2.0
    #     z = 3.0
    #     W = 8.0
    #     H = 2.0

    success = start_mission_client(x, y, z,H,W)

    print("Mission running successfully: %s" % success)

    