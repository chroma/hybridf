# Hybrid Framework
![plot](Images/HybridFramework.png)

This Framework was create during the Europeen project bugWright2.
## *Abstract*

This video demonstrates how a hybrid team of real and simulated robot can operate in a virtually shared space in multi-robot path-planning experiments. Within the BugWright2 project, an EU-funded 4-year project, we focused on the use of robotic system for ship-hull inspections and specifically, magnetic-wheel crawlers for hull thickness measurements. These crawlers are typically attached to a ground unit with a tether, which is an unpassable obstacle for a multi-robot path planner.
We developed the theoretical NC\_MAPF algorithm, which provides a path-planning solution for multiple tethered robots operating on a 2D surface. The idea of this hybrid team of real and simulated robots was born from the desire to test this algorithm on real crawlers. However, due to limited access to infrastructures and the limited number of real crawlers available for testing, the addition of simulated crawlers, using the robotic simulation software Gazebo, helped to fill this gap. To ensure that the robots, whether real or simulated, respect their order of passage, a global multi-robot manager has been set up. It constantly communicates with the robots to check if they have reached their objective or to give them the go-ahead to continue their trajectory.

This git has only the simulated and global planner parts.


## Real Part
### *Elements*
- [ ] Leica Geosystem
- [ ] Marker
- [ ] Metalic wall

## Sim Part

- [ ] gazebo clasic
- [ ] ROS 1

### Run the simulation
```
roslaunch chroma_gazebo_interfaces global_Metz.launch
```
## Merge Part
![plot](Images/Merge.png){ width=75% }

This part is split into two part
In one hand how we see the simulation on the real world (Augmented Reality)
In another hand how we visualize the real world on the simulation (Digital twin)

### Augmented Reality
We take the image from the camera and, thank static transform, place it on the frame of the world.
Thanks to this, we can see all the frames on the image.
![plot](Images/Augmented_Reality.png){ width=75% }

- [ ] RealSens Camera
- [ ] Rviz

### Digital twin
The digital twin reads the position of the real robot and moves to that position.
![](Images/Twin.mp4){ width=100% }

## Run the Multi-robot Global planner 
```
rosrun crawler_gm gm_node.py 4 1 
```
- [ ] 4 is the number of simulated crawler 
- [ ] 1 is the number of real crawler


To test this Global Planner only with simulated crawlers, you need to change from 0 to 1 at line 94 of the src/crawler_fleet_manager/src/gm_node.py 



